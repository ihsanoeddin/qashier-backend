source 'https://rubygems.org'
ruby "2.4.1"

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.0'

# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.3.18', '< 0.5'

# Full text search for active record
gem 'search_cop'

# Use Puma as the app server
gem 'puma', '~> 3.0'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'

# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.0'

# background processer server
gem 'sidekiq'
gem 'redis-namespace'
gem 'apartment-sidekiq'

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
#gem 'capistrano-rails', group: :development
group :development do
  #deployment
  gem 'capistrano', '~> 3.6', require: false
  gem 'capistrano-rails', '~> 1.1', require: false
  gem 'capistrano-rvm', require: false
  gem 'capistrano3-puma', require: false
  gem 'capistrano3-nginx', require: false
  gem 'capistrano-upload-config', require: false
  gem 'capistrano-sidekiq', require: false
  gem 'capistrano-memcached', require: false

  # simple mail opener for development
  gem 'letter_opener'
end

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors', :require => 'rack/cors'

# multi tenant
gem  'apartment'#, github: 'vladra/apartment', branch: 'connection_based_threading_fix'

# User management
gem 'devise'#,'~> 4.1'
gem 'cancancan'#, '~> 1.15'
gem 'rolify'#, '~> 1.2'
gem 'devise_token_auth' 
gem 'doorkeeper'
gem "rack-oauth2"#, "~> 1.0.5"

# serializer support
gem 'active_model_serializers', '~> 0.10.0'

# memcached with dalli
gem 'memcachier'
gem 'dalli'
gem 'rack-cache'
gem 'kgio'

# cached active record objects
gem 'identity_cache'
gem 'cityhash'

# friendly url id
gem 'friendly_id'#, '~> 5.0.0'

# geolocation
gem 'geocoder'

# pagination
gem 'kaminari'#, '~> 1.0'

# stateful models
gem 'aasm'

# lightweight api stack GRAPE
gem 'grape'
gem 'rack-cors', :require => 'rack/cors'
gem 'grape_on_rails_routes'
gem 'grape-entity'
gem 'grape-swagger'
gem 'kaminari-grape'
gem 'wine_bouncer'
gem 'grape-kaminari'
gem 'grape-cancan'

# file upload
gem 'carrierwave', '~> 0.5'
gem 'carrierwave_backgrounder', '~> 0.4.2'
gem 'mini_magick'
gem 'fog'

# soft delete support
gem "paranoia", "~> 2.2"

# PDF builder
gem 'wkhtmltopdf-binary'
gem 'wicked_pdf'

# CSV builder
gem 'fastest-csv'

# Ruby units converter
gem 'ruby-units'

#group :staging, :production do 
  # bugs and errors reports
  gem "sentry-raven"
#end

# for faster csv
gem 'fastest-csv'

# date validation
gem 'date_validator'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  gem 'factory_girl_rails', '~> 4.0'
  gem 'shoulda-matchers', '~> 3.1'
  gem 'faker'
  gem 'database_cleaner'
end

group :development do
  gem 'listen', '~> 3.0.5'
  
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  #best practice metric tool
  gem "rails_best_practices"

end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

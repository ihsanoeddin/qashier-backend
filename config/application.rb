require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
# require "sprockets/railtie"
require "rails/test_unit/railtie"

require 'apartment/elevators/first_subdomain'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Qashier
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    #
    # get subdomain as tenant name
    #
    if !Rails.env.development?
      config.middleware.use 'Apartment::Elevators::FirstSubdomain'
    else
      config.middleware.use 'ForceSubdomainElevator'
    end

    #
    # autoload handlers modules
    #
    config.autoload_paths << "#{Rails.root}/lib/exception_handlers"

    #
    # autoload grape api files
    #
    config.paths.add "app/services", glob: "**/*.rb"
    config.autoload_paths += Dir["#{Rails.root}/app/services/*"]

    #
    # use sidekiq as active job queue adapter
    #
    config.active_job.queue_adapter = :sidekiq

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :options, :put, :delete]
      end
    end

    #
    # sentry setting
    #
    unless Rails.env.development?
      Raven.configure do |config|
        config.dsn = 'https://1ccb1403b5c140d0a2f06dfbc9d23ea7:f03af66bf30944138029363ab797a191@sentry.io/170215'
      end
    end
  
  end
end

if Rails.env.development?
  Rails.application.config.middleware.insert_before 'Warden::Manager', 'ForceSubdomainElevator'
else
  Rails.application.config.middleware.insert_before(Warden::Manager, Apartment::Elevators::FirstSubdomain)
end
#Rails.application.config.middleware.insert_before(Doorkeeper, Apartment::Elevators::FirstSubdomain)

#Rails.env.development?? Rails.application.config.middleware.insert_before('Warden::Manager', 'ForceSubdomainElevator') : Rails.application.config.middleware.insert_before('Warden::Manager', 'Apartment::Elevators::FirstSubdomain')
#Rails.application.config.middleware.insert_before 'Warden::Manager', 'ForceSubdomainElevator'



set :application, 'qashier-backend'
set :stage, :staging
set :rails_env, 'staging'

set :full_app_name, "#{fetch(:applicaton)}-#{fetch(:stage)}"
set :server_name, '137.59.126.26'#"api-staging.qashier.id"

set :deploy_to, '/home/backend/apps/qashier-backend'

# http://stackoverflow.com/questions/21036175/how-to-deploy-a-specific-revision-with-capistrano-3
set :branch, ENV["REVISION"] || ENV["BRANCH_NAME"] || 'staging'

role :app, %w{backend@137.59.126.26}
role :web, %w{backend@137.59.126.26}
role :db,  %w{backend@137.59.126.26}
server '137.59.126.26', user: 'backend', roles: %w{web app}

# nginx conf
set :nginx_server_name, "api-staging.qashier.id www.api-staging.qashier.id *.api-staging.qashier.id"



# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

# server "example.com", user: "deploy", roles: %w{app db web}, my_property: :my_value
# server "example.com", user: "deploy", roles: %w{app web}, other_property: :other_value
# server "db.example.com", user: "deploy", roles: %w{db}



# role-based syntax
# ==================

# Defines a role with one or multiple servers. The primary server in each
# group is considered to be the first unless any hosts have the primary
# property set. Specify the username and a domain or IP for the server.
# Don't use `:all`, it's a meta role.

# role :app, %w{deploy@example.com}, my_property: :my_value
# role :web, %w{user1@primary.com user2@additional.com}, other_property: :other_value
# role :db,  %w{deploy@example.com}



# Configuration
# =============
# You can set any configuration variable like in config/deploy.rb
# These variables are then only loaded and set in this stage.
# For available Capistrano configuration variables see the documentation page.
# http://capistranorb.com/documentation/getting-started/configuration/
# Feel free to add new variables to customise your setup.



# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult the Net::SSH documentation.
# http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
# The server-based syntax can be used to override options:
# ------------------------------------
# server "example.com",
#   user: "user_name",
#   roles: %w{web app},
#   ssh_options: {
#     user: "user_name", # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: "please use keys"
#   }


#set :puma_user, fetch(:user)
#set :puma_rackup, -> { File.join(current_path, 'config.ru') }
#set :puma_state, "#{shared_path}/tmp/pids/puma.state"
#set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
#set :puma_bind, "unix://#{shared_path}/tmp/sockets/puma.sock"    #accept array for multi-bind
#set :puma_control_app, false
#set :puma_default_control_app, "unix://#{shared_path}/tmp/sockets/pumactl.sock"
#set :puma_conf, "#{shared_path}/puma.rb"
#set :puma_access_log, "#{shared_path}/log/puma_access.log"
#set :puma_error_log, "#{shared_path}/log/puma_error.log"
#set :puma_role, :app
#set :puma_env, fetch(:rack_env, fetch(:rails_env, 'production'))
set :puma_threads, [4, 16]
set :puma_workers, 5
#set :puma_worker_timeout, nil
#set :puma_init_active_record, false
#set :puma_preload_app, false
set :puma_daemonize, true
#set :puma_plugins, []  #accept array of plugins
#set :puma_tag, fetch(:application)
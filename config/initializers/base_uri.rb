base_uri_yaml = Rails.root.join("config", 'base_uri.yml').to_s

if File.exists?(base_uri_yaml)
  YAML.load_file(base_uri_yaml)[Rails.env].each do |key, value|
    ENV[key] = value
  end
end
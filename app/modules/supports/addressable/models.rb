module Supports::Addressable::Models
  def self.table_name_prefix
    'supports_addressable_'
  end
end
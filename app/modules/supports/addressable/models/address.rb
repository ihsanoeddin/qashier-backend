# Schema
#    create_table :supports_addressable_addresses do |t|
#      t.string :type
#      t.string :name
#      t.string :address
#      t.string :city
#      t.string :state
#      t.string :country
#      t.string :zip_code
#      t.float :latitude
#      t.float :longitude
#      t.references :addressable, polymorphic: true, index: { name: 'addresses_addressable_id_and_addressable_type' }
#      t.datetime :deleted_at, index: true
#      t.timestamps
#    end
#

module Supports
  module Addressable
    module Models
      class Address < ::ApplicationRecord

          #
          # include cacheable methods
          #
          include ::Supports::Cacheable::Cacheable

          belongs_to :addressable, polymorphic: true

          cache_belongs_to :addressable, inverse_name: :addresses

          #
          # get full address
          #
          def full_address
            [name, address, city, state, country].compact.join(", ")
          end

      end
    end
  end
end
module Supports
  module Synchronizable
    module Helpers
      class Synchronize

        attr_accessor :synchronizable_class, :synchronize_options, :synchronize_base_query, :synchronization_result

        #
        # constructor
        #
        def initialize(synchronizable_class, token, options={})
          self.synchronizable_class= synchronizable_class
          synchronize_options options.merge!(token: token)
        end

        #
        # to perform synchronization
        #
        def synchronize
          clear_cache_if_last_sync_is_different
          if synchronize_options.resource_status
            get_status_synchronization
          else
            get_synchronized_collections
          end
        end

        protected

          def get_status_synchronization
            #clear cache first
            clear_resource_query_cache
            synchronization_result total: 0
            synchronize_options do
              synchronization_result.total= synchronize_base_query.count
              if synchronization_result.total > 0
                Rails.cache.delete(synchronize_options.resource_key)
                cache_synchronized_collections
              end
            end
            synchronization_result
          end

          def get_synchronized_collections
            synchronize_options do
              clear_resource_query_cache if synchronize_options.resource_clear_cache
              query= Rails.cache.fetch(synchronize_options.resource_key)
              if query.blank?
                synchronize_base_query.load
                query= cache_synchronized_collections 
              end
              if query.respond_to?('loaded?') and not query.loaded?
                synchronize_base_query.load
                query= cache_synchronized_collections
              end
              collection= query.page(synchronize_options.resource_page.to_i.eql?(0) ? 1 : synchronize_options.resource_page.to_i).per(synchronize_options.resource_per_page)
              synchronization_result collection: present_collection(collection),
                                     per_page: synchronize_options.resource_per_page,
                                     current_page: collection.current_page.to_i, 
                                     total_pages: collection.total_pages, 
                                     last_page: collection.total_pages.eql?(collection.current_page.to_i) || (collection.total_pages < collection.current_page.to_i)

            end
            synchronization_result
          end

          def synchronize_options params={}
            if @synchronize_options.blank?
              last_sync= Time.zone.parse(params[:last_sync]) rescue 10.years.ago
              { :token => params[:token],
                :clear_cache => params[:clear_cache], 
                :custom_query => params[:custom_query],
                :last_sync => last_sync, 
                :max_date => params[:max_date], 
                :key => get_resource_cache_key(params[:token], last_sync), 
                :presenter => params[:presenter] || params[:each_presenter],
                :presenter_options => params[:presenter_options],
                :scopes => params[:scopes], :includes => params[:includes], 
                :status => params[:status], 
                :per_page => params[:per_page] || 50, 
                :page => params[:page] || 1 }.each do |dp, val|
                  @synchronize_options= OpenStruct.new if @synchronize_options.blank?
                  @synchronize_options.send("resource_#{dp}=", val) 
              end
            end
            if block_given?
              yield
            else
              @synchronize_options
            end
          end

          def synchronize_base_query
            return @synchronize_base_query if @synchronize_base_query.present?
            base_query ||= self.synchronizable_class.respond_to?(:cached_collection) ? self.synchronizable_class.cached_collection : self.synchronizable_class
            base_query= base_query.with_deleted if implement_soft_deletable?
            base_query= custom_query(base_query)
            base_query= use_scopes(base_query)
            base_query= includes_relations(base_query)
            @synchronize_base_query= time_scoped(base_query)
          end

          def custom_query(query)
            if synchronize_options.resource_custom_query.present?
              if synchronize_options.resource_custom_query.is_a?(Array)
                query= query.where(*synchronize_options.resource_custom_query)
              else
                query= query.where(synchronize_options.resource_custom_query)
              end
            end
            query
          end

          def includes_relations(query)
            synchronize_options do
              include_list = synchronize_options.resource_includes
              include_list = include_list.deep_symbolize_keys if include_list.is_a? Hash
              query.includes(include_list) if include_list.present?
            end
            query
          end

          def use_scopes(query)
            synchronize_options do
              scopes= synchronize_options.resource_scopes
              if scopes.is_a?(Array) or scopes.instance_of?(String) or scopes.instance_of?(Symbol)
                scopes= [scopes] unless scopes.is_a?(Array)
                scopes.each do |proposed_scope| 
                  query= query.send(proposed_scope) if query.respond_to?(proposed_scope) 
                end
              end
            end
            query
          end

          def time_scoped(query)
            synchronize_options do 
              query= query.updated_above(synchronize_options.resource_last_sync)
              query= query.updated_below(synchronize_options.resource_max_date.to_datetime) if synchronize_options.resource_max_date.present?
            end
            query
          end

          def present_collection(collection)
            synchronize_options do 
              presenter= synchronize_options[:resource_presenter]
              if presenter
                collection.map{|obj| presenter.new(*[obj, (synchronize_options[:resource_presenter_options] || {}).merge!(last_sync: synchronize_options[:resource_last_sync])].compact) }
              end
            end
          end

          def cache_synchronized_collections
            synchronize_options do
              Rails.cache.fetch(synchronize_options.resource_key, expires_in: 5.minutes) do
                synchronize_base_query.load
              end
            end
          end

          def get_resource_cache_key token=nil, last_sync=nil
            #raise 'Expected a valid Token object' unless token.instance_of? Token
            "#{::Apartment::Tenant.current}:#{last_sync.to_i}:sync:#{last_sync.to_i}:#{self.synchronizable_class.to_s}:#{token.token}"
          end

          def implement_soft_deletable?
            soft_deletable?
          end

          def synchronization_result params= {}
            @synchronization_result||= OpenStruct.new params
          end

          def soft_deletable?
            self.synchronizable_class.respond_to? :acts_as_paranoid
          end

          def clear_cache_if_last_sync_is_different
            last_sync_stamp= synchronize_options.resource_key.split(':')[1].to_i
            clear_resource_query_cache unless synchronize_options.resource_last_sync.to_i.eql?(last_sync_stamp)
          end

          def clear_resource_query_cache
            Rails.cache.delete(synchronize_options.resource_key)
          end

      end
    end
  end
end
#
# this worker class is to perform import processing
# 
#
module Supports
  module Importers
    module Workers

      class Import

        include Sidekiq::Worker

        def perform(class_name, id)
          importer= class_name.constantize.find(id) rescue nil
          importer.processing! if importer.present?
        end
      
      end

    end
  end
end

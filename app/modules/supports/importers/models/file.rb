#
# schema
#create_table :supports_importers_attachments do |t|
#      
#  t.references :import, index: { name: "supports_importers_attachments_import_id" }
#  t.string :syntax
#  t.string :attachment
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Supports
  module Importers
    module Models
      class File < ::ApplicationRecord

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # mount uploader carrierwave
        #
        mount_uploader :attachment, ::Supports::Importers::Uploaders::ExcelUploader

        #
        #define relationships
        #
        belongs_to :import, class_name: "::Supports::Importers::Models::Import", foreign_key: :import_id

        #
        # validation
        #
        validates :attachment, presence: true

        #
        # cache relationships
        #
        cache_belongs_to :import, inverse_name: :file

        #
        # full attachment path
        #
        def attachment_path
          Rails.root.join.to_s + "/public" + attachment.url
        end

        #
        # check if csv file has valid header
        #
        def valid_header?
          valid_header= import.class.csv_headers
          valid= open_attachment do |csv| 
                    headers= csv.shift
                    valid_header.map{|vh| vh.to_s.parameterize.underscore }.compact.uniq.sort == headers.map{|h| h.to_s.parameterize.underscore }.compact.uniq.sort
                 end
        end

        #
        # to open csv file
        #
        def open_attachment &block
          FastestCSV.open(attachment_path) do |csv|
            yield(csv)
          end
        end

        #
        # to get row value with its header as hash
        #
        def row_with_header headers=[], row_array=[]
          row_h= {}
          headers.each_with_index { |h,i| row_h[h.parameterize.underscore.to_sym]= row_array[i]  }
          row_h
        end

        #
        # get total row
        #
        def total_rows
          open_attachment do |csv|
            csv.shift
            csv.count
          end
        end


      end
    end
  end
end

module Supports
  module Batchable
    module Batchable
      extend ::ActiveSupport::Concern

      module ClassMethods

        def batch params={}
          options= { batch: [], base_query: nil }
          options= options.merge params
          options[:batch]= params[self.name.to_s.parameterize.underscore.pluralize] || [] if options[:batch].blank?

          results= OpenStruct.new errors: [], batch: [], error_state: false
          base_query= options[:base_query] || self

          if options[:batch].is_a? Hash
              batch_arr= []
              options[:batch].each { |k,v|  batch_arr << v }
              options[:batch]= batch_arr
          end

          begin
            transaction do
              options[:batch].each do |batch_params|
                persisted_record_identifier= batch_params[:id].to_i.eql?(0) ? false : batch_params.delete(:id)  
                batch_object= persisted_record_identifier ? batch_persisted_record(base_query, persisted_record_identifier, batch_params) : batch_new_record(base_query, batch_params)
                if not batch_object.errors.any?
                  batch_object= after_save_batch(batch_object)
                  results.batch << batch_object
                  results.errors << nil
                else        
                  results.error_state= true unless results.error_state
                  results.errors << { messages: batch_object.errors.messages }
                end
              end
              if results.error_state
                raise ActiveRecord::Rollback
              else 
                results.batch= after_save_batch(results.batch)
              end 
            end
          
          rescue => e
            puts e.message
            puts e.backtrace.join("\n")
            results.error_state= true
            #results.errors= { message: e.message }
            results.errors= { message: "Invalid id" }
          end
          results
        end

        def batch_persisted_record query, id, params
          batch_object= query.find(id)
          params= send(:before_save_batch, params)
          destroy= params.delete(:_destroy)
          destroy ? batch_object.destroy : batch_object.update(params)
          batch_object
        end

        def batch_new_record query, params
          params= before_save_batch params
          batch_object= query.new params.except(:_destroy)
          params.delete(:_destroy) ?  batch_object.destroy : batch_object.save
          batch_object      
        end

        def before_save_batch params={}
          params
        end

        def after_save_batch_object object
          object
        end

        def after_save_batch batch= []
          batch
        end

      end

    end
  end
end
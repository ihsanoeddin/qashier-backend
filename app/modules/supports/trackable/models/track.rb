#
# this class is used to store changes of tracked record 
#

module Supports
  module Trackable
    module Models
      class Track < ::ApplicationRecord
        
        serialize :previous_record_attributes, Hash

        #
        # define relationships
        #
        belongs_to :trackable, polymorphic: true
        belongs_to :editor, polymorphic: true

        validates :trackable, presence: true
        validates :editor, presence: true

      end
    end
  end
end
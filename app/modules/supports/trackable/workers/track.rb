#
# sidekiq worker for saving track object
#

require 'json'

module Supports
  module Trackable
    module Workers
      class Track

        include ::Sidekiq::Worker
        
        def perform(track_params)
          ::Supports::Trackable::Models::Track.new(JSON.parse(track_params)).save
        end

      end
    end
  end
end
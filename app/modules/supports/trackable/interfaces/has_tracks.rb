#
# this module is used on tracked records class
# include this module and declare method #track options={}
#

module Supports
  module Trackable
    module Interfaces
      module HasTracks
        
        extend ::ActiveSupport::Concern

        included do 
          has_many :tracks, as: :trackable, class_name: "::Supports::Trackable::Models::Track"
          accepts_nested_attributes_for :tracks

          before_save :previous_attributes_values

          class_attribute :track_default_options

          self.track_default_options = {
                                    persistent: true,
                                    when: :after_commit,
                                    depend_on_changes_of: :all
                                  }

          attr_accessor :current_editor
        end

        #
        # get last changes of trackable object
        #
        def last_changes
          tracks.last
        end

        protected

          #
          # set recent attributes values
          #
          def previous_attributes_values
            @previous_attributes_values= {}
            if @previous_attributes_values.blank?
              self.class.column_names.each do |col_name|
                @previous_attributes_values[col_name.to_sym]= send("#{col_name}_was") || send("#{col_name}")
              end
            end
            @previous_attributes_values
          end

          #
          # helper method to generate track object
          # send saving process on background worker
          #
          def track!
            if self.destroyed?
              track = tracks.new editor: current_editor, previous_record_attributes: previous_attributes_values, tracked_state: :deleted            
            else
              track= tracks.new editor: current_editor, previous_record_attributes: previous_attributes_values, tracked_state: previous_changes[:id].present?? :created : :updated
            end
            if track.valid?
              ::Supports::Trackable::Workers::Track.perform_async(track.attributes.to_json)
            else
              errors.add(:tracking_error, "Error when generating log")
              raise ::ActiveRecord::Rollback
            end
          end

          #
          # helper method to get recent changes of attributes
          #
          def recent_changes_attributes
            recent_changes= {}
            previous_changes.each do |att, value|
            end
          end

          #
          # helper method to asses if object will be tracked or not
          #
          def trackable?(options= self.trackable_options)
            fields= options[:depend_on_changes_of]
            generate = false
            if fields.is_a?(String) or fields.is_a?(Symbol)
              fields= String(fields)
              if fields == "all"
                generate= previous_changes.present?
              else
                generate= previous_changes[fields].present?
              end
            elsif fields.is_a?(Array)
              fields.each do |field|
                generate= previous_changes[field]
                break if generate
              end 
            elsif fields.is_a?(Hash)
              fields.each do |key,val|
                if previous_changes[key].present?
                  if val.is_a?(Array)
                    generate= previous_changes[key].any?{|change| val.include?(change) }
                  else
                    generate= previous_changes[key].include?(val)
                  end
                end
                break if generate
              end
            end
            generate
          end

        module ClassMethods

          #
          # helper method for initialize object tracking
          #
          def track! options={}
            #
            # track when record is created or updated
            #
            default_options= self.track_default_options
            class_attribute :trackable_options

            self.trackable_options= default_options.merge!(options)
            Array(default_options[:when]).flatten.compact.each do |callback|
              send(callback, :track!, if: :trackable?)
            end

            #
            # track when record is destroyed
            #
            send(:after_destroy, :track!)
          end

        end

      end
    end
  end
end
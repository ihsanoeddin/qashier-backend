module Supports
  module Imageable
    module Interfaces
      module BelongsToImageGroup
        extend ::ActiveSupport::Concern
      
        included do 
          belongs_to :image_group, class_name: "::Supports::Imageable::Models::ImageGroup", foreign_key: :image_group_id, optional: true
        end

        def image_urls(image_size = :thumb)
          Rails.cache.fetch("#{self.class.try(:cached_name)}-image_urls-#{self}-#{image_size}", :expires_in => 3.hours) do
            image_group ? image_group.image_urls(image_size) : []
          end
        end

      end
    end
  end
end
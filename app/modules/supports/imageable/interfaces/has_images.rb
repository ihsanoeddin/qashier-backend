module Supports
  module Imageable
    module Interfaces
      module HasImages
        extend ::ActiveSupport::Concern
      
        included do 
          has_many :images, -> {order(:position)},
                    as:          :imageable,
                    dependent:   :destroy,
                    class_name: "::Supports::Imageable::Models::Image"
          accepts_nested_attributes_for :images, allow_destroy: true, reject_if: :has_no_attachment?
        end

        #
        # check whether attachment is present
        #
        def has_no_attachment?(attributed)
            attributed['attachment'].try(:underscore).eql?('undefined') and attributed['attachment'].blank? and attributed['attachment_cache'].blank?
        end

      end
    end
  end
end
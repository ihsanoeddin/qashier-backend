module Supports::Imageable::Models
  def self.table_name_prefix
    'supports_imageable_'
  end
end
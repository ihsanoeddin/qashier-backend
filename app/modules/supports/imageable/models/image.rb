module Supports
  module Imageable
    module Models
      class Image < ::ApplicationRecord

        #
        # include cacheable methods
        #
        include ::Supports::Cacheable::Cacheable

        #
        # implement batch method
        #
        include ::Supports::Batchable::Batchable
        
        #
        # define relationship
        #
        belongs_to :imageable, polymorphic: true, optional: true#, counter_cache: true
        cache_belongs_to :imageable

        #
        # mount uploader
        #
        mount_uploader :attachment, ::Supports::Imageable::Uploaders::ImageUploader

        #
        # validation
        #
        validates :attachment, presence: true

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection.where(nil)
            if params[:product_id]
              res = res.where(imageable_type: "Main::ProductsManagement::Models::Product", imageable_id: params[:product_id])
            end
            if params[:image_group_id]
              res = res.where(imageable_type: "Supports::Imageable::Models::ImageGroup", imageable_id: params[:image_group_id])
            end
            res
          end

        end

      end
    end
  end
end
module Tenants::Addressable::Models
  def self.table_name_prefix
    'tenants_addressable_'
  end
end
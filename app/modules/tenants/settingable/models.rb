module Tenants::Settingable::Models
  def self.table_name_prefix
    'tenants_settingable_'
  end
end
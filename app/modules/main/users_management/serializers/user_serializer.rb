module Main
  module UsersManagement
    module Serializers
      class UserSerializer < ::ActiveModel::Serializer

        attributes :username, :email, :merchant, :deleted_at, :created_at, :updated_at, :roles

        def roles
          ::ActiveModel::Serializer::CollectionSerializer.new(object.roles, serializer: ::Main::UsersManagement::Serializers::RoleSerializer)
        end

      end
    end
  end
end
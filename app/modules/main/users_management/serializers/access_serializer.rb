module Main
  module UsersManagement
    module Serializers
      class AccessSerializer < ::ActiveModel::Serializer
        
        attributes :id, :name, :key, :active, :parent_id, :type#, :deleted_at, :created_at, :updated_at

      end
    end
  end
end
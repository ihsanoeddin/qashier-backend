#
# this module is used to override or help for accessing devise methods
#
module Main
  module UsersManagement
    module Helpers
      module DeviseHelper
        
        extend ::ActiveSupport::Concern

        #
        # HELPER for fetch devise generate_confirmation_token method
        #
        def fetch_confirmation_token
          unless fetch_raw_confirmation_token
            generate_confirmation_token
          end
          save
          fetch_raw_confirmation_token
        end

        #
        # HELPER to fetch var @raw_confirmation_token
        #
        def fetch_raw_confirmation_token
          instance_variable_get("@raw_confirmation_token")
        end

        protected

          #
          # @Override devise callback helper
          # by default devise will process confirmation instruction
          #
          def confirmation_required?
            false #!confirmed?
          end

      end
    end
  end
end
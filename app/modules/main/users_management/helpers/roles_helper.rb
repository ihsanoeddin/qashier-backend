#
# this module is helper for saving user with roles attributes
#
module Main
  module UsersManagement
    module Helpers
      module RolesHelper
        
        extend ::ActiveSupport::Concern
        included do 
          class_attribute :role_class
          self.role_class= ::Main::UsersManagement::Models::Role

          attr_accessor :roles_attributes, :commited_roles

          after_validation :append_commit_roles
          after_save :commit_roles
        end

        protected

          #
          # this method is used to simulate saving as one to many relationship to Role class
          # it will append new role object to :commited_roles attribute
          #
          def append_commit_roles
            begin
              self.commited_roles = []
              if self.roles_attributes.present?
                self.roles_attributes = Array(roles_attributes) unless roles_attributes.is_a?(Array)
                self.roles_attributes.each do |role_attributes|
                  #
                  # raise ArgumentError if role attributes is not a Hash Object
                  #
                  raise ArgumentError unless role_attributes.is_a?(Hash)
                  new_role = role_class.find_or_initialize_by(  name: role_attributes[:name], 
                                                                resource_type: role_attributes[:resource_type], 
                                                                resource_id: role_attributes[:resource_id] )
                  unless new_role.valid?(:merchant)
                    #
                    # append errors 
                    #
                    self.errors[:roles_attributes] << new_role.errors.messages
                  end
                end
              end
            rescue ArgumentError => e
              Rails.logger.error e.backtrace
              self.errors.add(:roles_attributes, e.message)
            end
          end

          #
          # append :commited_roles to self
          #
          def commit_roles
            if self.commited_roles.is_a?(Array)
              self.roles_attributes= nil
              self.commited_roles.each do |role|
                if role.save(context: :merchant)
                  self.commited_roles << role
                else
                  self.errors[:roles_attributes] << role.errors.messages
                  #
                  # rollback if errors present
                  #
                  throw :abort
                end
                self.roles << role
              end
            end
          end

      end
    end
  end
end
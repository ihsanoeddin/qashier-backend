#
# this concern provide just one class method
# to provide defaults data
#
module Main
  module UsersManagement
    module Templates
      module Accesses

        extend ::ActiveSupport::Concern

        module ClassMethods
          
          #
          # provide default accesses records 
          #
          def default_records_templates
            [
              # manage all
              {
                name: "Manage All",
                context: [:all],
                key: [:manage]
              },
              #manage users management
              {
                name: "Manage Users Management",
                context: [::Main::StoresManagement::Models::Store, ::Main::UsersManagement::Models::User, ::Main::UsersManagement::Models::Role],
                key: [:manage],
                childs_attributes: [
                  #manage stores accesses 
                  {
                    name: "Manage Stores",
                    context: [::Main::StoresManagement::Models::Store],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Stores",
                        context: [::Main::StoresManagement::Models::Store],
                        key: [:read]
                      },
                      {
                        name: "Create Store",
                        context: [::Main::StoresManagement::Models::Store],
                        key: [:create]
                      },
                      {
                        name: "Update Store",
                        context: [::Main::StoresManagement::Models::Store],
                        key: [:update]
                      },
                      {
                        name: "Delete Store",
                        context: [::Main::StoresManagement::Models::Store],
                        key: [:delete]
                      }
                    ]
                  },
                  #manage users accesses
                  {
                    name: "Manage Users",
                    context: [:all],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Users",
                        context: [::Main::StoresManagement::Models::User],
                        key: [:read]
                      },
                      {
                        name: "Create User",
                        context: [::Main::StoresManagement::Models::User],
                        key: [:create]
                      },
                      {
                        name: "Update User",
                        context: [::Main::StoresManagement::Models::User],
                        key: [:update]
                      },
                      {
                        name: "Delete User",
                        context: [::Main::StoresManagement::Models::User],
                        key: [:Delete]
                      }
                    ]
                  },
                  #manage roles accesses
                  {
                    name: "Manage Roles",
                    context: [:all],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Roles",
                        context: [::Main::StoresManagement::Models::Role],
                        key: [:read]
                      },
                      {
                        name: "Create Role",
                        context: [::Main::StoresManagement::Models::Role],
                        key: [:create]
                      },
                      {
                        name: "Update Role",
                        context: [::Main::StoresManagement::Models::Role],
                        key: [:update]
                      },
                      {
                        name: "Delete Role",
                        context: [::Main::StoresManagement::Models::Role],
                        key: [:Delete]
                      }
                    ]
                  }
                ]
              },
              #manage products management
              {
                name: "Manage Products Management",
                context: [
                          ::Main::ProductsManagement::Models::ProductType, ::Main::ProductsManagement::Models::TaxRate, 
                          ::Main::ProductsManagement::Models::Product, ::Main::ProductsManagement::Models::Property,
                          ::Main::ProductsManagement::Models::Variant, ::Main::ProductsManagement::Models::EntityProperty,
                          ::Main::ProductsManagement::Models::VariantsBundle
                         ],
                key: [:manage],
                childs_attributes:[
                  #manage product types
                  {
                    name: "Manage Product Types",
                    context: [::Main::ProductsManagement::Models::ProductType],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Product Types",
                        context: [::Main::ProductsManagement::Models::ProductType],
                        key: [:read]
                      },
                      {
                        name: "Create Product Type",
                        context: [::Main::ProductsManagement::Models::ProductType],
                        key: [:create]
                      },
                      {
                        name: "Update Product Type",
                        context: [::Main::ProductsManagement::Models::ProductType],
                        key: [:update]
                      },
                      {
                        name: "Delete Product Type",
                        context: [::Main::ProductsManagement::Models::ProductType],
                        key: [:delete]
                      },
                    ]
                  },
                  #manage tax rates
                  {
                    name: "Manage Tax Rates",
                    context: [::Main::ProductsManagement::Models::TaxRate],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Tax Rates",
                        context: [::Main::ProductsManagement::Models::TaxRate],
                        key: [:read]
                      },
                      {
                        name: "Create Tax Rate",
                        context: [::Main::ProductsManagement::Models::TaxRate],
                        key: [:create]
                      },
                      {
                        name: "Update Tax Rate",
                        context: [::Main::ProductsManagement::Models::TaxRate],
                        key: [:update]
                      },
                      {
                        name: "Delete Tax Rate",
                        context: [::Main::ProductsManagement::Models::TaxRate],
                        key: [:delete]
                      },
                    ]
                  },
                  {
                    name: "Manage Tax Rates",
                    context: [::Main::ProductsManagement::Models::TaxRate],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Tax Rates",
                        context: [::Main::ProductsManagement::Models::TaxRate],
                        key: [:read]
                      },
                      {
                        name: "Create Tax Rate",
                        context: [::Main::ProductsManagement::Models::TaxRate],
                        key: [:create]
                      },
                      {
                        name: "Update Tax Rate",
                        context: [::Main::ProductsManagement::Models::TaxRate],
                        key: [:update]
                      },
                      {
                        name: "Delete Tax Rate",
                        context: [::Main::ProductsManagement::Models::TaxRate],
                        key: [:delete]
                      },
                    ]
                  },
                  # manage items and services
                  {
                    name: "Manage Items and Services",
                    context: [
                              ::Main::ProductsManagement::Models::Product, ::Main::ProductsManagement::Models::Variant, 
                              ::Main::ProductsManagement::Models::VariantsBundle, ::Main::ProductsManagement::Models::EntityProperty,
                              ::Supports::Imageable::Models::Image, ::Supports::Imageable::Models::ImageGroup
                             ],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Items and Services",
                        context: [
                                  ::Main::ProductsManagement::Models::Product, ::Main::ProductsManagement::Models::Variant, 
                                  ::Main::ProductsManagement::Models::VariantsBundle, ::Main::ProductsManagement::Models::EntityProperty,
                                  ::Supports::Imageable::Models::Image, ::Supports::Imageable::Models::ImageGroup
                                 ],
                        key: [:read]
                      },
                      {
                        name: "Create Items and Services",
                        context: [
                                  ::Main::ProductsManagement::Models::Product, ::Main::ProductsManagement::Models::Variant, 
                                  ::Main::ProductsManagement::Models::VariantsBundle, ::Main::ProductsManagement::Models::EntityProperty,
                                  ::Supports::Imageable::Models::Image, ::Supports::Imageable::Models::ImageGroup
                                 ],
                        key: [:create]
                      },
                      {
                        name: "Update Items and Services",
                        context: [
                                  ::Main::ProductsManagement::Models::Product, ::Main::ProductsManagement::Models::Variant, 
                                  ::Main::ProductsManagement::Models::VariantsBundle, ::Main::ProductsManagement::Models::EntityProperty,
                                  ::Supports::Imageable::Models::Image, ::Supports::Imageable::Models::ImageGroup
                                 ],
                        key: [:update]
                      },
                      {
                        name: "Delete Items and Services",
                        context: [
                                  ::Main::ProductsManagement::Models::Product, ::Main::ProductsManagement::Models::Variant, 
                                  ::Main::ProductsManagement::Models::VariantsBundle, ::Main::ProductsManagement::Models::EntityProperty,
                                  ::Supports::Imageable::Models::Image, ::Supports::Imageable::Models::ImageGroup
                                 ],
                        key: [:delete]
                      }
                    ]
                  },
                  #manage properties 
                  {
                    name: "Manage Properties",
                    context: [
                              ::Main::ProductsManagement::Models::Property
                             ],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Properties",
                        context: [
                                  ::Main::ProductsManagement::Models::Property
                                 ],
                        key: [:read]
                      },
                      {
                        name: "Create Property",
                        context: [
                                  ::Main::ProductsManagement::Models::Property
                                 ],
                        key: [:create]
                      },
                      {
                        name: "Update Property",
                        context: [
                                  ::Main::ProductsManagement::Models::Property
                                 ],
                        key: [:update]
                      },
                      {
                        name: "Delete Property",
                        context: [
                                  ::Main::ProductsManagement::Models::Property
                                 ],
                        key: [:delete]
                      }
                    ]
                  }
                ]
              },
              # manage procurements managements
              {
                name: "Manage Procurements Management",
                context: [
                            ::Main::ProcurementsManagement::Models::Contact, ::Main::ProcurementsManagement::Models::Supplier,
                            ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                            ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail,
                            ::Main::ProcurementsManagement::Models::Return, ::Main::ProcurementsManagement::Models::SuppliersVariant
                         ],
                key: [:manage],
                categories: ["store_admin"],
                childs_attributes: [
                  #manage suppliers
                  {
                    name: "Manage Suppliers",
                    context: [
                              ::Main::ProcurementsManagement::Models::Contact, ::Main::ProcurementsManagement::Models::Supplier, 
                              ::Main::ProcurementsManagement::Models::SuppliersVariant
                             ],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Suppliers",
                        context: [
                                  ::Main::ProcurementsManagement::Models::Contact, ::Main::ProcurementsManagement::Models::Supplier, 
                                  ::Main::ProcurementsManagement::Models::SuppliersVariant
                                 ],
                        key: [:read] 
                      },
                      {
                        name: "Create Supplier",
                        context: [
                                  ::Main::ProcurementsManagement::Models::Contact, ::Main::ProcurementsManagement::Models::Supplier, 
                                  ::Main::ProcurementsManagement::Models::SuppliersVariant
                                 ],
                        key: [:create] 
                      },
                      {
                        name: "Update Supplier",
                        context: [
                                  ::Main::ProcurementsManagement::Models::Contact, ::Main::ProcurementsManagement::Models::Supplier, 
                                  ::Main::ProcurementsManagement::Models::SuppliersVariant
                                 ],
                        key: [:update] 
                      },
                      {
                        name: "Delete Supplier",
                        context: [
                                  ::Main::ProcurementsManagement::Models::Contact, ::Main::ProcurementsManagement::Models::Supplier, 
                                  ::Main::ProcurementsManagement::Models::SuppliersVariant
                                 ],
                        key: [:delete] 
                      },
                    ]
                  },
                  #manage purchase orders
                  {
                    name: "Manage Purchase Orders",
                    context: [
                              ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                              ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                             ],
                    key: [:manage],
                    categories: ["inventory"],
                    childs_attributes: [
                      {
                        name: "Read Purchase Orders",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:read] 
                      },
                      {
                        name: "Create Purchase Order",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:create] 
                      },
                      {
                        name: "Update Purchase Order",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:update] 
                      },
                      {
                        name: "Delete Purchase Order",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:update] 
                      },
                      {
                        name: "Open Purchase Order",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:open] 
                      },
                      {
                        name: "Cancel Purchase Order",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:cancel] 
                      },
                      {
                        name: "Process Purchase Order",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:process] 
                      },
                      {
                        name: "Receive Purchase Order",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:receive] 
                      },
                      {
                        name: "Create Invoice",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:create] 
                      },
                      {
                        name: "Update Invoice",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:update] 
                      },
                      {
                        name: "Accept Invoice",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:accept] 
                      },
                      {
                        name: "Reject Invoice",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:reject] 
                      },
                      {
                        name: "Pay Invoice",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:pay] 
                      },
                      {
                        name: "Complete Purchase Order",
                        context: [
                                  ::Main::ProcurementsManagement::Models::PurchaseOrder, ::Main::ProcurementsManagement::Models::PurchaseOrderDetail,
                                  ::Main::ProcurementsManagement::Models::Invoice, ::Main::ProcurementsManagement::Models::InvoiceDetail 
                                 ],
                        key: [:complete] 
                      }
                    ]
                  }
                ]
              },
              # manage inventory management
              {
                name: "Manage Inventory Management",
                context: [::Main::InventoryManagement::Models::StockAdjustment, ::Main::InventoryManagement::Models::Stock],
                key: [:manage],
                categories: ["inventory", "store_admin"],
                childs_attributes: [
                  # manage stock adjustments
                  {
                    name: "Manage Stock Adjustment",
                    context: [::Main::InventoryManagement::Models::StockAdjustment],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Stock Adjustment",
                        context: [::Main::InventoryManagement::Models::StockAdjustment],
                        key: [:read]
                      },
                      {
                        name: "Create Stock Adjustment",
                        context: [::Main::InventoryManagement::Models::StockAdjustment],
                        key: [:create]
                      },
                      {
                        name: "Update Stock Adjustment",
                        context: [::Main::InventoryManagement::Models::StockAdjustment],
                        key: [:update]
                      },
                      {
                        name: "Publish Stock Adjustment",
                        context: [::Main::InventoryManagement::Models::StockAdjustment],
                        key: [:publish]
                      }
                    ]
                  },
                  # manage stocks
                  {
                    name: "Read Stock",
                    context: [::Main::InventoryManagement::Models::Stock],
                    key: [:read]
                  }
                ]
              },
              #manage sales management
              {
                name: "Manage Sales Management",
                context: [
                            ::Main::SalesManagement::Models::Sale, ::Main::SalesManagement::Models::Customer,
                            ::Main::SalesManagement::Models::Discount, ::Main::SalesManagement::Models::DiscountRedemption,
                            ::Main::SalesManagement::Models::Transaction, ::Main::SalesManagement::Models::TransactionDetail,
                            ::Main::SalesManagement::Models::VariantDiscount, ::Main::SalesManagement::Models::Payment
                         ],
                key: [:manage],
                categories: ["store_admin"],
                childs_attributes: [
                  #manage sales
                  {
                    name: "Manage Sales/Shifts",
                    context: [
                               ::Main::SalesManagement::Models::Sale
                             ],
                    key: [:manage],
                    categories: ["cashier"],
                    childs_attributes: [
                      {
                        name: "Read Sales/Shifts",
                        context: [
                                   ::Main::SalesManagement::Models::Sale
                                 ],
                        key: [:read]  
                      },
                      {
                        name: "Create Sale/Shift",
                        context: [
                                   ::Main::SalesManagement::Models::Sale
                                 ],
                        key: [:create]  
                      },
                      {
                        name: "Update Sale/Shift",
                        context: [
                                   ::Main::SalesManagement::Models::Sale
                                 ],
                        key: [:update]  
                      },
                      {
                        name: "Delete Sale/Shift",
                        context: [
                                   ::Main::SalesManagement::Models::Sale
                                 ],
                        key: [:delete]
                      },
                      {
                        name: "Complete Sale/Shift",
                        context: [
                                   ::Main::SalesManagement::Models::Sale
                                 ],
                        key: [:complete]  
                      },
                      {
                        name: "Cancel Transactions",
                        context: [
                                   ::Main::SalesManagement::Models::Sale, ::Main::SalesManagement::Models::TransactionDetail,
                                   ::Main::SalesManagement::Models::Transaction, ::Main::SalesManagement::Models::Payment
                                 ],
                        key: [:cancel]  
                      },
                      {
                        name: "Complete Transactions",
                        context: [
                                   ::Main::SalesManagement::Models::Sale, ::Main::SalesManagement::Models::TransactionDetail,
                                   ::Main::SalesManagement::Models::Transaction, ::Main::SalesManagement::Models::Payment
                                 ],
                        key: [:complete]  
                      }
                    ]
                  },
                  #manage transactions
                  {
                    name: "Manage Transactions",
                    context: [
                               ::Main::SalesManagement::Models::TransactionDetail,
                               ::Main::SalesManagement::Models::Transaction, ::Main::SalesManagement::Models::Payment
                             ],
                    key: [:manage],
                    categories: ["cashier"],
                    childs_attributes: [
                      {
                        name: "Read Transactions",
                        context: [
                                   ::Main::SalesManagement::Models::TransactionDetail,
                                   ::Main::SalesManagement::Models::Transaction, ::Main::SalesManagement::Models::Payment
                                 ],
                        key: [:read]  
                      },
                      {
                        name: "Create Transaction",
                        context: [
                                   ::Main::SalesManagement::Models::TransactionDetail,
                                   ::Main::SalesManagement::Models::Transaction, ::Main::SalesManagement::Models::Payment
                                 ],
                        key: [:create]  
                      },
                      {
                        name: "Update Transaction",
                        context: [
                                   ::Main::SalesManagement::Models::TransactionDetail,
                                   ::Main::SalesManagement::Models::Transaction, ::Main::SalesManagement::Models::Payment
                                 ],
                        key: [:update]  
                      },
                      {
                        name: "Delete Transactions",
                        context: [
                                   ::Main::SalesManagement::Models::TransactionDetail,
                                   ::Main::SalesManagement::Models::Transaction, ::Main::SalesManagement::Models::Payment
                                 ],
                        key: [:delete]
                      },
                      {
                        name: "Pay Transactions",
                        context: [
                                   ::Main::SalesManagement::Models::TransactionDetail,
                                   ::Main::SalesManagement::Models::Transaction, ::Main::SalesManagement::Models::Payment
                                 ],
                        key: [:pay]  
                      },
                      {
                        name: "Cancel Transactions",
                        context: [
                                   ::Main::SalesManagement::Models::TransactionDetail,
                                   ::Main::SalesManagement::Models::Transaction, ::Main::SalesManagement::Models::Payment
                                 ],
                        key: [:cancel]  
                      },
                      {
                        name: "Complete Transactions",
                        context: [
                                   ::Main::SalesManagement::Models::TransactionDetail,
                                   ::Main::SalesManagement::Models::Transaction, ::Main::SalesManagement::Models::Payment
                                 ],
                        key: [:complete]  
                      }
                    ]
                  },
                  #manage discounts
                  {
                    name: "Manage Discounts",
                    context: [
                               ::Main::SalesManagement::Models::Discount, ::Main::SalesManagement::Models::VariantDiscount
                             ],
                    key: [:manage],
                    categories: ["store_admin"],
                    childs_attributes: [
                      {
                        name: "Read Discounts",
                        context: [
                                   ::Main::SalesManagement::Models::Discount, ::Main::SalesManagement::Models::VariantDiscount
                                 ],
                        key: [:read]  
                      },
                      {
                        name: "Create Discount",
                        context: [
                                   ::Main::SalesManagement::Models::Discount, ::Main::SalesManagement::Models::VariantDiscount
                                 ],
                        key: [:create]  
                      },
                      {
                        name: "Update Discount",
                        context: [
                                   ::Main::SalesManagement::Models::Discount, ::Main::SalesManagement::Models::VariantDiscount
                                 ],
                        key: [:update]  
                      },
                      {
                        name: "Delete Discount",
                        context: [
                                   ::Main::SalesManagement::Models::Discount, ::Main::SalesManagement::Models::VariantDiscount
                                 ],
                        key: [:delete]
                      },
                      {
                        name: "Expire Discount",
                        context: [
                                   ::Main::SalesManagement::Models::Discount, ::Main::SalesManagement::Models::VariantDiscount
                                 ],
                        key: [:expire]
                      }
                    ]
                  },
                  #manage members
                  {
                    name: "Manage Members",
                    context: [
                               ::Main::SalesManagement::Models::Customer
                             ],
                    key: [:manage],
                    categories: ["store_admin"],
                    childs_attributes: [
                      {
                        name: "Read Members",
                        context: [
                                   ::Main::SalesManagement::Models::Customer
                                 ],
                        key: [:read]  
                      },
                      {
                        name: "Create Members",
                        context: [
                                   ::Main::SalesManagement::Models::Customer
                                 ],
                        key: [:create]  
                      },
                      {
                        name: "Update Members",
                        context: [
                                   ::Main::SalesManagement::Models::Customer
                                 ],
                        key: [:update]  
                      },
                      {
                        name: "Delete Members",
                        context: [
                                   ::Main::SalesManagement::Models::Customer
                                 ],
                        key: [:delete]  
                      }
                    ]
                  }
                ]
              },
              #returns management
              {
                name: "Manage Returns Management",
                context: [::Main::ReturnsManagement::Models::Return, ::Main::ReturnsManagement::Models::Refund, ::Main::ReturnsManagement::Models::Void],
                key: [:manage],
                categories: ["store_admin", "cashier"],
                childs_attributes: [
                  #manage retur
                  {
                    name: "Manage Retur",
                    context: [::Main::ReturnsManagement::Models::Return],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Retur",
                        context: [::Main::ReturnsManagement::Models::Return],
                        key: [:read]
                      },
                      {
                        name: "Create Retur",
                        context: [::Main::ReturnsManagement::Models::Return],
                        key: [:create]
                      },
                      {
                        name: "Update Retur",
                        context: [::Main::ReturnsManagement::Models::Return],
                        key: [:update]
                      },
                      {
                        name: "Reject Retur",
                        context: [::Main::ReturnsManagement::Models::Return],
                        key: [:reject]
                      },
                      {
                        name: "Accept Retur",
                        context: [::Main::ReturnsManagement::Models::Return],
                        key: [:accept]
                      },
                    ]
                  },
                  # manage refund
                  {
                    name: "Manage Refund",
                    context: [::Main::ReturnsManagement::Models::Refund],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Refund",
                        context: [::Main::ReturnsManagement::Models::Refund],
                        key: [:read]
                      },
                      {
                        name: "Create Refund",
                        context: [::Main::ReturnsManagement::Models::Refund],
                        key: [:create]
                      },
                      {
                        name: "Update Refund",
                        context: [::Main::ReturnsManagement::Models::Refund],
                        key: [:update]
                      },
                      {
                        name: "Reject Refund",
                        context: [::Main::ReturnsManagement::Models::Refund],
                        key: [:reject]
                      },
                      {
                        name: "Accept Refund",
                        context: [::Main::ReturnsManagement::Models::Refund],
                        key: [:accept]
                      },
                    ]
                  },
                  #manage voids
                  {
                    name: "Manage Voids",
                    context: [::Main::ReturnsManagement::Models::Void],
                    key: [:manage],
                    childs_attributes: [
                      {
                        name: "Read Voids",
                        context: [::Main::ReturnsManagement::Models::Void],
                        key: [:read]
                      },
                      {
                        name: "Create Void",
                        context: [::Main::ReturnsManagement::Models::Void],
                        key: [:create]
                      },
                      {
                        name: "Update Void",
                        context: [::Main::ReturnsManagement::Models::Void],
                        key: [:update]
                      },
                      {
                        name: "Reject Void",
                        context: [::Main::ReturnsManagement::Models::Void],
                        key: [:reject]
                      },
                      {
                        name: "Accept Void",
                        context: [::Main::ReturnsManagement::Models::Void],
                        key: [:accept]
                      },
                    ]
                  }
                ]
              }
            ]
          end

        end

      end
    end
  end
end
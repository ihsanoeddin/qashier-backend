#
# schema
#  create_table(:main_users_management_roles) do |t|
#    t.string :name
#    t.references :resource, :polymorphic => true, index: { name: 'resource_id_and_resource_type' }
#    t.datetime :deleted_at, index: true
#    t.timestamps
#  end
#
module Main
  module UsersManagement
    module Models
      class Role < ::ApplicationRecord

        #
        # include cacheable methods
        #
        include ::Supports::Cacheable::Cacheable
        self.object_caches_suffix= ['accesses']

        #
        # define users relationship by rolify
        #
        has_and_belongs_to_many :users, :join_table => :main_users_management_users_roles
        belongs_to :resource,
                   :polymorphic => true,
                   :optional => true

        validates :resource_type,
                  :inclusion => { :in => Rolify.resource_types },
                  :allow_nil => true
        #
        # define validation for merchant context
        #
        validates :name, presence: true, uniqueness: { scope: [:resource_id, :resource_type] }
        validates :resource_type, presence: true, on: :merchant
        validates :resource_id, presence: true, on: :merchant

        scopify

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :name
        end

        #
        # scopes
        #
        scope :valid_roles, -> { where.not(resource_type: nil, resource_id: nil) }

        #
        # define accesses relationship
        #
        has_and_belongs_to_many :accesses, :join_table => :main_users_management_roles_accesses

        #
        # determine resource type based on short string
        #
        before_validation :determine_resource_type!

        DEFAULT_ROLE_NAMES= [
                              "cashier",
                              "inventory",
                              "accounting",
                              "store_admin"
                            ]

        #
        # get cached role accesses
        #
        def cached_accesses
          Rails.cache.fetch("#{self.cached_name}-#{self.id}-accesses", expires_in: 1.month) do 
            self.accesses
          end
        end

        protected

          #
          # determine resource type
          #
          def determine_resource_type!
            case resource_type
            when "shop", "store", "warehouse"
              self.resource_type= ::Main::StoresManagement::Models::Store
            end
          end

      end
    end
  end
end
#
# sidekiq worker for saving balance insertion object
#

require 'json'

module Main
  module Accounting
    module Workers
      class BalanceInsertion

        include ::Sidekiq::Worker
        
        def perform(balance_insertion_params)
          Array(JSON.parse(balance_insertion_params)).flatten.each do |params|
            ::Main::Accounting::Models::Balance.new(params).save
          end
        end

      end
    end
  end
end
#
# schema
#
#create_table :main_accounting_management_balances do |t|
#  t.references :account, index: { name: "main_accounting_management_balances_account_id" }
#  t.references :reference, polymorphic: true, index: { name: "main_accounting_management_balances_reference_type_and_id"}
#  t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2 
#  t.string :type 
#  t.text :notes
#  t.datetime :time
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module Accounting
    module Models
      module BalanceTypes
        class Credit < ::Main::Accounting::Models::Balance

          #
          # include cacheable
          #
          include ::Supports::Cacheable::Cacheable

        end
      end
    end
  end
end
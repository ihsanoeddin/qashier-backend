#
# schema
# create_table :main_accounting_management_accounts do |t|
#  t.string :name
#  t.string :code
#  t.references :group
#  t.text :description
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module Accounting
    module Models
      class Account < ::ApplicationRecord

        STATIC_ACCOUNTS={ 
                          cash: { name: "Cash", code: 'cash', identifing_name: 'cash', static: true}, 
                          sales: { name: 'Sales', code: 'sales', identifing_name: 'sales', static: true },
                          cost_of_sales: { name: 'Cost of Sales', code: 'cost_of_sales', identifing_name: 'cost_of_sales', static: true },
                          inventory: { name: "Inventory", code: 'inventory', identifing_name: 'inventory', static: true },
                          ppn: { name: 'PPN', code: 'ppn', identifing_name: 'ppn', static: true },
                          rejected_inventory: { name: "Rejected Inventory", code: "r_inventory", identifing_name: "r_inventory", static: true },
                          refund: { name: "Refund", code: "refund", identifing_name: "refund", static: true },
                          fund: { name: "Equity", code: "equity", identifing_name: "equity", static: true }
                        }

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # define relationships
        #
        has_many :balances, class_name: "::Main::Accounting::Models::Balance", foreign_key: :account_id, dependent: :destroy, inverse_of: :account
        belongs_to :group, class_name: self.name, foreign_key: :group_id
        has_many :sub_accounts, class_name: self.name, foreign_key: :group_id, inverse_of: :group
        #
        # cache relationships
        #
        cache_has_many :balances, inverse_name: :account
        cache_has_many :sub_accounts, inverse_name: :group
        cache_belongs_to :group, inverse_name: :accounts

        #
        # delegations
        #
        delegate :name, to: :group, prefix: true

        #
        # validations
        #
        validates :name, presence: true
        validates :code, presence: true, uniqueness: true

        #
        # callbacks
        #
        before_destroy :only_static

        #
        # use search cop
        #
        include SearchCop
        search_scope :search do
          attributes :name, :code
        end

        class << self

          #
          # to filter records
          #
          def filter(params={})
            res = cached_collection
            if params[:common].present?
              res = res.search(params[:common])
            end
            res
          end

          #
          # define static object helpers
          #
          def static_object_helpers
            STATIC_ACCOUNTS.each do |key, val|
              class_eval %{
                def self.get_#{key.to_s}
                  self.find_by_identifing_name("#{key.to_s}") || self.create(STATIC_ACCOUNTS["#{key.to_s}".to_sym])
                end
              }
            end
          end

        end

        #
        # declare static object helpers
        #
        static_object_helpers

        protected

          #
          # prevent static object deletion
          #
          def only_static
            if self.static
              erros.add(:name, "can not delete default object")
              throw(:abort)
            end
          end

      end
    end
  end
end
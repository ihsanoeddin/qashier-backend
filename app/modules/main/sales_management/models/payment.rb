#
# schema
#create_table :main_sales_management_payments do |t|
#
#  t.string :name
#  t.references :transaction, index: { name: "main_sales_management_payment_transaction_id" }
#  t.string :state
#  t.text :details
#  t.string :type
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Main
  module SalesManagement
    module Models
      class Payment < ::ApplicationRecord

        include ::Supports::Trackable::Interfaces::HasTracks
        track!
        
        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # relationships definitions
        #
        belongs_to :transaksi, class_name: '::Main::SalesManagement::Models::Transaction', foreign_key: :transaction_id

        #
        # cache relationships
        #
        cache_belongs_to :transaksi, inverse_name: :payments

        #
        # serialize details
        #
        serialize :details, Hash

        #
        # constants
        #
        BANK_TRANSFER   = 'bank_transfer'
        CASH            = 'cash'
        CARD            = 'card'
        TYPES           = [BANK_TRANSFER, CARD, CASH]

        #
        # callbacks
        #
        after_initialize :set_details, unless: :persisted?
        after_validation :initialize_context, if: :new_record?

        #
        # validations
        #
        validates :payment_type, presence: true, inclusion: { in: TYPES , allow_blank: true}

        #
        # attr accessors
        #
        attr_accessor :payment_type

        #
        # use search cop
        #
        include SearchCop
        search_scope :search do
          attributes :name
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection
            if params[:common]
              res = res.search(params[:common])
            end
            res
          end

        end

        protected

          #
          # change class context
          #
          def initialize_context
            begin
            return self.becomes!("::Main::SalesManagement::Models::PaymentTypes::#{self.payment_type.classify}".constantize)
            rescue => e
              Rails.logger.info e.backtrace
            end
          end

          #
          # override this method on its subclasses
          #
          def set_details
            self.details= {}   
          end


      end
    end
  end
end

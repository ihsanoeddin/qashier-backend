#
# schema
#create_table :main_sales_management_customers do |t|
#
#  t.string :name
#  t.string :type
#  t.string :number
#  t.string :phone_number
#  t.string :id_number
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Main
  module SalesManagement
    module Models
      module CustomerTypes
        class Member < ::Main::SalesManagement::Models::Customer

          #
          # include cacheable
          #
          #include ::Supports::Cacheable::Cacheable

          #
          # define relationships
          #
          #has_many :transactions, class_name: "::Main::SalesManagement::Models::Transaction", foreign_key: :customer_id
          #has_many :addresses, as: :addressable, class_name: '::Supports::Addressable::Models::Address', :dependent => :destroy
          #accepts_nested_attributes_for :addresses, allow_destroy: true, reject_if: :all_blank

          #
          # cache relationships
          #
          #cache_has_many :transactions, inverse_name: :customer
          #cache_has_many :addresses, inverse_name: :addressable

          #
          # validations
          #
          validates :name, presence: true
          validates :number, presence: true, uniqueness: true
          validates :id_number, presence: true

          #
          # callbacks
          #
          before_validation :set_number

          #
          # include search cop
          #
          include SearchCop
          search_scope :search do
            attributes :name, :number, :id_number, :phone_number
          end

          #
          # set number uniq
          #
          def set_number
            self.number ||= loop do
              random_str = "#{::Apartment::Tenant.current.capitalize}-MBR-#{SecureRandom.urlsafe_base64(10)}"
              break random_str unless self.class.exists?(number: random_str)
            end  
          end

          protected :set_number



        end
      end
    end
  end
end

#
# schema
#create_table :main_sales_management_sales do |t|
#
#    t.references :store, index: { name: "main_sales_management_sales_store_id" }
#    t.string :name
#    t.string :state
#    t.datetime :started_at
#    t.datetime :finished_at
#    t.datetime :deleted_at, index: true
#    t.timestamps null: false
#
#end
#

module Main
  module SalesManagement
    module Models
      class Sale < ::ApplicationRecord
        
        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # include has balances
        #
        include ::Main::Accounting::Interfaces::HasBalances

        #
        # @Overriding params for balance insertion
        #
        def balance_insertion_params
          [ 
            { 
              journal_identifing_name: 'inventory_expenditure', account_identifing_name: 'fund', amount: self.fund, balance_type: 'debt', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.fund_notes, 
              parts_attributes: [ {journal_identifing_name: 'inventory_expenditure', account_identifing_name: 'cash', amount:  self.fund, balance_type: 'credit', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.fund_notes } ]
            }    
          ]
        end

        #
        # relationships
        #
        include ::Main::StoresManagement::Interfaces::BelongsToStore
        has_many :transactions, class_name: "::Main::SalesManagement::Models::Transaction", foreign_key: :sale_id
        belongs_to :operator, class_name: "::Main::UsersManagement::Models::User", foreign_key: :operator_id

        #
        # cache relationships
        #
        cache_belongs_to :store, inverse_name: :sales
        cache_has_many :transactions, inverse_name: :sale

        #
        # stateful model
        #
        include ::AASM
        aasm column: :state do
          state :started, initial: true
          state :finished
          event :complete, after: :stamp_finished_at do 
            transitions from: :started, to: :finished
          end
        end

        #
        # validations
        # 
        validates :started_at, presence: true, date: { allow_blank: true }
        validates :name, presence: true
        validates :store, presence: true

        #
        # callbacks
        #
        before_validation :set_name
        before_validation :set_started_at
        #before_update :valid_to_update?, unless: :state_changed?
        before_destroy :valid_to_update?
        after_initialize :fill_fund
        after_save :set_balance_fund

        #
        # attr accessors
        #
        attr_accessor :fund, :fund_notes # to generate balance

        #
        # fill_fund
        # fill fund_notes
        #
        def fill_fund
          self.fund = self.balances.where(type: "Main::Accounting::Models::BalanceTypes::Debt").inject(0){ |sum, debt| sum += debt.amount } 
          self.fund_notes = self.balances.where(type: "Main::Accounting::Models::BalanceTypes::Debt").first.try(:notes)
        end

        #
        #
        #
        def set_balance_fund
         self.balances.present?? update_balance_fund : generate_balance_fund 
        end

        #
        # create balances/capital
        #
        def generate_balance_fund
          self.balances.create(balance_insertion_params)
        end

        def update_balance_fund
          self.balances.update_all(amount: self.fund, notes: self.fund_notes)
        end

        #
        # set name
        #
        def set_name
          self.name||= "Sales at #{Date.today}"
        end

        #
        # set started at
        #
        def set_started_at
          self.started_at||= DateTime.now
        end
        protected :set_name, :set_started_at

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection
            if params[:store_id]
              res = res.where(store_id: params[:store_id])
            end
            if params[:started_time]
              started_time= DateTime.parse(params[:started_time]) rescue nil
              res = res.where("started_at >= ?", started_time) if started_time
            end
            if params[:finish_time]
              finish_time= DateTime.parse(params[:finish_time]) rescue nil
              res = res.where("finished_at <= ?", finish_time) if finish_time
            end
            if params[:state]
              res= res.where(state: params[:state])
            end
            res
          end

        end

        protected

          #
          # stamp `finished_at` attribute
          #
          def stamp_finished_at
            self.update_attribute :finished_at, DateTime.now
          end

          #
          # method to apply on callback to authorize update
          #
          def valid_to_update?
            unless started?
              errors.add(:state, "can not update or delete")
              throw(:abort)
            end
          end

      end
    end
  end
end

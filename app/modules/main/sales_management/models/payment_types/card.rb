#
# schema
#create_table :main_sales_management_payments do |t|
#
#  t.string :name
#  t.references :transaction, index: { name: "main_sales_management_payment_transaction_id" }
#  t.string :state
#  t.text :details
#  t.string :type
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Main
  module SalesManagement
    module Models
      module PaymentTypes
        class Card < ::Main::SalesManagement::Models::Payment
          
          #
          # include cacheable
          #
          include ::Supports::Cacheable::Cacheable

        end
      end
    end
  end
end

#
# schema
#create_table :main_sales_management_transactions do |t|
#
#  t.references :store, index: { name: "main_sales_management_transactions_store_id" }
#  t.references :sale, index: { name: "main_sales_management_transactions_sale_id" } }
#  t.references :customer, index: { name: "main_sales_management_transactions_customer_id" }
#  t.string :state
#  t.string :number
#  t.string :barcode
#  t.string :type
#  t.text :notes
#  t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
#  t.decimal :cost, :default => 0.0, :precision => 15, :scale => 2
#  t.decimal :discount_amount, default: 0.0, precision: 15, scale: 2
#  t.decimal :tax_rate, :default => 0.0, :precision => 15, :scale => 2
#  t.datetime :time, index: true
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

require 'securerandom'

module Main
  module SalesManagement
    module Models
      class Transaction < ::ApplicationRecord

        include ::Supports::Trackable::Interfaces::HasTracks
        track!

        #
        # include has balances
        #
        include ::Main::Accounting::Interfaces::HasBalances
        insert_balance! depend_on_changes_of: { state: "completed" }

        #
        # @Overriding params for balance insertion
        #
        def balance_insertion_params
          [ 
            { 
              journal_identifing_name: 'cash_revenue', account_identifing_name: "cash", amount: self.amount_after_tax, balance_type: 'debt', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.notes,
              parts_attributes: [ 
                {journal_identifing_name: 'cash_revenue', account_identifing_name: 'sales', amount:  self.total_tax, balance_type: 'credit', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.notes }, 
                {journal_identifing_name: 'cash_revenue', account_identifing_name: 'sales', amount:  self.amount_after_discount, balance_type: 'credit', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.notes, } 
              ]
            },
            { 
              journal_identifing_name: 'inventory_expenditure', account_identifing_name: 'cost_of_sales', amount: self.cost, balance_type: 'debt', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.notes, 
              parts_attributes: [ {journal_identifing_name: 'inventory_expenditure', account_identifing_name: 'inventory', amount:  self.cost, balance_type: 'credit', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.notes } ]
            }    
          ]
        end
        
        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # relationships
        #
        include ::Main::StoresManagement::Interfaces::BelongsToStore
        belongs_to :sale, class_name: "::Main::SalesManagement::Models::Sale", foreign_key: :sale_id
        belongs_to :customer, class_name: "::Main::SalesManagement::Models::Customer", foreign_key: :customer_id, optional: true
        has_many :details, class_name: "::Main::SalesManagement::Models::TransactionDetail", :inverse_of => :transaksi, foreign_key: :transaction_id
        has_many :payments, class_name: "::Main::SalesManagement::Models::Payment", :inverse_of => :transaksi, foreign_key: :transaction_id
        has_many :discount_redemptions, class_name: "::Main::SalesManagement::Models::DiscountRedemption", as: :redeemable
        has_many :returns, class_name: "::Main::ReturnsManagement::Models::Return", foreign_key: :transaction_id, inverse_of: :transaksi
        has_many :refunds, class_name: "::Main::ReturnsManagement::Models::Refund", foreign_key: :transaction_id, inverse_of: :transaksi
        has_many :voids, class_name: "::Main::ReturnsManagement::Models::Void", foreign_key: :transaction_id, inverse_of: :transaksi

        #
        # allow nested attributes for
        #
        accepts_nested_attributes_for :payments, allow_destroy: true, reject_if: :all_blank
        accepts_nested_attributes_for :details, allow_destroy: true, reject_if: :all_blank
        accepts_nested_attributes_for :discount_redemptions, reject_if: :all_blank

        #
        # cache relationships
        #
        cache_belongs_to :sale, inverse_name: :transactions
        cache_belongs_to :customer, inverse_name: :transactions
        cache_belongs_to :store, :inverse_name => :transactions
        cache_has_many :payments, inverse_name: :transaksi
        cache_has_many :details, inverse_name: :transaksi
        cache_has_many :discount_redemptions, inverse_name: :redeemable
        cache_has_many :returns, inverse_name: :transaksi
        cache_has_many :refunds, inverse_name: :transaksi
        cache_has_many :voids, inverse_name: :transaksi

        #
        # stateful model
        #
        include ::AASM
        aasm column: :state do
          state :in_progress, initial: true
          state :paid
          state :completed
          state :canceled
          event :pay, guard: :payments_valid?, after: :after_paying_transaction do
            transitions from: :in_progress, to: :paid
          end
          event :cancel, after: :after_canceling_transaction do 
            transitions from: :in_progress, to: :canceled
          end
          event :complete, after: :after_completing_transaction do 
            transitions from: :paid, to: :completed
          end
        end

        #
        # validations
        #
        validates :number, presence: true, uniqueness: { scope: :deleted_at, allow_blank: true }
        validate :valid_transaction?
        validates :store, presence: true
        validates :sale, presence: true

        #
        # attr_accessor
        #
        attr_accessor :exclude_ppn, :discount_ids

        #
        # check whether transaction is allowed to happen
        #
        def valid_transaction?
          self.errors.add(:amount, "sale is already completed" ) if new_record? && sale.finished?
        end
        protected :valid_transaction?


        #
        # callbacks
        #
        before_validation :set_time
        before_save :calculate_amount
        before_save :calculate_cost
        before_save :calculate_discount
        before_save :calculate_tax_rate
        before_validation :set_store_id
        before_validation :set_number

        #
        # set store id from sale
        #
        def set_store_id
          self.store_id||= self.sale.try(:store_id)
        end

        #
        # generate number
        # number is constructed from current tenant name + store short name + random string
        #
        def set_number
          self.number ||= loop do
            prop_num = "#{store.try(:short_name)}-#{DateTime.now.to_i}"
            break prop_num unless self.class.exists?(number: prop_num)
          end
        end

        #
        # calculate amount based on details amounts
        #
        def calculate_amount
          self.amount= fetch_details.inject(0){ |sum, detail| sum += detail.deleted?? 0 : detail.amount }
        end

        #
        # calculate cost based on details cost
        #
        def calculate_cost
          self.cost= fetch_details.inject(0){ |sum, detail| sum += detail.deleted?? 0 : detail.cost }
        end

        #
        # get list available_discounts
        #
        def available_discounts
          @available_discounts||= if persisted?
                                    discount_redemptions.map{|discount_redemption| discount_redemption.fetch_discount || discount_redemption.discount }.compact.uniq
                                  else
                                    if self.discount_ids.present?
                                      Array(self.discount_ids).flatten.uniq.compact.map do |discount_id|
                                        ::Main::SalesManagement::Models::Discount.fetch(discount_id) rescue nil 
                                      end
                                    else
                                      ::Main::SalesManagement::Models::DiscountTypes::Transaction.available_discounts || []
                                    end
                                  end
        end

        #
        # get total discount
        #
        def total_discount_details
          (fetch_details || details).inject(0){ |sum, detail| sum + detail.discount_amount}
        end

        #
        # calculate discount amount
        # if concurrent discount present with another unconcurrent discount disregard it
        # return discount amount of transaction
        #
        def calculate_discount
          self.discount_amount= available_discounts.compact.inject(total_discount_details) do |sum, discount| 
            sum += discount.apply(discount_options)# if discount.concurrent or (available_discounts.compact.length == 1) 
          end
        end

        #
        # define discount options
        #
        def discount_options
          { discount_object: self }
        end

        #
        # check whether it has discount
        #
        def got_discount?
          fetch_discount_redemptions.present?
        end

        #
        # calculate total tax
        # use PPN 
        #
        def total_tax
          amount_after_discount * (self.tax_rate/100)
          #(fetch_details || details).inject(0){ |sum, detail| sum + detail.total_tax}
        end

        #
        # amount after discount
        #
        def amount_after_discount
           self.amount - self.discount_amount
        end

        #
        # get amount after tax
        #
        def amount_after_tax
          amount_after_discount + total_tax
        end

        #
        # ppn tax rate
        #
        def ppn
          ::Main::ProductsManagement::Models::TaxRate.ppn
        end

        #
        # set tax rate
        #
        def calculate_tax_rate
          unless self.try(:exclude_tax) || self.exclude_ppn 
            self.tax_rate= ppn.try(:rate) || 0 
          end
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection
            if params[:from_time].present?
              from_time= DateTime.parse(params[:from_time]) rescue nil
              res = res.where("created_at >= ?", from_time) if from_time
            end
            if params[:to_time].present?
              to_time= DateTime.parse(params[:to_time]) rescue nil
              res = res.where("created_at <= ?", to_time) if to_time
            end
            if params[:state].present?
              res= res.where(state: params[:state])
            end
            if params[:number].present?
              res = res.where(number: params[:number])
            end
            if params[:store_id].present?
              res = res.where(store_id: params[:store_id])
            end
            if params[:sale_id].present?
              res = res.where(sale_id: params[:sale_id])
            end
            if params[:customer_id].present?
              res = res.where(customer_id: params[:customer_id])
            end
            if params[:operator_id].present?
              res = res.joins(:sale).where(:main_sales_management_sales => { operator_id: params[:operator_id] })
            end
            res
          end

        end

        protected

          #
          # insert DateTime.now if object#time value is blank
          #
          def set_time
            self.time||= DateTime.now
          end

          #
          # create discount redemptions if discount available or discount_ids presents
          # this method is called on after_save callback if object#state is `in_progress`
          #

          after_save :create_discount_redemptions, :if => :in_progress?

          def create_discount_redemptions
            if available_discounts.compact.present?
              available_discounts.compact.each{|discount| discount.create_redemption(redeemable: self) }
            end
          end

          #
          # check whether payments presents
          #
          def payments_valid?
            (fetch_payments || payments) && ( fetch_details.present? || details.present?)
          end

          #
          # callback after events
          #
          def after_paying_transaction
            update_details_states
          end

          def after_completing_transaction
            update_details_states
            update_redemption_discounts
          end

          def after_canceling_transaction
            update_details_states
          end

          #
          # end callbacks after events
          #

          #
          # update all details state based on self#state
          #
          def update_details_states
            details.each { |detail| detail.update(:state => self.state) }
          end

          #
          # update redemption discounts states
          #
          def update_redemption_discounts
           (fetch_discount_redemptions || discount_redemptions).each{|redemption| redemption.redeem! } 
          end


      end
    end
  end
end

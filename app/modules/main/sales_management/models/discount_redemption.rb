#
# schema
#create_table :main_sales_management_discount_redemptions do |t|
#
#  t.references :discount, index: { name: "main_sales_management_discount_redemptions_discount_id" }
#  t.references :redeemable, polymorphic: true, index: { name: "main_sales_management_discount_redemptions_redeemable_type_and_redeemable_id" }
#  t.string :state
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Main
  module SalesManagement
    module Models
      class DiscountRedemption < ::ApplicationRecord
        
        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # define relationships
        #
        belongs_to :discount, class_name: "::Main::SalesManagement::Models::Discount", foreign_key: :discount_id#, counter_cache: :discount_redemptions_count
        belongs_to :redeemable, polymorphic: true

        #
        # cache relationships
        # 
        cache_belongs_to :discount, inverse_name: :discount_redemptions
        cache_belongs_to :redeemable

        #
        # stateful model
        #
        include ::AASM
        aasm column: :state do
          state :draft, initial: true
          state :redeemed
          event :redeem do
            transitions from: :draft, to: :redeemed
          end
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection
            if params[:discount_id]
              res = res.where(discount_id: params[:discount_id])
            end
            if params[:redeemable_type]
              redeemable_type=  case params[:redeemable_type]
                                when "transaction"
                                  "Main::SalesManagement::Models::Transaction"
                                when "transaction_detail"
                                  "Main::SalesManagement::Models::TransactionDetail"
                                end
              res = res.where(redeemable_type: redeemable_type)
            end
            if params[:redeemable_id]
              res = res.where(redeemable_id: params[:redeemable_id])
            end
            res
          end

        end

      end
    end
  end
end

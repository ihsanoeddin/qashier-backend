#
# schema
#create_table :main_sales_management_discounts do |t|
#
#  t.references :store, index: { name: "main_sales_management_discounts_store_id" }
#  t.string :code
#  t.text :description
#  t.date :valid_from
#  t.date :valid_until
#  t.integer :redemption_limit, default: 0
#  t.integer :discount_redemptions_count, default: 0
#  t.decimal :amount, default: 0
#  t.string :based_on
#  t.string :discount_orientation
#  t.decimal :discount_orientation_amount, :default => 0.0, :precision => 15, :scale => 2
#  t.string :type
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Main
  module SalesManagement
    module Models
      module DiscountTypes
        class Transaction < ::Main::SalesManagement::Models::Discount
          
          #
          # include cacheable
          #
          #include ::Supports::Cacheable::Cacheable

          #
          # relationships definitions
          #
          #belongs_to :store, class_name: "::Main::StoresManagement::Models::Store", foreign_key: :store_id, optional: true
          #has_many :discount_redemptions, class_name: "::Main::SalesManagement::Models::DiscountRedemption", foreign_key: :discount_id

          #
          # cache relationships
          #
          #cache_belongs_to :store, :inverse_name => :discount_items

          validates :discount_orientation, presence: true, inclusion: { in: %w{ amount member }, allow_blank: true }
          validates :discount_orientation_amount, presence: true, numericality: { greater_than_or_equal_to: 0 }, if: :amount_oriented?

          #
          # check whether discount orientation based on amount
          #
          def amount_oriented?
            discount_orientation == 'amount'
          end

          #
          # check whether discount orientation based on member
          #
          def member_oriented?
            discount_orientation == 'member'
          end

          #
          # @Override from parent class
          # return amount of discounted amount
          # return zero if condition is not satisfied
          #
          def apply(options={})
            discount_object = options[:discount_object]
            raise ArgumentError unless discount_object.is_a?(::Main::SalesManagement::Models::Transaction)
            discount_amount = BigDecimal(percentage_based? ? percentage_discount(discount_object.amount) : amount)
            #create_redemption(redeemable: discount_object, discount: self) do 
              if self.redeemable?
                if amount_oriented? && (discount_orientation_amount < discount_object.amount)
                  discount_amount= discount_amount
                elsif member_oriented?
                  member= discount_object.fetch_customer || discount_object.customer
                  discount_amount= member.is_a?(::Main::SalesManagement::Models::CustomerTypes::Member) ? discount_amount : 0
                end
              else
                discount_amount= 0
              end
            #end
            discount_amount
          end

        end
      end
    end
  end
end

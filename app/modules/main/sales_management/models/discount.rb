#
# schema
#create_table :main_sales_management_discounts do |t|
#
#  t.references :store, index: { name: "main_sales_management_discounts_store_id" }
#  t.string :code
#  t.text :description
#  t.date :valid_from
#  t.date :valid_until
#  t.integer :redemption_limit, default: 0
#  t.integer :discount_redemptions_count, default: 0
#  t.decimal :amount, default: 0
#  t.string :based_on
#  t.string :discount_orientation
#  t.decimal :discount_orientation_amount, :default => 0.0, :precision => 15, :scale => 2
#  t.boolean :concurrent, default: false
#  t.string :type
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Main
  module SalesManagement
    module Models
      class Discount < ::ApplicationRecord
        
        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable
        self.caches_suffix_list= ['collection', "#{name.demodulize.underscore}-available-discounts", "transaction-available-discounts", "item-available-discounts"]

        #
        # relationships definitions
        #
        belongs_to :store, class_name: "::Main::StoresManagement::Models::Store", foreign_key: :store_id, optional: true
        has_many :discount_redemptions, class_name: "::Main::SalesManagement::Models::DiscountRedemption", foreign_key: :discount_id
        has_many :variant_discounts, class_name: "::Main::SalesManagement::Models::VariantDiscount", foreign_key: :discount_id
        has_many :variants, through: :variant_discounts

        #
        # cache relationships
        #
        cache_belongs_to :store, inverse_name: :discounts
        cache_has_many :discount_redemptions, inverse_name: :discount
        cache_has_many :variant_discounts, inverse_name: :discount

        #
        # validations
        #
        validates :code, presence: true, uniqueness: { scope: :deleted_at, allow_nil: true }
        validates :valid_from, date: true
        validates :valid_until, date: { after: :valid_from }
        validates :based_on, inclusion: { in: %w[percentage amount] }
        validates :redemption_limit, presence: true, numericality: { allow_nil: true, less_than_or_equal_to: 0 }
        validates :amount, numericality: {greater_than_or_equal_to: 0, less_than_or_equal_to: 100}, if: :percentage_based?
        validates :amount, numericality: {greater_than_or_equal_to: 0, only_integer: true }, if: :amount_based?
        validates :discount_type, presence: true, inclusion: { in: %w{item transaction}, allow_blank: true }

        #
        # scopes
        #
        scope :available, -> { where("date(valid_until) >= ? and date(valid_from) <= ?",Date.today,Date.today) }

        #
        # attr accessors
        #
        attr_accessor :discount_type

        #
        # use search cop
        #
        include SearchCop
        search_scope :search do
          attributes :name, :code
        end

        #
        #
        #
        def item_type?
          self.type.to_s.demodulize.underscore == 'item' 
        end

        #
        # @Override on sub class if needed
        #
        def apply(discount_object)
         raise "Must be implemented!" 
        end

        #
        # check whether discount#based_on percentage
        #
        def percentage_based?
          based_on == 'percentage'
        end

        #
        # check whether discount#based_on percentage 
        #
        def amount_based?
          based_on == 'amount'
        end

        #
        # sugar method
        #
        def redemptions_count
          discount_redemptions_count
        end

        #
        # check whether discount is expired
        #
        def expired?
          valid_until <= Date.current
        end

        #
        # check whether discount can be applied
        #
        def has_available_redemptions?
          redemptions_count.zero? || redemptions_count < redemption_limit
        end

        #
        # check whether discount is started ?
        #
        def started?
          valid_from <= Date.current
        end

        #
        # check whether discount can be redeemed ?
        #
        def redeemable?
          !expired? && has_available_redemptions? && started?
        end

        #
        # calculate amount of percentage discount
        #
        def percentage_discount(input_amount)
          BigDecimal("#{input_amount}") * (BigDecimal("#{amount}") / 100)
        end

        #
        # create redemptions
        # if yield return  value > 0
        #
        def create_redemption(options={})
          #if yield > 0
            #masalah anjing
            discount_redemptions.find_or_create_by(options)
            self.update_attribute(:discount_redemptions_count, self.discount_redemptions_count + 1) unless redemption_limit.zero?
          #end
        end

        #
        # check whether discount is on store
        #
        def included_on_store?(pstore_id=nil)
          if store.present?
            pstore_id == self.store_id
          else
            return true
          end
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection
            if params[:common]
              res = res.search(params[:common])
            end
            if params[:valid_from]
              valid_from= Date.parse(params[:valid_from]) rescue nil
              res = res.where("valid_from >= ?", valid_from) if valid_from
            end
            if params[:valid_until]
               valid_until= Date.parse(params[:valid_until]) rescue nil
              res = res.where("valid_until <= ?", valid_until) if valid_until
            end
            if params[:redemption_limit]
              res = res.where(redemption_limit: params[:redemption_limit])
            end
            if params[:discount_redemptions_count]
            end
            if params[:based_on]
              res = res.where(based_on: params[:based_on])
            end
            if params[:amount]
              res = res.where(amount: params[:amount])
            end
            res
          end

          #
          # get all valid discounts
          #
          def available_discounts
            Rails.cache.fetch("#{cached_name}-#{name.demodulize.underscore}-available-discounts", expires_in: 1.day) do 
              discs = available.reject{|discount| !discount.redeemable? }
              discs || nil
            end
          end

        end

      end
    end
  end
end

#
# schema
#create_table :main_inventory_management_stocks do |t|
#  t.references :store, index: { name: "main_inventory_management_stocks_store_id" }
#  t.references :variant, index: { name: "main_inventory_management_stocks_variant_id" }
#  t.integer :count_on_store,               :default => 0
#  t.integer :count_pending_to_customer,   :default => 0
#  t.integer :count_pending_from_supplier, :default => 0
#  t.string :type
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module InventoryManagement
    module Models
      class Stock < ::ApplicationRecord

        #
        # cache relationship
        # generate cached collection
        #
        include ::Supports::Cacheable::Cacheable
        
        #
        # define relationships
        #
        #::Main::StoresManagement::Interfaces::BelongsToStore
        belongs_to :store, class_name: '::Main::StoresManagement::Models::Store', foreign_key: :store_id
        #::Main::ProductsManagement::Interfaces::BelongsToVariant
        belongs_to :variant, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: "variant_id"
        has_many :stock_adjustments, class_name: "::Main::InventoryManagement::Models::StockAdjustment", foreign_key: :stock_id
        
        cache_belongs_to :store, inverse_name: :stocks#, embed: true
        cache_belongs_to :variant, inverse_name: :stocks#, embed: true
        cache_belongs_to :variant, inverse_name: :sale_stocks
        cache_belongs_to :variant, inverse_name: :rejected_stocks
        cache_has_many :stock_adjustments, inverse_name: :stock

        OUT_OF_STOCK_QTY        = 1
        LOW_STOCK_QTY           = 6

        #
        # validations
        #
        validate :must_have_stock

        #
        # check whether stock is on low state
        #
        def low?
          (quantity_available) <= LOW_STOCK_QTY
        end

        #
        # check whether stock is on out state
        #
        def out?
           (quantity_available) < OUT_OF_STOCK_QTY
        end

        #
        # get quantity available
        #
        def quantity_available
          (count_on_store - count_pending_to_customer)
        end

        class << self

          #
          # find stock of product on a store
          #
          def find_stock_for(product,store)
            cached_collection.where(product_id: product.id, store_id: store.id) 
          end

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection

            if params[:count_on_store].present?
              res = res.where(count_on_store: params[:count_on_store].to_i)
            end
            if params[:count_pending_to_customer].present?
              res = res.where(count_pending_to_customer: params[:count_pending_to_customer].to_i)
            end
            if params[:count_pending_from_supplier].present?
              res = res.where(count_pending_from_supplier: params[:count_pending_from_supplier].to_i)
            end
            if params[:store_id].present?
              res = res.where(store_id: params[:store_id])
            end

            if params[:variant_id].present?
              res = res.where(:variant_id => params[:variant_id])
            end

            if params[:from_quantity_available].present?
              res = res.where("#{self.table_name}.count_on_store - #{self.table_name}.count_pending_to_customer >= ?", params[:from_quantity_available].to_i)
            end

            if params[:to_quantity_available].present?
              res = res.where("#{self.table_name}.count_on_store - #{self.table_name}.count_pending_to_customer <= ?", params[:to_quantity_available].to_i)
            end

            if params[:type].present?
              res = res.where(type: params[:type])
            end

            res
          end

          def cached_collection(options={ condition: {} })
            if options[:condition].present?
              Rails.cache.delete(self.cached_name)
            end
            Rails.cache.fetch("#{self.cached_name}-collection", expires_in: 1.day) do 
              self.where(options[:condition]).load
            end
          end

        end

        protected

          #
          # ensure stock is present
          #
          def must_have_stock
            if (count_on_store - count_pending_to_customer) < 0
              errors.add :count_on_store, 'There is not enough stock to sell this item'
            end
          end

      end
    end
  end
end
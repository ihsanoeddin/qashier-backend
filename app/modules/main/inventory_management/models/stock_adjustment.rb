#
# schema
#
# create inventories_adjusment
#
#create_table :main_inventory_management_stock_adjustments do |t|
#  t.references :stock, index: { name: "main_inventory_management_stock_adjustments_stock_id" }
#  t.references :reference, polymorphic: true, index: { name: "main_inventory_management_stock_adjustments_reference" }
#  t.integer :count, :default => 0
#  t.string :on
#  t.string :type
#  t.string :stock_type
#  t.datetime :deleted_at, index: true
#  t.timestamps
#end
#

module Main
  module InventoryManagement
    module Models
      class StockAdjustment < ::ApplicationRecord

        #
        # include has balances
        #
        include ::Main::Accounting::Interfaces::HasBalances
        insert_balance! depend_on_changes_of: { state: "published" }, when: :after_save

        #
        # @Overriding params for balance insertion
        #
        def balance_insertion_params
          #return if @balance_insertion_params
          #@balance_insertion_params||= 
          if reference.blank?
            if sale_stock?
              [ 
                { 
                  journal_identifing_name: 'purchase', account_identifing_name: "inventory", amount: self.calculate_cost, balance_type: 'debt', reference_type: self.class.name, reference_id: self.id, time: self.created_at || DateTime.now,
                  parts_attributes: [ 
                    { journal_identifing_name: 'purchase', account_identifing_name: 'ppn', amount:  self.calculate_tax , balance_type: 'debt', reference_type: self.class.name, reference_id: self.id, time: self.created_at || DateTime.now },
                    { journal_identifing_name: 'purchase', account_identifing_name: "cash", amount: self.calculate_cost + self.calculate_tax, balance_type: 'credit', reference_type: self.class.name, reference_id: self.id, time: self.created_at || DateTime.now } 
                  ]
                }
              ]
            end
            if rejected_stock?
              [
                {
                  journal_identifing_name: "return", account_identifing_name: "inventory", amount: self.calculate_cost, balance_type: 'debt', reference_type: self.class.name, reference_id: self.id, time: self.created_at || DateTime.now,
                  parts_attributes: [ 
                    { journal_identifing_name: 'return', account_identifing_name: 'r_inventory', amount:  self.calculate_cost , balance_type: 'credit', reference_type: self.class.name, reference_id: self.id, time: self.created_at || DateTime.now }
                  ]
                }
              ]
            end
          end
        end
        
        #
        # define relationships
        #
        belongs_to :stock, class_name: "::Main::InventoryManagement::Models::Stock", foreign_key: :stock_id
        belongs_to :reference, polymorphic: true

        #
        # cache relationship
        # generate cached collection
        #
        include ::Supports::Cacheable::Cacheable
        cache_belongs_to :stock, inverse_name: :stock_adjustments#, embed: true

        #
        # validations
        #
        validates :count, presence: true, numericality: { minimum: 1 }
        validates :on, presence: true, inclusion: { in: %w{ count_on_store count_pending_to_customer count_pending_from_supplier }, allow_blank: true }
        validates :stock, presence: true
        validates :stock_type, presence: true, inclusion: { in: %w{ reject sale }, allow_blank: true }

        #
        # callbacks
        #
        before_validation :initialize_stock!, if: Proc.new{ |sa| sa.variant_id && sa.store_id}
        after_create :process!

        #
        # attr accessors
        #
        attr_accessor :variant_id, :store_id

        #
        # include AASM
        #
        include ::AASM
        aasm column: :state do
          state :draft, initial: true
          state :published

          event :publish, after: :process! do
            transitions from: :draft, to: :published
          end
        end

        #
        # check whether stock reject
        #
        def rejected_stock?
          self.stock_type == 'reject'
        end

        def sale_stock?
          self.stock_type == 'sale'
        end

        #
        # calculate cost
        #
        def calculate_cost
          (self.fetch_stock.try(:fetch_variant).try(:cost) || 0) * count
        end

        #
        # calculate tax
        #
        def calculate_tax
          calculate_cost * (10/100)
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res= cached_collection

            if params[:from_count].present?
              res = res.where("#{self.table_name}.count >= ?", params[:from_count].to_i)
            end
            if params[:to_count].present?
              res = res.where("#{self.table_name}.count <= ?", params[:to_count].to_i)
            end
            if params[:variant_id].present?
              res = res.joins(:stock).where(main_inventory_management_stocks: { variant_id: params[:variant_id] })
            end
            if params[:store_id].present?
              res = res.joins(:stock).where(main_inventory_management_stocks: { store_id: params[:store_id] })
            end
            
            res
          end

        end

        protected

          #
          # initialize stock
          #
          def initialize_stock!
            self.stock= ::Main::InventoryManagement::Models::Stock.find_or_create_by store_id: fetch_store.try(:id), 
                                                                                     variant_id: self.variant_id,
                                                                                     type: stock_type_class.name
            self.stock_id= self.stock.id
          end

          def fetch_store(id_store=nil)
            id_store||= self.store_id
            ::Main::StoresManagement::Models::Store.fetch(self.store_id) rescue nil
          end

          #
          # aasm callback to drop qty to variant stock
          # add/substract variant stock
          #
          def process!
            if draft?
              if on.eql?("count_on_store") or rejected_stock?
                publish!
              else
                send(on)
              end
            elsif published?
              case on
              when "count_on_store"
                count_on_store
              when  "count_pending_to_customer"
                count_pending_to_customer(-count)
                count_on_store(-count)
              when "count_pending_from_supplier"
                count_pending_from_supplier(-count)
                count_on_store
              end
            end
          end

          def stock_type_class
            case self.stock_type
            when 'reject'
              ::Main::InventoryManagement::Models::Types::RejectedStock
            else
              ::Main::InventoryManagement::Models::Types::SaleStock
            end
          end

          #
          # add #count on stock#count_on_store
          #
          def count_on_store(num= nil)
            num||= count
            if stock.low?
              stock.lock!
                self.stock.count_on_store = stock.count_on_store + num.to_i
              stock.save!
            else
              sql = "UPDATE #{::Main::InventoryManagement::Models::Stock.table_name} SET count_on_store = (#{num} + count_on_store) WHERE id = #{self.stock.id}"
              ActiveRecord::Base.connection.execute(sql)
            end
          end

          #
          # add #count on stock#count_pending_to_customer 
          #

          def count_pending_to_customer(num=nil)
            num||= count
            if stock.low?
              stock.lock!
                self.stock.count_pending_to_customer = stock.count_pending_to_customer + num.to_i
              stock.save!
            else
              sql = "UPDATE #{::Main::InventoryManagement::Models::Stock.table_name} SET count_pending_to_customer = (#{num} + count_pending_to_customer) WHERE id = #{self.stock.id}"
              ActiveRecord::Base.connection.execute(sql)
            end
          end

          def count_pending_from_supplier(num=nil)
            num||= count
            if stock.low?
              stock.lock!
                self.stock.count_pending_from_supplier = stock.count_pending_from_supplier + num.to_i
              stock.save!
            else
              sql = "UPDATE #{::Main::InventoryManagement::Models::Stock.table_name} SET count_pending_from_supplier = (#{num} + count_pending_from_supplier) WHERE id = #{self.stock.id}"
              ActiveRecord::Base.connection.execute(sql)
            end
          end

      end
    end
  end
end
#
# sidekiq worker for saving stock adjustment object
#

require 'json'

module Main
  module InventoryManagement
    module Workers
      class StockAdjustment

        include ::Sidekiq::Worker
        
        def perform(stock_adjustments_params)
          Array(JSON.parse(stock_adjustments_params)).flatten.each do |stock_adjustment_params|
            ::Main::InventoryManagement::Models::StockAdjustment.new(stock_adjustment_params).save
          end
        end

      end
    end
  end
end
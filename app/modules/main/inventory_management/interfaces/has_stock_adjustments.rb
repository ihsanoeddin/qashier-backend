module Main
  module InventoryManagement
    module Interfaces
      module HasStockAdjustments
     
        extend ::ActiveSupport::Concern

        included do 
          has_many :stocks, class_name: "::Main::InventoryManagement::Models::StockAdjustment", foreign_key: "#{self.name.demodulize.underscore}_id"
        end
        
      end
    end
  end
end
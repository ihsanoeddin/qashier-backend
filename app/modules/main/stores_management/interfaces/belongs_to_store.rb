#
# this module is used to define belongs_to :store, :shop
#

module Main
  module StoresManagement
    module Interfaces
      module BelongsToStore
        extend ::ActiveSupport::Concern

        included do 
          belongs_to :store, class_name: '::Main::StoresManagement::Models::Store', foreign_key: :store_id
          belongs_to :shop, class_name: '::Main::StoresManagement::Models::Types::Shop', foreign_key: :store_id
        end
        
      end
    end
  end
end

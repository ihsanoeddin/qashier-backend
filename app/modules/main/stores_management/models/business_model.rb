#
# schema
#create_table :main_stores_management_business_models do |t|
#  t.string :name
#  t.string :identifing_name, index: true
#  t.boolean :static, default: false
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module StoresManagement
    module Models
      class BusinessModel < ::ApplicationRecord

        #
        # include cacheable methods
        #
        include ::Supports::Cacheable::Cacheable

        #
        # define relationships
        #
        has_many :stores, class_name: "::Main::StoresManagement::Models::Store", foreign_key: :business_model_id
        has_many :products, class_name: "::Main::ProductsManagement::Models::Product", foreign_key: :business_model_id
        has_many :variants, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: :business_model_id

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :name
        end

        #
        # cache relationships
        #
        cache_has_many :stores, inverse_name: :business_model
        cache_has_many :products, inverse_name: :business_model
        cache_has_many :variants, inverse_name: :business_model

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection
            
            if params[:common].present?
              res = res.search(params[:common])
            end

            res
          end

        end

      end
    end
  end
end
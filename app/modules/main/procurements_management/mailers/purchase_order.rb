module Main
  module ProcurementsManagement
    module Mailers
      class PurchaseOrder < ::ActionMailer::Base
        
        #default :from => ::Tenants::Merchants::Models::Company.fetch(::Apartment::Tenant.current).try(:email) || "Qashier <no-reply@qashier.com>"
        default template_path: "purchase_orders"
        append_view_path Rails.root.join('app', 'modules', 'main', 'procurements_managements', 'views', 'mailers')

        def open(purchase_order, recipients=[], sender=nil)
          @purchase_order||= purchase_order
          @recipients||= recipients || [@purchase_order.supplier.email]
          mail to: recipients, subject: 'Opened Purchase Order'
        end

        def cancel(purchase_order, recipients=[], sender= nil)
          @purchase_order||= purchase_order
          @recipients||= recipients || [@purchase_order.supplier.email]
          mail to: recipients, subject: 'Canceled Purchase Order'
        end
      
      end
    end
  end
end

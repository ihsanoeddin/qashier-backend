#
# schema
#create_table :main_procurements_management_invoice_details do |t|
#  t.references :invoice, index: { name: "main_purchase_orders_management_invoice_details_invoice_id" }
#  t.references :variant, index: { name: "main_purchase_orders_management_invoice_details_variant_id" }
#  t.integer :received_quantity, default: 0
#  t.integer :expected_quantity, default: 0
#  t.string :name
#  t.decimal :price, :default => 0.0, :precision => 15, :scale => 2
#  t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
#  t.string :state
#  t.float :tax_rate, default: 0
#  t.float :discount_rate, default: 0 
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module ProcurementsManagement
    module Models
      class InvoiceDetail < ::ApplicationRecord

        include ::Supports::Trackable::Interfaces::HasTracks
        track!

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # relationships
        #
        belongs_to :invoice, class_name: "::Main::ProcurementsManagement::Models::Invoice", foreign_key: :invoice_id
        belongs_to :variant, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: :variant_id

        #
        # cache relationships
        #       
        cache_belongs_to :invoice, inverse_name: :invoice_details
        cache_belongs_to :variant, inverse_name: :invoice_details

        #
        # validations
        #
        validates :invoice, presence: true
        validates :variant, presence: true, unless: :name
        validates :name, presence: true, unless: :variant
        validates :received_quantity, presence: true, numericality: { allow_blank: true }
        validates :expected_quantity, presence: true, numericality: { allow_blank: true }
        validates :price, presence: true, numericality: true
        validate :valid_invoice

        #
        # can be deleted, created, or updated if invoice state is `draft`
        #
        def valid_invoice
          if invoice
            errors.add(:invoice_id, "Can't be created, updated or deleted unless invoice state is draft") unless invoice.draft?
          end
        end

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :name
          attributes :variant => "variant.name"
        end

        #
        # callbacks
        #
        before_save :calculate_amount
        after_save :calculate_invoice_amount
        #before_update :valid_to_update?, unless: :state_changed?
        before_destroy :valid_to_update?

        def valid_to_update?
          unless invoice.draft?
            errors.add(:state, "can not update or delete")
            throw(:abort)
          end
        end

        #
        # calculate amount
        #
        def calculate_amount
          self.amount= self.received_quantity * self.price  
        end

        def calculate_invoice_amount
          self.invoice.update :amount, self.invoice.details.inject(0){ |sum, detail| sum += detail.deleted?? 0 : detail.amount }
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection.order("created_at DESC")
            if params[:common].present?
              res = res.search(params[:common])
            end
            if params[:variant_id].present?
              res = res.where(variant_id: params[:variant_id])
            end
            res
          end

        end

      end
    end
  end
end
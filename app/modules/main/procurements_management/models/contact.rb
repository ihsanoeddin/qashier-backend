#
# schema
#create_table :main_procurements_management_contacts do |t|
#  t.references :supplier, index: { name: "main_procurements_management_contacts_supplier_id" }
#  t.string :name
#  t.string :email
#  t.string :phone_number      
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module ProcurementsManagement
    module Models
      class Contact < ::ApplicationRecord

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # relationships
        #
        belongs_to :supplier, class_name: "::Main::ProcurementsManagement::Models::Supplier", foreign_key: :supplier_id

        #
        # cache relationships
        #
        cache_belongs_to :supplier, inverse_name: :contacts

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :name, :email, :phone_number
        end

        validates :name, presence: true
        validates :phone_number, numericality: true

        class << self

            #
            # filter records
            #
            def filter(params={})
              res = cached_collection
              if params[:common].present?
                res = res.search(params[:common])
              end
              res
            end

        end

      end
    end
  end
end
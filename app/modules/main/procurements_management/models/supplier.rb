#
# schema
#create_table :main_procurements_management_suppliers do |t|
#  t.string :name
#  t.string :email
#  t.string :phone_number
#  t.string :code
#  t.string :mode
#  t.string :state
#  t.integer :max_purchase_orders_qty, default: 0
#  t.integer :min_purchase_orders_qty, default: 0      
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module ProcurementsManagement
    module Models
      class Supplier < ::ApplicationRecord

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # relationships
        #
        include ::Supports::Addressable::Interfaces::HasAddresses
        has_many :purchase_orders, class_name: "::Main::ProcurementsManagement::Models::PurchaseOrder", foreign_key: :supplier_id, inverse_of: :supplier
        has_many :contacts, class_name: "::Main::ProcurementsManagement::Models::Contact", foreign_key: :supplier_id, inverse_of: :supplier
        has_many :returns, class_name: "::Main::ProcurementsManagement::Models::Return", foreign_key: :supplier_id
        has_many :suppliers_variants, class_name: "::Main::ProcurementsManagement::Models::SuppliersVariant", foreign_key: :supplier_id

        #
        # cache relationships
        #
        cache_has_many :suppliers_variants, inverse_name: :supplier
        cache_has_many :contacts, inverse_name: :supplier
        cache_has_many :addresses, inverse_name: :addressable
        cache_has_many :purchase_orders, inverse_name: :supplier

        #
        # accepts nested attributes
        #
        accepts_nested_attributes_for :contacts, allow_destroy: true, reject_if: :all_blank

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :name, :email, :code, :phone_number
          attributes :contacts => 'contacts.name'
        end

        #
        # stateful model
        #
        include ::AASM
        aasm column: :state do
          
        end

        validates :name, presence: true, uniqueness: { allow_blank: true, scope: :deleted_at }
        validates :phone_number, presence: true, numericality: true
        validates :code, presence: true
        validates :mode, presence: true, inclusion: { in: %w{consigment direct}, allow_blank: true }
        validates :max_purchase_orders_qty, numericality: true
        validates :min_purchase_orders_qty, numericality: true

        #
        # use permalink as friendly id column
        #
        extend FriendlyId
        friendly_id :slug_candidates, use: :slugged, slug_column: :code

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection
            if params[:common].present?
              res = res.search(params[:common])
            end
            res
          end

        end

        protected

          #
          # abbr name
          #
          def abbr_name
            self.split(" ").map {|name| name[0].chr }.join.upcase 
          end

          #
          # permalink slug candidates
          #
          def slug_candidates
            [ :abbr_name,[:abbr_name,:id] ]
          end

      end
    end
  end
end
#
# schema
#
#create_table :main_returns_management_exchanges do |t|
#
#  t.references :store, index: { name: "main_returns_management_refunds_store_id" }
#  t.references :transaction, index: { name: "main_returns_management_refunds_transaction_id" }
#  #t.references :return, index: { name: "main_returns_management_refunds_return_id" }
#  t.references :variant, index: { name: "main_returns_management_refunds_variant_id" }
#  t.string :state
#  t.integer :quantity, default: 0
#  t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
#
#  t.references :exchange_variant, index: { name: "main_returns_management_refunds_exchange_variant_id" }
#  t.integer :exchange_quantity, default: 0
#  t.decimal :exchange_amount, :default => 0.0, :precision => 15, :scale => 2
#
#  t.text :notes
#  t.datetime :time
#
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#
#

module Main
  module ReturnsManagement
    module Models
      class Return < ::ApplicationRecord

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # include has balances
        #
        include ::Main::Accounting::Interfaces::HasBalances
        insert_balance! depend_on_changes_of: { state: "accepted" }
        #
        # @Overriding params for balance insertion
        #
        def balance_insertion_params
          [ 
            { 
              journal_identifing_name: 'return', account_identifing_name: "r_inventory", amount: self.amount, balance_type: 'debt', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.notes,
              parts_attributes: [ 
                { journal_identifing_name: 'return', account_identifing_name: 'inventory', amount:  self.exchange_amount, balance_type: 'credit', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.notes }
              ]
            }    
          ]
        end

        #
        # include stock_adjustable helpers
        #
        include ::Main::InventoryManagement::Helpers::StockAdjustable
        adjust_stock! depend_on_changes_of: { state: "accepted" }#, when: :after_save

        #
        # @Override from ::Main::InventoryManagement::HelpersStockAdjustable
        #
        def stock_adjustment_params
          if self.variant.present? 
            [
              { reference_type: self.class.name, reference_id: self.id, stock_id: fetch_variant.try(:stock, store).try(:id), state: "published", count: self.quantity, on: "count_on_store", stock_type: "reject" }, 
              { reference_type: self.class.name, reference_id: self.id, stock_id: fetch_exchange_variant.try(:stock, store).try(:id), state: "published", count: -(self.exchange_quantity), on: "count_on_store", stock_type: "sale" }
            ]
          else 
            {}
          end
        end

        #
        # define relationships
        #
        #belongs_to :retur, class_name: "::Main::ReturnsManagement::Models::Return", foreign_key: :return_id#, counter_cache: :transaction_details_count
        include ::Main::StoresManagement::Interfaces::BelongsToStore
        belongs_to :transaksi, class_name: "::Main::SalesManagement::Models::Transaction", foreign_key: :transaction_id#, counter_cache: :transaction_details_count
        belongs_to :variant, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: :variant_id
        belongs_to :exchange_variant, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: :exchange_variant_id
        
        #
        # cache relationships
        #
        #cache_belongs_to :retur
        cache_belongs_to :variant
        cache_belongs_to :exchange_variant
        cache_belongs_to :transaksi
        cache_belongs_to :store

        #
        # stateful model
        #
        include ::AASM
        aasm column: :state do
          state :draft, initial: true
          state :rejected
          state :accepted
          event :reject do 
            transitions from: [:draft, :processed], to: :rejected
          end
          event :accept, guard: :exchange_variant_is_present? do 
            transitions from: [:draft, :processed, :rejected], to: :accepted
          end
        end

        #
        # validations
        #
        validates :transaksi, presence: true
        validates :store, presence: true
        validates :variant, presence: true
        validates :quantity, presence: true, numericality: { minimum: 1, maximum: Proc.new{|exc| exc.transaction_detail.try(:quantity) || 0 }, allow_blank: true }
        validates :exchange_quantity, presence: true, numericality: { minimum: 1, maximum: Proc.new{|exc| exc.transaction_detail.try(:quantity) || 0 }, allow_blank: true }, if: :exchange_variant_present?

        #
        # callbacks
        #
        before_save :calculate_amount
        before_save :calculate_exchange_amount
        before_validation :initialize_time
        #before_update :valid_to_update?, unless: :state_changed?
        before_destroy :valid_to_update?

        #
        # sugar name for transaksi
        #
        def transaction_object
          fetch_transaksi || transaksi
        end

        #
        # initialize time 
        #
        def initialize_time
          self.time||= DateTime.now
        end

        #
        # check whether exchange variant is present
        #
        def exchange_variant_present?
          exchange_variant.present? || exchange_variant_id.present?
        end

        #
        # transactiond detail object
        #
        def transaction_detail(item_id= nil)
          item_id||= self.variant_id
          fetch_transaksi.details.where(variant_id: item_id).first rescue nil
        end

        #
        # calculate amount
        #
        def calculate_amount
          self.amount= cost_per_item * self.quantity
        end

        def calculate_exchange_amount
          if exchange_variant.present?
            self.exchange_amount= cost_per_exchange_item * self.exchange_quantity
          end
        end

        #
        # get cost per item
        #
        def cost_per_item
          variant.try(:cost) || 0 
        end

        def cost_per_exchange_item
          exchange_variant.try(:cost) || 0 
        end

        #
        # method to apply on callback to authorize update
        #
        def valid_to_update?
          unless draft?
            errors.add(:state, "can not update or delete")
            throw(:abort)
          end
        end

        class << self

          #
          # to filter records
          #
          def filter(params={})

            res= cached_collection

            if params[:id].present?
              res = res.where(id: params[:id])
            end

            if params[:store_id].present?
              res= res.where(store_id: params[:store_id])
            end
            if params[:transaction_id]
              res= res.where(transaction_id: params[:transaction_id])
            end
            if params[:state].present?
              res= res.where(state: params[:state])
            end
            if params[:variant_id].present?
              res = res.where(variant_id: params[:variant_id])
            end
            if params[:min_amount].present?
              params[:min_amount]= BigDecimal(params[:min_amount]) rescue 0
              res = res.where("amount >= ?", params[:min_amount])
            end
            if params[:max_amount].present?
              params[:max_amount]= BigDecimal(params[:max_amount]) rescue 0
              res = res.where("amount <= ?", params[:max_amount])
            end
            if params[:exchange_variant_id].present?
              res = res.where(exchange_variant_id: params[:exchange_variant_id])
            end
            if params[:min_quantity].present?
              params[:min_quantity]= Integer(params[:min_quantity]) rescue 0
              res = res.where("quantity >= ?", params[:min_quantity])
            end
            if params[:max_quantity].present?
              params[:max_quantity]= Integer(params[:max_quantity]) rescue 0
              res = res.where("quantity <= ?", params[:max_quantity])
            end
            if params[:from_time].present?
              from_time= DateTime.parse(params[:from_time]) rescue nil
              res = res.where("time >= ?", from_time) if from_time
            end
            if params[:to_time].present?
              to_time= DateTime.parse(params[:to_time]) rescue nil
              res = res.where("time <= ?", to_time) if to_time
            end
            res

          end

        end
      
      end
    end
  end
end
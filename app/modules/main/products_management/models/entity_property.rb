#
# schema
#create_table :main_products_management_entity_properties do |t|
#  t.references :entity, polymorphic: true, index: { name: "products_management_entity_type_and_entity_id" }
#  t.references :property, index: { name: "products_management_entity_property_id" }
#  t.integer :position
#  t.float :value, default: 0
#  t.string :unit_value
#  t.datetime :deleted_at, index: { name: "products_management_entity_properties_deleted_at" }
#  t.timestamps
#end
#

module Main
  module ProductsManagement
    module Models
      class EntityProperty < ::ApplicationRecord

        #
        # support batch insertion
        #
        include ::Supports::Batchable
      
        #
        # define relationships
        #
        belongs_to :entity, polymorphic: true
        belongs_to :property, class_name: "::Main::ProductsManagement::Models::Property", foreign_key: :property_id

        #
        # cache relationship
        # generate cached collection
        #
        include ::Supports::Cacheable::Cacheable
        cache_belongs_to :property, inverse_name: :entity_properties#, embed: true
        cache_belongs_to :entity

        #
        # validations
        #
        #validates :entity_id, presence: true, uniqueness: { scope: :entity_type }
        #validates :entity_type, presence: true
        #validates :value, presence: true, numericality: { allow_blank: true }
        validates :property, presence: true
        validates :unit_value, presence: true

        #
        # scopes
        #
        scope :weight, -> { joins(:property).where(main_products_management_properties: {identifing_name: 'weight'}) }
        scope :long, -> { joins(:property).where(main_products_management_properties: {identifing_name: 'length'}) }
        scope :width, -> { joins(:property).where(main_products_management_properties: {identifing_name: 'width'}) }
        scope :height, -> { joins(:property).where(main_products_management_properties: {identifing_name: 'height'}) }
        scope :dimension, -> { joins(:property).where(main_products_management_properties: {identifing_name: ['height', 'width', 'length']}) }

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :position, :value, :unit_value
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection.where(nil)
            if params[:common].present?
              res = res.search(params[:common])
            end
            if params[:product_id].present?
              res = res.where(entity_id: params[:product_id], entity_type: "Main::ProductsManagement::Models::Product")
            end
            if params[:property_id].present?
              res = res.where(property_id: params[:property_id])
            end
            res
          end

        end

      end
    end
  end
end
#
# schema
#create_table :main_products_management_properties do |t|
#  t.string :identifing_name
#  t.string :display_name
#  t.text :units
#  t.boolean :active, :default => true
#  t.datetime :deleted_at, index: { name: "products_management_properties_deleted_at" }
#  t.timestamps
#end
#

module Main
  module ProductsManagement
    module Models
      class Property < ::ApplicationRecord

        serialize :units, Array

        #
        # define relationships
        #
        has_many :entity_properties, class_name: "::Main::ProductsManagement::Models::EntityProperty", foreign_key: :property_id
        has_many :products, through: :entity_properties, source: :entity, source_type: "::Main::ProductsManagement::Models::Product"
        has_many :variants, through: :entity_properties, source: :entity, source_type: "::Main::ProductsManagement::Models::Variant"

        #
        # cache relationship
        # generate cached collection
        #
        include ::Supports::Cacheable::Cacheable
        cache_has_many :entity_properties, inverse_name: :property

        #
        # validations
        # 
        validates :identifing_name, presence: true, uniqueness: true
        validates :display_name, presence: true
        #validates :units, presence: true
        validates :description, length: { maximum: 255 }

        #
        # callbacks
        #
        before_validation :generate_identifing_name

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :identifing_name, :display_name
        end

        #
        # scopes
        #
        scope :actives, -> { where(active: true) }

        #
        # generate identifing name if blank 
        #
        def generate_identifing_name
          self.identifing_name||= self.display_name.to_s.parameterize.underscore
        end

        class << self

          #
          # filter records
          #
          def filter params={}
            res= cached_collection.where(nil)
            if params[:active].present?
              res = res.where(active: params[:active])
            end
            if params[:common].present?
              res = res.search(params[:common])
            end
            res
          end


        end

      end
    end
  end
end
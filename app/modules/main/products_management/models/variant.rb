#
# schema
#create_table :main_products_management_variants do |t|
#  t.references :product, index: { name: "products_management_variants_product_id" }
#  t.string :name
#  t.string :sku
#  t.string :state
#  t.string :type
#  t.string :keywords
#  t.string :permalink
#  t.decimal :cost, :default => 0.0, :precision => 15, :scale => 2
#  t.decimal :price, :default => 0.0, :precision => 15, :scale => 2
#  t.string :short_description
#  t.boolean :bundled, default: false
#  t.boolean :master, default: false
#  t.boolean :featured, default: false
#  t.datetime :deleted_at, index: { name: "products_management_variants_deleted_at" }
#  t.timestamps
#end
#

module Main
  module ProductsManagement
    module Models
      class Variant < ::ApplicationRecord

        #
        # support batch insertion
        #
        include ::Supports::Batchable

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable
        cache_index :permalink
        self.caches_suffix_list= ["collection", "image-urls-variant-thumb", "image-urls-variant-medium", "image-urls-variant-original"]

        #
        # include belongs to image group
        #
        include ::Supports::Imageable::Interfaces::BelongsToImageGroup
        #include ::Main::InventoryManagement::Interfaces::HasStocks
        include ::Main::InventoryManagement::Interfaces::HasStocks::VariantHelper
        belongs_to :business_model, class_name: "::Main::StoresManagement::Models::BusinessModel", foreign_key: :business_model_id
        has_many :stocks, class_name: "::Main::InventoryManagement::Models::Stock", foreign_key: "#{self.name.demodulize.underscore}_id"
        has_many :sale_stocks, -> { where(type: "Main::InventoryManagement::Models::Types::SaleStock") }, class_name: "::Main::InventoryManagement::Models::Stock", foreign_key: "#{self.name.demodulize.underscore}_id"
        has_many :rejected_stocks, -> { where(type: "Main::InventoryManagement::Models::Types::RejectedStock") }, class_name: "::Main::InventoryManagement::Models::Stock", foreign_key: "#{self.name.demodulize.underscore}_id"
        has_many :stores, through: :stocks
        has_many :transaction_details, class_name: "::Main::SalesManagement::Models::TransactionDetail", inverse_of: :variant, foreign_key: :variant_id
        has_many :variant_discounts, class_name: "::Main::SalesManagement::Models::VariantDiscount", inverse_of: :variant, foreign_key: :variant_id
        has_many :discounts, through: :variant_discounts
        has_many :refunds, class_name: "::Main::ReturnsManagement::Models::Refund", inverse_of: :variant, foreign_key: :variant_id
        has_many :returns, class_name: "::Main::ReturnsManagement::Models::Return", inverse_of: :variant, foreign_key: :variant_id
        has_many :variant_exchanges, class_name: "::Main::ReturnsManagement::Models::Return", inverse_of: :variant, foreign_key: :exchange_variant_id
        has_many :variant_bundles, class_name: "::Main::ProductsManagement::Models::VariantsBundle", inverse_of: :variant, foreign_key: :variant_id
        has_many :variants, through: :variants_bundles
        has_many :bundle_variants, class_name: "::Main::ProductsManagement::Models::VariantsBundle", inverse_of: :bundle, foreign_key: :bundle_id
        has_many :bundles, through: :bundles_variants
        has_many :suppliers_variants, class_name: "::Main::ProcurementsManagement::Models::SuppliersVariant", foreign_key: :variant_id
        has_many :purchase_order_details, class_name: "::Main::ProcurementsManagement::Models::PurchaseOrderDetail", foreign_key: :variant_id
        has_many :invoice_details, class_name: "::Main::ProcurementsManagement::Models::InvoiceDetail", foreign_key: :variant_id

        #
        # method delegation
        #
        delegate :name, to: :business_model, prefix: true 

        #
        # make keywords attr array
        #
        serialize :keywords, Array

        #
        # define relationship
        #
        belongs_to :product, class_name: "::Main::ProductsManagement::Models::Product", foreign_key: :product_id#,counter_cache: :variants_count
        has_many :entity_properties, as: :entity, class_name: "::Main::ProductsManagement::Models::EntityProperty"
        accepts_nested_attributes_for :entity_properties, allow_destroy: true, reject_if: :all_blank
        has_many :properties, through: :entity_properties

        #cache relationship
        cache_belongs_to :business_model, inverse_name: :variants
        cache_belongs_to :product, inverse_name: :variants
        cache_belongs_to :image_group, inverse_name: :variants
        cache_has_many :entity_properties, inverse_name: :entity
        cache_has_many :stocks, inverse_name: :variant
        cache_has_many :sale_stocks, inverse_name: :variant
        cache_has_many :rejected_stocks, inverse_name: :variant
        cache_has_many :transaction_details, inverse_name: :variant
        cache_has_many :refunds, inverse_name: :variant
        cache_has_many :returns, inverse_name: :variant
        cache_has_many :variant_exchanges, inverse_name: :exchange_variant
        cache_has_many :variant_bundles, inverse_name: :variant
        cache_has_many :bundle_variants, inverse_name: :bundle
        cache_has_many :suppliers_variants, inverse_name: :variant
        cache_has_many :purchase_order_details, inverse_name: :variant
        cache_has_many :invoice_details, inverse_name: :variant

        #
        # use permalink as friendly id column
        #
        extend FriendlyId
        friendly_id :slug_candidates, use: :slugged, slug_column: :permalink

        #
        # validations
        #
        validates :product, presence: true, unless: :bundled
        validates :business_model, presence: true
        validates :name, presence: true, uniqueness: { allow_blank: true, scope: :product_id }
        validates :sku, uniqueness: { allow_blank: true, scope: [ :type ] }
        validates :cost, presence: true, numericality: { allow_blank: true }
        validates :price, presence: true, numericality: { allow_blank: true }

        #
        # callbacks
        #
        before_validation :valid_bundled
        before_validation :set_business_model

        #
        # set bundled to false if product_id or product is present
        #
        def valid_bundled
          self.bundled= false if product.present? || product_id.present?
        end

        #
        # set business_model if product present
        #
        def set_business_model
          self.business_model_id= self.product.business_model_id if self.product.present?
        end

        protected :valid_bundled

        #
        # include search cop
        #
        include SearchCop
        search_scope :search do
          attributes :name, :permalink, :keywords
        end

        #
        # set default values
        #
        before_validation :set_default_values

        #
        # get cached image urls
        #
        def image_urls(image_size = :thumb)
          Rails.cache.fetch("#{self.class.cached_name}-image-urls-#{self.class.name.demodulize.downcase}-#{image_size}", :expires_in => 1.day) do
            fetch_image_group ? fetch_image_group.image_urls(image_size) : fetch_product.image_urls(image_size)
          end
        end

        #
        # get cached discounts
        # it will be cleared when variants_discounts inserted, updated, or deleted
        # refer to VariantDiscount#clear_variants_and_discount_cache
        #
        def cached_discounts
          Rails.cache.fetch("#{::Apartment::Tenant.current}-#{self.class.name.demodulize.underscore}-#{self.id}-cached-discounts", expires_in: 1.day) do
            discounts.available.load
          end 
        end

        #
        # price after tax
        #
        def price_after_tax
          self.price + tax_rate_cost
        end

        #
        # get tax rate cost
        #
        def tax_rate_cost(proposed_price=nil)
          proposed_price||= self.price
          (fetch_product || product).tax_rate_cost(proposed_price)
        end

        class << self

          #
          # filter records
          #
          def filter params={}
            res = cached_collection
            if params[:price_from].present?
              res = res.where("#{self.table_name}.price >= ?", params[:price_from])
            end
            if params[:price_to].present?
              res = res.where("#{self.table_name}.price <= ?", params[:price_to])
            end
            if params[:cost_from].present?
              res = res.where("#{self.table_name}.cost >= ?", params[:cost_from])
            end
            if params[:cost_to].present?
              res = res.where("#{self.table_name}.cost <= ?", params[:cost_to])
            end
            if params[:product_id].present?
              res = res.where(product_id: params[:product_id])
            end
            if params[:sku].present?
              res = res.where(sku: params[:sku])
            end
            if params[:bundled].present?
              res = res.where(bundled: params[:bundled])
            end
            if params[:common].present?
              res.search(params[:common])
            end
            if params[:from_available_quantity].present?
                res = res.joins(:stocks).where("(#{::Main::InventoryManagement::Models::Stock.table_name}.count_on_store - #{::Main::InventoryManagement::Models::Stock.table_name}.count_pending_to_customer) >= ?", params[:from_available_quantity].to_i)
              end
              if params[:to_available_quantity].present?
                res = res.joins(:stocks).where("(#{::Main::InventoryManagement::Models::Stock.table_name}.count_on_store - #{::Main::InventoryManagement::Models::Stock.table_name}.count_pending_to_customer) <= ?", params[:to_available_quantity].to_i)
              end
            res # return query result
          end

        end

        protected
          #
          # set default values of cost and price if blank
          #
          def set_default_values
            self.cost||= self.product.try(:cost)
            self.price||= self.product.try(:price)
          end

          #
          # permalin slug candidates
          #
          def slug_candidates
            [ :name,[:name,:id] ]
          end

          def should_generate_new_friendly_id?
            permalink.blank?
          end

      end
    end
  end
end
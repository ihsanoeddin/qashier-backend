#
# schema
#create_table :main_products_management_variants_bundles do |t|
#  t.references :bundle, index: { name: "main_products_management_variants_bundles_bundle_id" }
#  t.references :variants, index: { name: "main_products_management_variants_bundles_variant_id" }
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module ProductsManagement
    module Models
      class VariantsBundle < ::ApplicationRecord

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # define relationships
        #
        belongs_to :variant, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: :variant_id
        belongs_to :bundle, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: :bundle_id

        #
        # cache relationships
        #
        cache_belongs_to :variant, inverse_name: :bundles
        cache_belongs_to :bundle, inverse_name: :variants

      end
    end
  end
end

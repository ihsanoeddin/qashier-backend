#
# schema
#create_table :main_products_management_products do |t|
#  t.string :name
#  t.string :state
#  t.string :brand
#  t.string :type
#  t.string :keywords
#  t.string :permalink
#  t.decimal :cost, :default => 0.0, :precision => 15, :scale => 2
#  t.decimal :price, :default => 0.0, :precision => 15, :scale => 2
#  t.string :short_description
#  t.text :description
#  t.boolean :featured, default: false
#  t.references :product_type, index: { name: "products_management_products_product_type_id" }
#  t.references :tax_rate, index: { name: "products_management_products_tax_rate_id" }
#  t.datetime :deleted_at, index: { name: "products_management_products_deleted_at" }
#  t.timestamps
#end
#

module Main
  module ProductsManagement
    module Models
      module Types
        class Item < ::Main::ProductsManagement::Models::Product

          #
          # include cacheable
          #
          include ::Supports::Cacheable::Cacheable

          #
          # @Override caches_suffix_list attribute
          #
          self.caches_suffix_list= ["collection", "image-urls-item-thumb", "image-urls-item-medium", "image-urls-item-original" ]

          #
          # define relationships
          #
          belongs_to :product_type, class_name: '::Main::ProductsManagement::Models::ProductType', counter_cache: :products_count
          belongs_to :tax_rate, class_name: '::Main::ProductsManagement::Models::TaxRate', optional: true, counter_cache: :products_count

          #
        # use search cop
        #
        include SearchCop
        search_scope :search do
          attributes :name, :permalink#, :keywords
          attributes :product_type => "product_type.name"
          attributes :tax_rate => "tax_rate.name"
        end

        end
      end
    end
  end
end
#
# schema
#create_table :supports_importers_imports do |t|
#      
#  t.references :store, index: { name: "supports_importers_imports_store_id" }
#  t.string :name
#  t.string :state
#  t.text :details
#  t.datetime :started_at
#  t.datetime :finished_at
#  t.integer :total_row
#  t.integer :current_row
#  t.string :type
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Main
  module ProductsManagement
    module Importers
      class Variant < ::Supports::Importers::Models::Import

        self.csv_headers= [ "No", "Product Name", "Product Type", "Product Type Name",
                            "Product Description", "Variant Name", "Cost", "Price", "SKU", 
                            "Variant Short Description", "Properties", "Image URL"]

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # save a row to class constant table
        # @Override from parent class
        # parameter `params` is hash that has keys on self.csv headers
        #
        def save_a_row(row_number=0, params={})
          product_type= ::Main::ProductsManagement::Models::ProductType.find_or_create_by name: params[:product_type_name]
          product = product_type.products.find_by_name(params[:product_name])
          # if product blank then we must create it
          if product.blank?
            product= product_type.products.new name: params[:product_name], cost: params[:cost].to_i, price: params[:price].to_i, description: params[:product_description]
            case params[:product_type].to_s.downcase
            when "service"
              product.becomes!(::Main::ProductsManagement::Models::Types::Service)
            else
              product.becomes!(::Main::ProductsManagement::Models::Types::Item)
            end
            product.save
          end
          # save image url if image name is not existed yet on records
          if params[:image_url].present? && product.images.blank? #&& product.images.find_by_attachment(params[:image_url].to_s.split('/').last).blank?
            product.images.create remote_attachment_url: params[:image_url]
          end
          # save errors details if product is not saved
          unless product.persisted?
            self.details[:errors]||= []
            self.details[:errors] << "Error when saving product on row number #{row_number}.\nMessage: #{product.errors.full_messages}"
          end
          # create variant if product persisted
          if product.persisted?
            variant= product.variants.create name: params[:variant_name], cost: params[:cost], price: params[:price], 
                                    sku: params[:sku], short_description: params[:variant_short_description]
            if variant.persisted?
              # create entity properties
              # change string to array by splitting by `,` or comma
              entity_properties_params = params[:properties].to_s.split(",").map do |properties|
                                    # split string by space
                                    prop= properties.strip.split(" ")
                                    # check if prop length without value
                                    # prop.first is property name
                                    # create property object if not present
                                    property = ::Main::ProductsManagement::Models::Property.find_or_create_by(display_name: prop.first)
                                    if prop.length == 2
                                      # prop.last is unit name
                                      # add property unit
                                      unless property.units.include?(prop.last)
                                        property.units << prop.last
                                        property.save
                                      end
                                      { entity_type: variant.class.name, entity_id: variant.id, unit_value: prop.last, property_id: property.id } 
                                    elsif prop.length > 2
                                      # prop[0] is property name
                                      # prop[1] is value
                                      # prop[2] is unit name
                                      # add property unit
                                      unless property.units.include?(prop[2])
                                        property.units << prop[2]
                                        property.save
                                      end
                                      { entity_type: variant.class.name, entity_id: variant.id, value: prop[1], unit_value: prop.last, property_id: property.id } 
                                    end
                                  end
              ::Main::ProductsManagement::Models::EntityProperty.create(entity_properties_params)
              # assign images if params[:image_url].present? and image name is not found on product.imagesimp
              image = product.images.find_by_attachment(params[:image_url].to_s.split('/').last)
              if image.blank? && params[:image_url].present?
                # create image group object for variant
                image_group = ::Supports::Imageable::Models::ImageGroup.find_or_create_by  contextable_type: product.class.name, contextable_id: product.id, 
                                                                                name: "#{variant.name}-#{variant.id}-images"
                image_group.images.create remote_attachment_url: params[:image_url]
                variant.update_attribute :image_group_id, image_group.id 
              end
            else
              # save errors results if variant is not saved
              self.results[:errors]||= []
              self.results[:errors] << "Error when saving variant on row number #{row_number}.\nMessage: #{variant.errors.full_messages.join(", ")}"
              save
            end
          end
        end

      end
    end
  end
end
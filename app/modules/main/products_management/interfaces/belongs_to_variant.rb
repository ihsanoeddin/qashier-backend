module Main
  module ProductsManagement
    module Interfaces
      module BelongsToVariant
        extend ::ActiveSupport::Concern
      
        included do 
          belongs_to :variant, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: "variant_id"
        end

      end
    end
  end
end
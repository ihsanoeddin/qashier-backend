#
# @Override Doorkeeper::TokensController
# to provide specific authorization scenario 
# this class should be provide grant password scenario only!
# TODO : change all message text to Int.
#

require "#{Rails.root.join('lib/exception_handlers/rails/responses')}"

module Api
  module V1
    class SessionsController < ::Doorkeeper::TokensController

      # include exception handlers module
      # include ::ExceptionHandlers::Rails::Responses
     
      #
      # [POST] /api/v1/sessions
      # @Override ::Doorkeeper::TokensController#token
      # this method should be enough to generate token
      #
      def create
        response = authorize_response
        headers.merge! response.headers
        if response.status == :ok
          self.status        = response.status
          self.response_body = response_body(response)
        else
          raise ::Doorkeeper::Errors::DoorkeeperError
        end
      rescue ::Doorkeeper::Errors::DoorkeeperError => e
        self.status = :unauthorized
        self.response_body = { message: "Invalid username or password" }.to_json
      end

      #
      # [PUT] /api/v1/sessions/:access_token
      # to simulate refresh access token process
      #
      def update
        params[:refresh_token]= params[:token]
        create
      end

      #
      # [DELETE] /api/v1/sessions
      # @Override ::Doorkeeper::Tokens::Controller#revoke
      #
      def destroy
        if authorized?
          revoke_token
        end
        render json: { message: "Token is revoked!" }, status: 200
      end

      protected

        #
        # get current resource token owner
        #
        def current_resource_owner(resource_owner_id=nil)
          @current_resource_owner||= ::Main::UsersManagement::Models::User.find(resource_owner_id)
        end

        def response_body(response)
          user_serializer = ::Main::UsersManagement::Serializers::UserSerializer.new(current_resource_owner(response.token.resource_owner_id))
          response_body= response.body.merge!({ resource_owner: user_serializer.to_h }).to_json
        end


    end
  end
end
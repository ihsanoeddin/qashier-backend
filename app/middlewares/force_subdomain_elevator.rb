#
# This class should used for development environment only!
#
class ForceSubdomainElevator < Apartment::Elevators::FirstSubdomain

   def call(env)
    super
    rescue ::Apartment::TenantNotFound, ActiveRecord::NoDatabaseError
      raise ActionController::RoutingError.new('Not Found')
  end

  # @return {String} - The tenant to switch to
  def parse_tenant_name(request)
    # request is an instance of Rack::Request
    if Rails.env.development?
    #  # example: look up some tenant from the db based on this request
      tenant_name = extract_domain(request)
      return tenant_name
    else
      super
    end
  end

  #
  # extract domain even if it is full domain with '.'
  #
  def extract_domain(request)
    request.host.split('.').first
  end


end
#
# this class is used to list business fields object
# TODO : change all message text to Int.
#
module Api
  module V1
    module Register
      class BusinessFields < Base

        helpers do 

          def business_fields
            ::Tenants::Merchants::Models::BusinessField.cached_collection
          end

          def skip_authorization;true;end

        end

        resources "companies/registrations/business_fields" do 

          desc "[GET] list all business_fields"
          get do
            { collection: business_fields.map{| bf |  { id: bf.id, name: bf.name } } }
          end

        end

      end
    end
  end
end
#
# this class is used to handle company registration
#
module Api
  module V1
    module Register
      class Registrations < Base
        
        helpers do 
          
          def registration_params
            posts[:company] ||= posts
            posts
          end

          def registration_builder_class
            ::Tenants::Merchants::Builders::CompanyRegistration
          end

          def skip_authorization;true;end

        end

        resources "companies/registrations" do 

          desc "[POST] post new company object"
          post do 
            company_registration= registration_builder_class.new(registration_params)
            if company_registration.save
              company_registration.admin.send_confirmation_notification(company_registration.company, options: { version: 'v1' })
              presenter company_registration.company, presenter_name: "Register::CompanyRegistration"
            else
              standard_validation_error(details: company_registration.errors)
            end
          end

        end

      end
    end
  end
end
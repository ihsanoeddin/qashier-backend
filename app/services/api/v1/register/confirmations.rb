#
# this class is used to handle company registration confirmation
# TODO : change all message text to Int.
#
module Api
  module V1
    module Register
      class Confirmations < Base
        
        self.context_resource_class= '::Main::UsersManagement::Models::User'
        use_resource!

        helpers do 
          
          def confirmation_params
            posts
          end

          def skip_authorization;true;end

        end

        resources "companies/confirmations" do 

          desc "[GET] confirm company or merchant registration"
          get ':confirmation_token/:domain' do
            switch_tenant(params[:domain].to_s.underscore)
            context_resource= resource_class_constant.confirm_by_token(params[:confirmation_token])
            if context_resource.persisted?
              message message: "confirmation succeed.", status: :ok
            else
              standard_validation_error(message: { confirmation_token: ["Token is invalid."] }, status: 401)
            end
          end

        end

      end
    end
  end
end
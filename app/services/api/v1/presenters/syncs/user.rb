module Api
  module V1
    module Presenters
      module Syncs
        class User < ::Api::V1::Presenters::Merchants::UsersManagement::User

          expose :resource_status do |user, options|
            user.resource_status(options[:last_sync])
          end

        end
      end
    end
  end
end
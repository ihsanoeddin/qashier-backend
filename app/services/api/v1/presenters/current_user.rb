module Api
  module V1
    module Presenters
      class CurrentUser < ::Grape::Entity

        expose :id
        expose :username
        expose :email
        expose :merchant
        expose :deleted_at
        expose :created_at
        expose :updated_at
        expose :roles, with: "::Api::V1::Presenters::Merchants::UsersManagement::Role"
        expose :avatar, with: "::Api::V1::Presenters::Merchants::UsersManagement::Avatar"

      end
    end
  end
end
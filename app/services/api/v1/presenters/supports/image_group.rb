module Api
  module V1
    module Presenters
      module Supports
        class ImageGroup < ::Grape::Entity

          expose :id
          expose :name
          expose :contextable_id
          expose :contextable_type do |image_group, options|
            image_group.contextable_type.to_s.demodulize.parameterize.underscore
          end
          expose :images, using: "::Api::V1::Presenters::Supports::Image" do |image_group, options|
            image_group.fetch_images
          end
          expose :deleted_at
          expose :created_at
          expose :updated_at

        end
      end
    end
  end
end

module Api
  module V1
    module Presenters
      module Merchants
        module ProductsManagement
          class TaxRate < ::Grape::Entity
            
            expose :id
            expose :name
            expose :abbr
            expose :rate
            expose :products_count
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end
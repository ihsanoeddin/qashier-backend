module Api
  module V1
    module Presenters
      module Merchants
        module ProductsManagement
          class Product < ::Grape::Entity
            
            expose :id
            expose :name
            expose :permalink
            expose :description
            expose :short_description
            expose :short_description
            expose :brand
            expose :featured
            expose :business_model_id
            expose :business_model_name do |product, opt|
              product.fetch_business_model.try(:name)
            end
            expose :type do |product, options|
              product.type.to_s.demodulize.underscore
            end
            expose :tax_rate, using: "::Api::V1::Presenters::Merchants::ProductsManagement::TaxRate" do |product, opt|
              product.fetch_tax_rate
            end
            expose :product_type, using: "::Api::V1::Presenters::Merchants::ProductsManagement::ProductType" do |product, opt|
              product.fetch_product_type
            end
            expose :variants_count
            expose :images_count
            expose :entity_properties_count
            expose :cost
            expose :price
            expose :variants, using: "::Api::V1::Presenters::Merchants::ProductsManagement::Variant"do |product, opt|
              product.fetch_variants
            end
            expose :images, using: "::Api::V1::Presenters::Merchants::ProductsManagement::Image"do |product, opt|
              product.fetch_images
            end
            expose :entity_properties, using: "::Api::V1::Presenters::Merchants::ProductsManagement::EntityProperty"do |product, opt|
              product.fetch_entity_properties
            end
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end

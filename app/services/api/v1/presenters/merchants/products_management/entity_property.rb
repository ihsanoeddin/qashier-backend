module Api
  module V1
    module Presenters
      module Merchants
        module ProductsManagement
          class EntityProperty < ::Grape::Entity

            expose :id
            expose :value
            expose :unit_value
            expose :entity_type do |entity_property, options|
              entity_property.entity_type.to_s.demodulize.underscore
            end
            expose :entity_id
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end

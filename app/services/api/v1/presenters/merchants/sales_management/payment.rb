module Api
  module V1
    module Presenters
      module Merchants
        module SalesManagement
          class Payment < Grape::Entity

            expose :id 
            expose :name
            expose :details
            expose :state
            expose :amount
            expose :type do |payment, options|
              payment.type.to_s.demodulize.underscore
            end
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end

module Api
  module V1
    module Presenters
      module Merchants
        module SalesManagement
          class Detail < Grape::Entity

            expose :id 
            expose :transaction_id
            expose :variant_id
            expose :variant, with: "::Api::V1::Presenters::Merchants::SalesManagement::Variant" do |detail, options|
              detail.fetch_variant || detail.variant
            end
            expose :quantity
            expose :got_discount do |transaction, options|
              transaction.got_discount?
            end
            expose :available_discounts, with: "::Api::V1::Presenters::Merchants::SalesManagement::Discount"
            expose :variant_tax_rate, with: "::Api::V1::Presenters::Merchants::SalesManagement::TaxRate" do |detail, options|
              detail.fetch_variant.try(:fetch_product).try(:fetch_tax_rate) || detail.variant.try(:product).try(:tax_rate)
            end
            expose :total_discount
            expose :total_tax
            expose :amount
            expose :amount_after_discount
            expose :amount_after_tax
            expose :notes
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end

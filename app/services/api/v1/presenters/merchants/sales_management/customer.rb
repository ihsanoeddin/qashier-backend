module Api
  module V1
    module Presenters
      module Merchants
        module SalesManagement
          class Customer < Grape::Entity

            expose :id
            expose :type do |customer, options|
              customer.type.to_s.demodulize.underscore
            end
            expose :number
            expose :phone_number
            expose :id_number
            expose :addresses, with: "::Api::V1::Presenters::Merchants::SalesManagement::Address" do |customer, options|
              customer.respond_to?(:addresses) ? ( customer.fetch_addresses || customer.addresses ) : []
            end
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end

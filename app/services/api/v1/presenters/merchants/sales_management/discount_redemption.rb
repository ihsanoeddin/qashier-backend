module Api
  module V1
    module Presenters
      module Merchants
        module SalesManagement
          class DiscountRedemption < Grape::Entity

            expose :id 
            expose :discount_id
            #expose :discount, with: "::Api::V1::Presenters::Merchants::SalesManagement::Discount" do |dv, options|
            #  dv.try(:fetch_discount) || dv.discount
            #end
            expose :redeemable_type do |dr, options|
              dr.redeemable_type.to_s.demodulize.underscore
            end
            expose :redeemable_id
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end

module Api
  module V1
    module Presenters
      module Merchants
        module SalesManagement
          class VariantDiscount < Grape::Entity

            expose :id
            expose :variant_id
            expose :variant, with: "::Api::V1::Presenters::Merchants::SalesManagement::Variant" do |dv, options|
              dv.try(:fetch_variant) || dv.variant
            end
            expose :discount_id
            expose :discount, with: "::Api::V1::Presenters::Merchants::SalesManagement::Discount" do |dv, options|
              dv.try(:fetch_discount) || dv.discount
            end
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end

module Api
  module V1
    module Presenters
      module Merchants
        module SalesManagement
          class Transaction < Grape::Entity

            expose :id 
            expose :customer_id
            expose :customer, with: "::Api::V1::Presenters::Merchants::SalesManagement::Customer" do |transaction, options|
              transaction.fetch_customer || transaction.customer
            end
            expose :sale_id
            expose :sale, with: "::Api::V1::Presenters::Merchants::SalesManagement::Sale" do |transaction, options|
              transaction.fetch_sale || transaction.sale
            end
            expose :store_id
            expose :store, with: "::Api::V1::Presenters::Merchants::SalesManagement::Store" do |transaction, options|
              transaction.fetch_store || transaction.store
            end
            expose :state
            expose :number
            expose :got_discount do |transaction, options|
              transaction.got_discount?
            end
            expose :available_discounts, with: "::Api::V1::Presenters::Merchants::SalesManagement::Discount"
            expose :discount_amount
            expose :tax_rate
            expose :total_tax
            expose :amount
            expose :amount_after_discount
            expose :amount_after_tax
            expose :details, with: "::Api::V1::Presenters::Merchants::SalesManagement::Detail" do |transaction, options|
              transaction.fetch_details || transaction.details
            end
            expose :payments, with: "::Api::V1::Presenters::Merchants::SalesManagement::Payment" do |transaction, options|
              transaction.fetch_payments || transaction.payments
            end
            expose :notes
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end

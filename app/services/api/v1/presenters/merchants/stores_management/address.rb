module Api
  module V1
    module Presenters
      module Merchants
        module StoresManagement
          class Address < Grape::Entity

            expose :id
            expose :address
            expose :city
            expose :province
            expose :country
            expose :latitude
            expose :longitude
            expose :zip_code
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end

module Api
  module V1
    module Presenters
      module Merchants
        module StoresManagement
          class Shop < ::Grape::Entity
            
            expose :id
            expose :name
            expose :deleted_at
            expose :created_at
            expose :updated_at
            expose :addresses, with: "::Api::V1::Presenters::Merchants::StoresManagement::Address"

          end
        end
      end
    end
  end
end
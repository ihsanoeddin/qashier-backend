module Api
  module V1
    module Presenters
      module Merchants
        module InventoryManagement
          class Store < ::Grape::Entity
              expose :id
              expose :name
              expose :business_model_id
              expose :business_model_name do |store, options|
                store.fetch_business_model.try(:name)
              end
              expose :short_name
          end
        end
      end
    end
  end
end
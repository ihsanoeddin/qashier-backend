module Api
  module V1
    module Presenters
      module Merchants
        module InventoryManagement
          class Stock < ::Grape::Entity
              expose :id
              expose :variant_id
              expose :store_id
              expose :quantity_available
              expose :type do |stock, options|
                stock.type.to_s.demodulize.underscore
              end
              expose :low_stock do |stock, options|
                stock.low?
              end
              expose :out_of_stock do |stock, options|
                stock.out?
              end
              expose :count_on_store
              expose :count_pending_to_customer
              expose :count_pending_from_supplier
              expose :store, using: "::Api::V1::Presenters::Merchants::InventoryManagement::Store" do |stock, options|
                stock.fetch_store
              end
          end
        end
      end
    end
  end
end
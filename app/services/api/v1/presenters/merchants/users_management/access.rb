module Api
  module V1
    module Presenters
      module Merchants
        module UsersManagement
          class Access < ::Grape::Entity
            
            expose :id
            expose :name
            expose :key
            expose :type
            expose :active
            expose :parent_id

          end
        end
      end
    end
  end
end
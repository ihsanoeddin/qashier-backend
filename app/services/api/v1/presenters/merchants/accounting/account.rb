module Api
  module V1
    module Presenters
      module Merchants
        module Accounting
          class Account < ::Grape::Entity
              
              expose :id
              expose :name
              expose :group_id
              expose :group_name do |account, options|
                account.fetch_group.name if account.fetch_group.present?
              end
              expose :sub_accounts, with: self.name do |account, options|
                account.fetch_sub_accounts || account.sub_accounts
              end
              expose :code
              expose :identifing_name
              expose :static
              expose :description
              expose :deleted_at
              expose :created_at
              expose :updated_at
          
          end
        end
      end
    end
  end
end
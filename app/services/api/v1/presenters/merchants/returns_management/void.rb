module Api
  module V1
    module Presenters
      module Merchants
        module ReturnsManagement
          class Void < ::Grape::Entity
            
            expose :id
            expose :state
            expose :transaction_id
            expose :transaction, with: "::Api::V1::Presenters::Merchants::ReturnsManagement::Transaction" do |void, options|
              void.fetch_transaksi || void.transaksi
            end
            expose :store_id
            expose :store, with: "::Api::V1::Presenters::Merchants::ReturnsManagement::Store" do |void, options|
              void.fetch_store || void.store
            end
        
            expose :amount

            expose :state
            expose :notes
            expose :time

            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end
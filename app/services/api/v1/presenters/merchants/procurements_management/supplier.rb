module Api
  module V1
    module Presenters
      module Merchants
        module ProcurementsManagement
          class Supplier < ::Grape::Entity
            expose :id
            expose :name
            expose :email
            expose :phone_number
            expose :code
            expose :mode
            expose :state
            expose :max_purchase_orders_qty
            expose :min_purchase_orders_qty
            expose :contacts, using: "::Api::V1::Presenters::ProcurementsManagement::Contact" do |supplier, options|
              supplier.fetch_contacts || supplier.contacts
            end
            expose :addresses, using: "::Api::V1::Presenters::ProcurementsManagement::Address" do |supplier, options|
              supplier.fetch_addresses || supplier.addresses
            end
            expose :contacts, using: "::Api::V1::Presenters::ProcurementsManagement::Contact" do |supplier, options|
              supplier.fetch_contacts || supplier.contacts
            end
            expose :deleted_at
            expose :created_at
            expose :updated_at
          end
        end
      end
    end
  end
end
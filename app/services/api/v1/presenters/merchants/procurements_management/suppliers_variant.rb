module Api
  module V1
    module Presenters
      module Merchants
        module ProcurementsManagement
          class SuppliersVariant < ::Grape::Entity
            expose :id
            expose :supplier_id
            expose :supplier, using: "::Api::V1::Presenters::Merchants::ProcurementsManagement::Supplier" do |sv, options|
              sv.fetch_supplier || sv.supplier
            end
            expose :variant_id
            expose :variant, using: "::Api::V1::Presenters::Merchants::ProcurementsManagement::Variant" do |sv, options|
              sv.fetch_variant || sv.variant
            end
            expose :deleted_at
            expose :created_at
            expose :updated_at
          end
        end
      end
    end
  end
end
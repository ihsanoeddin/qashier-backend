module Api
  module V1
    module Presenters
      module Merchants
        module ProcurementsManagement
          class Contact < ::Grape::Entity
            expose :id
            expose :name
            expose :email
            expose :phone_number
            expose :deleted_at
            expose :created_at
            expose :updated_at
          end
        end
      end
    end
  end
end
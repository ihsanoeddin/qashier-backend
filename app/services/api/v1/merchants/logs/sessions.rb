#
# Class tomhandle all sessions activities
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module Logs
        class Sessions < Base
          
          self.context_resource_class= '::Main::UsersManagement::Models::User'
          use_resource!

          self.presenter_name= "Merchants::Logs::Session"

          resources "merchants/logs/sessions/actives" do 

            desc "[GET] index all logged in users"
            get do
              users= resource_class_constant.filter(filter_params).current_signing_in
              presenter paginate(users)
            end

            desc "[GET] get existing user"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

          end

          resources "merchants/log/sessions/inactive" do

            desc "[GET] index all logged out users"
            get do
              users= resource_class_constant.filter(filter_params).current_signing_out
              presenter paginate(users)
            end

            desc "[GET] get existing user"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end
          
          end

        end
      end
    end
  end
end
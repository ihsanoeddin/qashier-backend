#
# this class is used as base class for users merchants namespace
#

module Api
  module V1
    module Merchants
      class Base < Api::V1::Base

        #
        # mount users endpoints
        #
        mount Api::V1::Merchants::UsersManagement::Base
        mount Api::V1::Merchants::StoresManagement::Base
        mount Api::V1::Merchants::ProductsManagement::Base
        mount Api::V1::Merchants::InventoryManagement::Base
        mount Api::V1::Merchants::SalesManagement::Base
        mount Api::V1::Merchants::Accounting::Base
        mount Api::V1::Merchants::ReturnsManagement::Base
        mount CurrentCompany
        mount Api::V1::Merchants::Logs::Base

      end
    end
  end
end
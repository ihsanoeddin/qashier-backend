#
# this class is used to handle product management product types
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProductsManagement
        class TaxRates < Base
          
          self.context_resource_class= '::Main::ProductsManagement::Models::TaxRate'
          use_resource!

          self.presenter_name= "Merchants::ProductsManagement::TaxRate"

          helpers do 
            
            def tax_rate_params
              posts[:product_type]||= posts
              posts.require(:product_type).permit(:name, :abbr, :rate)
            end

          end

          resources "merchants/products_management/tax_rates" do 

            desc "[GET] index all merchants tax rates"
            get do
              tax_rates= resource_class_constant.cached_collection.search(filter_params[:name])
              presenter paginate(tax_rates)
            end

            desc "[GET] show a tax rate"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[POST] create a tax rate"
            post do
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a tax rate"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.update tax_rate_params
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a tax rate"
            delete ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end
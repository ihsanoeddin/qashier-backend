#
# this class is used to handle product management product types
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProductsManagement
        class ProductTypes < Base
          
          self.context_resource_class= '::Main::ProductsManagement::Models::ProductType'
          use_resource!

          self.presenter_name= "Merchants::ProductsManagement::ProductType"

          helpers do 
            
            def product_type_params
              posts[:product_type]||= posts
              posts.require(:product_type).permit(:name, :permalink, :description)
            end

          end

          resources "merchants/products_management/product_types" do 

            desc "[GET] index all merchants product types"
            get do
              product_types= resource_class_constant.cached_collection.search(filter_params[:name])
              presenter paginate(product_types)
            end

            desc "[GET] show a product type"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[POST] create a product type"
            post do
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a product type"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.update product_type_params
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a product type"
            delete ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end
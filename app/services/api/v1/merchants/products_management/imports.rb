#
# this class is used to handle imports product
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProductsManagement
        class Imports < Base
          
          self.context_resource_class= '::Supports::Importers::Models::Import'
          use_resource!

          self.presenter_name= "Merchants::ProductsManagement::ProductImporter"

          #
          # change upload attributes to uploaded class
          #
          before do 
            if params[:file_attributes].present? && params[:file_attributes][:attachment].present?
              file_params = params[:file_attributes][:attachment]
              if file_params.respond_to?(:keys) && (file_params.keys.uniq.sort == ["filename", "type", "name", "tempfile", "head"].sort)
                params[:file_attributes][:attachment]= ActionDispatch::Http::UploadedFile.new(file_params)
              end
            end
          end

          helpers do 
            
            def import_params
              posts[:import]||= posts
              posts.require(:import).permit(:name, :file_attributes => [:attachment, :remote_attachment_url, :id, :destroy])
            end

          end

          resources "merchants/products_management/imports" do 

            desc "[GET] index all merchants imports"
            get do
              imports= resource_class_constant.filter(filter_params.merge(type: "variant"))
              presenter paginate(imports)
            end

            desc "[GET] show an import"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[POST] create an import"
            post do
              context_resource.becomes! ::Main::ProductsManagement::Importers::Variant
              context_resource.import_class= ::Main::ProductsManagement::Models::Variant.name
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a import"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.new?
                if context_resource.update import_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              else
                context_resource.errors.add(:state, "Can not be updated if state is not new")
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete an import"
            delete ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.new?
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              else
                context_resource.errors.add(:state, "Can not be deleted if state is not new")
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] process an import"
            put ":id/process", requirements: { id: /[0-9]*/ } do
              context_resource.process!
              presenter context_resource
            end

            desc "[PUT] pause an import"
            put ":id/pause", requirements: { id: /[0-9]*/ } do
              context_resource.pause!
              presenter context_resource
            end

          end

        end
      end
    end
  end
end
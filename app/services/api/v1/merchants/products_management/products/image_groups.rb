#
# this class is used to handle product management product services
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProductsManagement
        module Products
          class ImageGroups < Base
            
            self.context_resource_class= '::Supports::Imageable::Models::ImageGroup'
            use_resource!

            self.presenter_name= "Merchants::ProductsManagement::ImageGroup"

            #
            # change upload attributes to uploaded class
            #
            before do 
              begin
                if params[:images_attributes].present? 
                  if params[:images_attributes].is_a?(Array) or params[:images_attributes].is_a?(Hashie::Mash)
                    params[:images_attributes].each_with_index do |images_attributes, index|
                      image_params = images_attributes.is_a?(Array) ? images_attributes.last[:attachment] : images_attributes[:attachment]
                      if image_params.respond_to?(:keys) && (image_params.keys.uniq.sort == ["filename", "type", "name", "tempfile", "head"].sort)
                        params[:images_attributes][index][:attachment]= ActionDispatch::Http::UploadedFile.new(image_params)
                      end
                    end
                  end
                end
              rescue => e
                Rails.logger.info(e.message)
                Rails.logger.info(e.backtrace)
              end
            end

            helpers do 
              
              def image_group_params
                posts[:image_group]||= posts
                posts.require(:image_group).permit(:name, :contextable_id, :contextable_type, images_attributes: [:id, :_destroy, :remote_attachment_url, :attachment])
              end

              def skip_authorization;Rails.env.development?;end

            end

            #
            # unscoped
            #
            resources "merchants/products_management/products/image_groups" do 
              desc "[GET] index all merchants product images"
              get do
                image_groups= resource_class_constant.filter(filter_params)
                presenter paginate(image_groups)
              end

              desc "[GET] show an image group"
              get ':id', requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[GET] get requirement data for image group"
              get "new" do
                presenter context_resource, root: "resource"
              end

              desc "[POST] create an image group"
              post do
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[GET] get requirement data for update image group"
              get "/:id/edit", requirements: { id: /[0-9]*/ } do
                presenter context_resource, root: "resource"
              end

              desc "[PUT] update an image group"
              put ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.update image_group_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end


              desc "[DELETE] delete an image group"
              delete ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            #
            # scoped by product id
            #
            resources "merchants/products_management/products/:product_id/image_groups" do 

              desc "[GET] index all merchants product images"
              get do
                image_groups= resource_class_constant.filter(filter_params).where(contextable: product)
                presenter paginate(image_groups)
              end

              desc "[GET] show an image group"
              get ':id', requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[GET] get requirement data for image group"
              get "new" do
                presenter context_resource, root: "resource",
                                            parameters: { 
                                                          images: direct_present(product.fetch_images, with: ::Api::V1::Presenters::Merchants::ProductsManagement::Image)
                                                        }
              end

              desc "[POST] create an image group"
              post do
                context_resource.contextable= product
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[GET] get requirement data for update image group"
              get "/:id/edit", requirements: { id: /[0-9]*/ } do
                presenter context_resource, root: "resource",
                                            parameters: { 
                                                          images: direct_present(product.fetch_images, with: ::Api::V1::Presenters::Merchants::ProductsManagement::Image)
                                                        }
              end

              desc "[PUT] update an image group"
              put ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.update image_group_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end


              desc "[DELETE] delete an image group"
              delete ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

            end

          end
        end
      end
    end
  end
end
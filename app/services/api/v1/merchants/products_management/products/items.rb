#
# this class is used to handle product management product items
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProductsManagement
        module Products
          class Items < Base
            
            self.context_resource_class= '::Main::ProductsManagement::Models::Product'#Types::Item'
            use_resource!

            self.presenter_name= "Merchants::ProductsManagement::Types::Item"

            #
            # change upload attributes to uploaded class
            #
            before do 
              if params[:images_attributes].present? 
                if params[:images_attributes].is_a?(Array)
                  params[:images_attributes].each_with_index do |images_attributes, index|
                    image_params = images_attributes[:attachment]
                    if image_params.respond_to?(:keys) && (image_params.keys.uniq.sort == ["filename", "type", "name", "tempfile", "head"].sort)
                      params[:images_attributes][index][:attachment]= ActionDispatch::Http::UploadedFile.new(image_params)
                    end
                  end
                end
              end
            end

            helpers do 
            
              def product_params
                posts[:item]||= posts
                posts.require(:item).permit(:name, :permalink, :brand, :cost, 
                                            :short_description, :price, :description, :featured, :product_type_id, :tax_rate_id,
                                            :keywords => [],
                                            :entity_properties_attributes => [:id ,:_destroy, :value, :unit_value, :position],
                                            :images_attributes => [:id, :remote_attachment_url, :attachment, :_destroy],
                                            :variants_attributes => [ :id, :_destroy, :name, :image_group_id, :active, :sku, :cost, :price, :permalink, :keywords, :short_description, :master, :featured ])
              end

            end

            resources "merchants/products_management/products/items" do 

              desc "[GET] index all merchants products"
              get do
                products= ::Main::ProductsManagement::Models::Types::Item.filter(filter_params)
                presenter paginate(products)
              end

              desc "[GET] show a product"
              get ':id', requirements: { id: /[0-9]*/ } do
                presenter context_resource
              end

              desc "[GET] get new product item data requirements"
              get "new" do
                presenter context_resource, root: "resource",
                                            parameters: { 
                                                          product_types: direct_present(::Main::ProductsManagement::Models::ProductType.cached_collection, with: ::Api::V1::Presenters::Merchants::ProductsManagement::ProductType),
                                                          tax_rates: direct_present(::Main::ProductsManagement::Models::TaxRate.cached_collection, with: ::Api::V1::Presenters::Merchants::ProductsManagement::TaxRate),
                                                          properties: direct_present(::Main::ProductsManagement::Models::Property.cached_collection.actives, with: ::Api::V1::Presenters::Merchants::ProductsManagement::Property)
                                                        }
              end

              desc "[POST] create a product"
              post do
                context_resource.becomes! ::Main::ProductsManagement::Models::Types::Item
                if context_resource.save
                  context_resource.activate!
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[GET] get existing product item data requirements"
              get ":id/edit", requirements: { id: /[0-9]*/ } do 
                presenter context_resource, root: "resource",
                                            parameters: { 
                                                          product_types: direct_present(::Main::ProductsManagement::Models::ProductType.cached_collection, with: ::Api::V1::Presenters::Merchants::ProductsManagement::ProductType),
                                                          tax_rates: direct_present(::Main::ProductsManagement::Models::TaxRate.cached_collection, with: ::Api::V1::Presenters::Merchants::ProductsManagement::TaxRate),
                                                          properties: direct_present(::Main::ProductsManagement::Models::Property.cached_collection.actives, with: ::Api::V1::Presenters::Merchants::ProductsManagement::Property)
                                                        }

              end

              desc "[PUT] update a product"
              put ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.update product_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[PUT] activate a product"
              put ':id/activate', requirements: { id: /[0-9]*/ } do 
                context_resource.activate!
                presenter context_resource
              end

              desc "[PUT] deactivate a product"
              put ':id/deactivate', requirements: { id: /[0-9]*/ } do
                context_resource.deactivate!
                presenter context_resource
              end

              desc "[DELETE] delete a product"
              delete ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

            end

          end
        end
      end
    end
  end
end
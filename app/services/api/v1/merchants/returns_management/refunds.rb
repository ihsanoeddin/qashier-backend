#
# this class is used to operate return management function process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ReturnsManagement
        class Refunds < Base
          
          self.context_resource_class= '::Main::ReturnsManagement::Models::Refund'
          use_resource!

          self.presenter_name= "Merchants::ReturnsManagement::Refund"

          helpers do 

            def refund_params
              posts[:refund]||= posts
              posts.require(:refund).permit( :store_id, :transaction_id, :variant_id, :quantity, :amount, :notes )
            end

          end

          resources "merchants/returns_management/refunds" do 

            desc "[GET] index all refunds"
            get do
              refunds= resource_class_constant.filter(filter_params)
              presenter paginate(refunds)
            end

            desc "[GET] show a refund"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[POST] create a refund"
            post do 
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a refund"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.update refund_params
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a refund"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end          

            desc "[PUT] reject a refund"
            put ":id/reject", requirements: { id: /[0-9]*/ } do 
              if context_resource.reject!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] accept a refund"
            delete ":id/accept", requirements: { id: /[0-9]*/ } do 
              if context_resource.accept!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end
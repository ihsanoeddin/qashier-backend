
module Api
  module V1
    module Merchants
      module ProcurementsManagement
        class Base < ::Api::V1::Stores::Base
          
          mount Suppliers
          mount Contacts
          mount SuppliersVariants

        end
      end
    end
  end
end
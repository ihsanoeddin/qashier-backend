#
# this class is used to handle invoice details
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProcurementsManagement
        class InvoiceDetails < Base
          
          self.context_resource_class= '::Main::ProcurementsManagement::Models::InvoiceDetail'
          use_resource!

          self.presenter_name= "Merchants::ProcurementsManagement::InvoiceDetail"

          helpers do 

            def invoice_detail_params
              posts[:invoice_detail]||= posts
              posts.require(:invoice_detail).permit(:variant_id, :received_quantity, :expected_quantity, :price, :name, :tax_rate, :discount_rate)
            end

          end

          resources "merchants/procurements_management/purchase_orders/:purchase_order_id/invoices/:invoice_id/details" do 

            desc "[GET] index all merchants invoice details scoped by purchase order and invoice"
            get do
              details= resource_class_constant.filter(filter_params)
              presenter paginate(details)
            end

            desc "[GET] show a detail"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[POST] create a detail"
            post do
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a detail"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.update invoice_detail_params
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a detail"
            delete ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end
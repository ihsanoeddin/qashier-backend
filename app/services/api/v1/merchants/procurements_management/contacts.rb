#
# this class is used to handle suppliers contacts
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProcurementsManagement
        class Contacts < Base
          
          self.context_resource_class= '::Main::ProcurementsManagement::Models::Contact'
          use_resource!

          self.presenter_name= "Merchants::ProcurementsManagement::Contact"

          helpers do 
            
            def contact_params
              posts[:contact]||= posts
              posts.require(:contact).permit(:name, :email, :phone_number)
            end

            def supplier
              @supplier||= ::Main::ProcurementsManagement::Models::Supplier.find(posts[:supplier_id])
            end

          end

          resources "merchants/procurements_management/suppliers/:supplier_id/contacts" do 

            desc "[GET] index all merchants suppliers"
            get do
              contacts= supplier.contacts.filter(filter_params)
              presenter paginate(contacts)
            end

            desc "[GET] show a contact"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[POST] create a contact"
            post do
              context_resource.supplier= supplier
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a contact"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.update supplier_params
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a contact"
            delete ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end
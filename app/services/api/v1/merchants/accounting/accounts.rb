#
# this class is to handle accounting account
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module Accounting
        class Accounts < Base
          
          self.context_resource_class= '::Main::Accounting::Models::Account'
          use_resource!

          self.presenter_name= "Merchants::Accounting::Account"

          helpers do 

            def account_params
              posts[:account]||= posts
              posts.require(:account).permit(:name, :group_id, :code, :description)
            end

          end

          resources "merchants/accounting/accounts" do 

            desc "[GET] index all accounts"
            get do
              accounts= resource_class_constant.filter(filter_params)
              presenter paginate(accounts)
            end

            desc "[GET] show an account"
            get ':id' do 
              presenter context_resource
            end

            desc "[POST] create an account"
            post do 
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update an account"
            put ":id" do 
              if context_resource.update account_params
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete an account"
            delete ":id" do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end            

          end

        end
      end
    end
  end
end
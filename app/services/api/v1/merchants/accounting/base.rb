#
# this class is base class for all classes in this namespace
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module Accounting
        class Base < ::Api::V1::Merchants::Base
          
          mount Accounts
          mount Balances
          mount Journals

        end
      end
    end
  end
end
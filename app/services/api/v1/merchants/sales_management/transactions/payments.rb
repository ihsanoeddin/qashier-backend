#
# this class is used to handle sales transactions
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module SalesManagement
        module Transactions
          class Payments < ::Api::V1::Merchants::SalesManagement::Base

            self.context_resource_class= '::Main::SalesManagement::Models::Payment'
            use_resource!

            self.presenter_name= "Merchants::SalesManagement::Payment"

            helpers do 
              
              def payment_params
                posts[:payment]||= posts
                payment_details= case posts[:payment_type]
                                  when 'cash'
                                    [:name]
                                  end
                posts.require(:payment).permit(:amount, :name, :payment_type, :details => payment_details)
              end

            end

            #
            # get all merchant's transactions
            #
            resources "merchants/sales_management/payments" do
              
              desc "[GET] index all merchants transaction's payment methods"
              get do 
                payments= resource_class_constant.filter(filter_params)
                presenter paginate(payments)
              end

            end

            resources "merchants/sales_management/sales/:sale_id/transactions/:transaction_id/payments" do 

              desc "[GET] index all merchants transaction payment based on transaction"
              get do
                payments= resource_class_constant.filter(filter_params.merge!(transaction_id: transaction.id))
                presenter paginate(payments)
              end

              desc "[GET] show a transaction payment"
              get ':id', requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[POST] create a transaction payment"
              post do
                authorize_transaction! do
                  context_resource.transaction_id= transaction.id
                  if context_resource.save
                    presenter context_resource
                  else
                    standard_validation_error(details: context_resource.errors)
                  end
                end
              end

              desc "[PUT] update a transaction payment"
              put ":id", requirements: { id: /[0-9]*/ } do 
                authorize_transaction! do
                  if context_resource.update payment_params
                    presenter context_resource 
                  else
                    standard_validation_error(details: context_resource.errors)
                  end
                end
              end

              desc "[DELETE] delete transaction payment"
              delete ":id", requirements: { id: /[0-9]*/ } do 
                authorize_transaction! do
                  if context_resource.destroy
                    presenter context_resource 
                  else
                    standard_validation_error(details: context_resource.errors)
                  end
                end
              end

            end

          end
        end
      end
    end
  end
end
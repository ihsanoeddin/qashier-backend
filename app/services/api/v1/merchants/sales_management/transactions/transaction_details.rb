#
# this class is used to handle sales transactions
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module SalesManagement
        module Transactions
          class TransactionDetails < ::Api::V1::Merchants::SalesManagement::Base

            self.context_resource_class= '::Main::SalesManagement::Models::TransactionDetail'
            use_resource!

            self.presenter_name= "Merchants::SalesManagement::Detail"

            helpers do 
              
              def transaction_detail_params
                posts[:transaction_detail]||= posts
                posts.require(:transaction_detail).permit(:variant_id, :name, :quantity, :notes, :amount)
              end

              def batch_transaction_details_params
                posts[:batch_details]||= posts
                posts.require(:batch_details).map do |p|
                 ActionController::Parameters.new(p.to_hash).permit(:variant_id, :name, :quantity, :notes, :amount)
              end
              end

            end

            #
            # get all merchant's transactions
            #
            resources "merchants/sales_management/transaction_details" do
              
              desc "[GET] index all merchants transaction details"
              get do 
                transaction_details= resource_class_constant.filter(filter_params)
                presenter paginate(transaction_details)
              end

            end

            resources "merchants/sales_management/sales/:sale_id/transactions/:transaction_id/details" do 

              desc "[GET] index all merchants transaction detailss based on transaction"
              get do
                details= resource_class_constant.filter(filter_params.merge!(transaction_id: transaction.id))
                presenter paginate(details)
              end

              desc "[GET] show a transaction detail"
              get ':id', requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[POST] create a transaction detail"
              post do
                authorize_transaction! do
                  context_resource.transaction_id= transaction.id
                  if context_resource.save
                    presenter context_resource
                  else
                    standard_validation_error(details: context_resource.errors)
                  end
                end
              end

              desc "[PUT] update a transaction detail"
              put ":id", requirements: { id: /[0-9]*/ } do 
                authorize_transaction! do
                  if context_resource.update transaction_detail_params
                    presenter context_resource 
                  else
                    standard_validation_error(details: context_resource.errors)
                  end
                end
              end

              desc "[DELETE] delete transaction detail"
              delete ":id", requirements: { id: /[0-9]*/ } do 
                authorize_transaction! do 
                  if context_resource.destroy
                    presenter context_resource 
                  else
                    standard_validation_error(details: context_resource.errors)
                  end
                end
              end

              desc "[POST] batch details"
              post "batch" do 
                authorize_transaction! do 
                  results= resource_class_constant.batch(batch: batch_transaction_details_params, base_query: resource_class_constant.cached_collection.where(transaction_id: transaction.id))
                  if not results.error_state
                     presenter results.batch, root: "details"
                  else
                    standard_validation_error details: results.errors
                  end
                end
              end

            end

          end
        end
      end
    end
  end
end
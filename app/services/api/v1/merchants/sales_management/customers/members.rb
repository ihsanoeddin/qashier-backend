#
# this class is used to handle sales variants discounts
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module SalesManagement
        module Customers
          class Members < Base
            
            self.context_resource_class= '::Main::SalesManagement::Models::Customer'
            use_resource!

            self.presenter_name= "Merchants::SalesManagement::Customer"

            helpers do 

              def customer_params
                posts[:member]||= posts
                posts.require(:member).permit(:name, :number, :phone_number, :id_number, addresses_attributes: [:id, :address, :city, :province, :country, :zip_code, :latitude, :longitude, :_destroy])
              end
           end

            resources "merchants/sales_management/members" do 

              desc "[GET] index all created members"
              get do
                members= resource_class_constant.search(filter_params[:common])
                presenter paginate(members)
              end

              desc "[GET] show a members"
              get ":id", requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[POST] create a member"
              post do
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[PUT] update a member"
              put ":id", requirements: { id: /[0-9]*/ } do
                if context_resource.update customer_params
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[DELETE] delete a member"
              delete ":id", requirements: { id: /[0-9]*/ } do
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

            end
          end
        end
      end
    end
  end
end
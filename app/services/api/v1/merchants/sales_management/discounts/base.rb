#
# this class is used to handle sales discounts base class
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module SalesManagement
        module Discounts
          class Base < ::Api::V1::Merchants::SalesManagement::Base
            
            mount Coupons
            mount DiscountedVariants
            mount DiscountRedemptions

          end
        end
      end
    end
  end
end
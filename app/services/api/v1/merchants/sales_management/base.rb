#
# this class is used to handle sales base class
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module SalesManagement
        class Base < ::Api::V1::Merchants::Base
          
          mount Sales
          mount ::Api::V1::Merchants::SalesManagement::Transactions::Base
          mount ::Api::V1::Merchants::SalesManagement::Discounts::Base
          mount ::Api::V1::Merchants::SalesManagement::Customers::Base

        end
      end
    end
  end
end
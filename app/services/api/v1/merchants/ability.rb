#
# this class is used to map access for current user
# valid only for api version 1
# initialize on base class for specific namespace if you'd like
# inherit the class on specific namespace if you need specific authorization
#
module Api
  module V1
    module Merchants
      class Ability

        def initialize(current_user)
          super
        end
        
      end
    end
  end
end
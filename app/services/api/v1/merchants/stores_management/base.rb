#
# this class is used to handle stores management process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module StoresManagement
        class Base < ::Api::V1::Merchants::Base
          
          mount Shops
          mount BusinessModels

        end
      end
    end
  end
end
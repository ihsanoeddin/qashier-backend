#
# this class is used to handle stores management process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module StoresManagement
        class BusinessModels < Base
          
          self.context_resource_class= '::Main::StoresManagement::Models::BusinessModel'#Types::Shop'
          use_resource!

          self.presenter_name= "Merchants::StoresManagement::BusinessModel"

          before do 
            #
            # populate business models record
            #
            unless Rails.env.production?
              ::Main::StoresManagement::Models::BusinessModel.create!([{ name: 'Retail', identifing_name: 'retail', static: true }, { name: "Resto/Cafe", identifing_name: 'resto/cafe', static: true}]) if ::Main::StoresManagement::Models::BusinessModel.cached_collection.blank?
            end
          end

          resources "merchants/stores_management/business_models" do 

            desc "[GET] index all model business"
            get do
              business_models= resource_class_constant.filter(filter_params)
              presenter paginate(business_models)
            end

            desc "[GET] show a model business"
            get ':id' do 
              presenter context_resource
            end

          end

        end
      end
    end
  end
end
#
# this class is used to handle stores management process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module StoresManagement
        class Shops < Base
          
          self.context_resource_class= '::Main::StoresManagement::Models::Store'#Types::Shop'
          use_resource!

          self.presenter_name= "Merchants::StoresManagement::Shop"

          #
          # change upload attributes to uploaded class
          #
          before do 
            if params[:avatar_attributes].present? && params[:avatar_attributes][:attachment].present?
              avatar_params = params[:avatar_attributes][:attachment]
              if avatar_params.respond_to?(:keys) && (avatar_params.keys.uniq.sort == ["filename", "type", "name", "tempfile", "head"].sort)
                params[:avatar_attributes][:attachment]= ActionDispatch::Http::UploadedFile.new(avatar_params)
              end
            end
          end

          helpers do 
            
            def store_params
              posts[:shop]||= posts
              posts.require(:shop).permit(:name, :short_name, addresses_attributes: [:id, :address, :city, :province, :country, :zip_code, :latitude, :longitude, :_destroy], :avatar_attributes => [:_destroy, :id, :attachment, :remote_attachment_url])
            end

          end

          resources "merchants/stores_management/shops" do 

            desc "[GET] index all merchants shops"
            get do
              shops= resource_class_constant.filter(filter_params)
              presenter paginate(shops)
            end

            desc "[GET] show a shop"
            get ':id' do 
              presenter context_resource
            end

            desc "[POST] create a shop"
            post do
              context_resource.becomes! ::Main::StoresManagement::Models::Types::Shop
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a shop"
            put ":id" do 
              if context_resource.update store_params
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a shop"
            delete ":id" do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end
#
# this class is used as base class for users stores namespace
#

module Api
  module V1
    module Stores
      class Base < Api::V1::Base

        #
        # track changes of trackable object
        #
        self.track_resource = true

        helpers do

          def store_class
            ::Main::StoresManagement::Models::Store            
          end

          #
          # current store object
          #
          def current_store
            @current_store ||= store_class.fetch(params[:store_id])
          end

          def current_ability
             Api::V1::Stores::Ability.new(current_user, current_store: current_store)
          end

        end

        before do 
          @current_store ||= store_class.fetch(params[:store_id])
        end

        #
        # mount all stores namespace endpoints
        #
        mount Api::V1::Stores::Cashier::Base
        

      end
    end
  end
end
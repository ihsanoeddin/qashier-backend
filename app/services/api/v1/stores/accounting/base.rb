#
# this class is used as base class for users stores cashiers namespace
#

module Api
  module V1
    module Stores
      module Accounting
        class Base < Api::V1::Stores::Base

          helpers do

            #
            # current accountant
            #
            def current_accountant
              @current_accountant ||= current_user
            end

          end

          mount Accounts
          mount Balances
          mount Journals
          
        end
      end
    end
  end
end
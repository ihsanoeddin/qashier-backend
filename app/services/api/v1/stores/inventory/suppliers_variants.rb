#
# this class is used to handle suppliers variants
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Inventory
        class SuppliersVariants < Base
          
          self.context_resource_class= '::Main::ProcurementsManagement::Models::SuppliersVariant'
          use_resource!

          self.presenter_name= "Stores::SuppliersVariant"

          helpers do 
            
            def suppliers_variant_params
              posts[:contact]||= posts
              posts.require(:contact).permit(:supplier_id, :variant_id, :price)
            end

            def supplier
              @supplier||= ::Main::ProcurementsManagement::Models::Supplier.find(posts[:supplier_id])
            end

          end

          resources "stores/:store_id/inventory/suppliers/:supplier_id/variants" do 

            desc "[GET] index all merchants suppliers variants"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              suppliers_variants= supplier.suppliers_variants.filter(filter_params)
              presenter paginate(suppliers_variants)
            end

            desc "[GET] show a suppliers_variant"
            get ':id', requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create a suppliers_variant"
            post '', authorize: [:read, self.context_resource_class.constantize] do
              context_resource.supplier= supplier
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a suppliers_variant"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              if context_resource.update suppliers_variant_params
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a suppliers variant"
            delete ":id", requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end
#
# this class is used to handle purchase orders
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Inventory
        class PurchaseOrders < Base
          
          self.context_resource_class= '::Main::ProcurementsManagement::Models::PurchaseOrder'
          use_resource!

          self.presenter_name= "StoresPurchaseOrder"

          #
          # change upload attributes to uploaded class
          #
          before do 
            begin
              (params[:invoices_attributes] || []).each_with_index do |invoce_attributes, outer_index|
                if invoce_attributes[:images_attributes].present? 
                  if invoce_attributes[:images_attributes].is_a?(Array) or invoce_attributes[:images_attributes].is_a?(Hashie::Mash)
                    invoce_attributes[:images_attributes].each_with_index do |images_attributes, index|
                      image_params = images_attributes.is_a?(Array) ? images_attributes.last[:attachment] : images_attributes[:attachment]
                      if image_params.respond_to?(:keys) && (image_params.keys.uniq.sort == ["filename", "type", "name", "tempfile", "head"].sort)
                        params[:invoce_attributes][outer_index][:images_attributes][index][:attachment]= ActionDispatch::Http::UploadedFile.new(image_params)
                      end
                    end
                  end
                end
              end
            rescue => e
              Rails.logger.info(e.message)
              Rails.logger.info(e.backtrace)
            end
          end

          helpers do 
            
            def purchase_order_params
              posts[:purchase_order]||= posts
              posts.require(:purchase_order).permit(:force_open, :store_id, :supplier_id, :order_date, :shipping_address, :notes,
                                                    :shipping_cost, :misc_cost,  notified_with: [], :details_attributes => [:id, :_destroy, :variant_id, :name, :quantity, :price])
            end

            def invoices_params
              posts[:invoices]||= posts
              posts.require(:invoices).permit(invoices_attributes: [ :force_accepted, :store_id, :supplier_delivery_order_number, :date, :notes, :tax_rate, :discount_rate, :details => [:id, :_destroy, :variant_id, :received_quantity, :expected_quantity, :price, :name, :tax_rate, :discount_rate], 
                                              :images_attributes => [:id, :_destroy, :remote_attachment_url, :attachment] ])
            end

            #
            # filter po based in store
            #
            def after_get_resource(got_resource)
              if got_resource.persisted?
                standard_not_found_error if got_resource.store_id != current_store.id
              else
                got_resource.store_id= current_store.id
              end
              got_resource
            end

          end

          resources "stores/:store_id/inventory/purchase_orders" do 

            desc "[GET] index all store purchase orders"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              purchase_orders= resource_class_constant.filter(filter_params.merge!({store_id: current_store.id}))
              presenter paginate(purchase_orders)
            end

            desc "[GET] show a purchase order"
            get ':id', requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create a purchase order"
            post '', authorize: [:create, self.context_resource_class.constantize] do
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a purchase order"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:update, self.context_resource_class.constantize] do 
              authorize_purchase_order_changes(id: params[:id]) do 
                if context_resource.update purchase_order_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[DELETE] delete a purchase_order"
            delete ":id", requirements: { id: /[0-9]*/ }, authorize: [:delete, self.context_resource_class.constantize] do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] open drafted purchase order to send notif to supplier"
            put ":id/open", requirements: { id: /[0-9]*/ }, authorize: [:open, self.context_resource_class.constantize] do 
              if context_resource.open!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] cancel opened purchase"
            put ":id/cancel", requirements: { id: /[0-9]*/ }, authorize: [:cancel, self.context_resource_class.constantize] do 
              if context_resource.cancel!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] change state purchase order to `processing`"
            put ":id/process", requirements: { id: /[0-9]*/ }, authorize: [:process, self.context_resource_class.constantize] do
              if context_resource.process!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] change state purchase order to `received`\n provide invoices params"
            put ":id/receive", requirements: { id: /[0-9]*/ }, authorize: [:receive, self.context_resource_class.constantize] do
              if context_resource.receive!(invoices_params)
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] change state purchase order to `completed`"
            put ":id/complete", requirements: { id: /[0-9]*/ }, authorize: [:complete, self.context_resource_class.constantize] do
              if context_resource.complete!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end           

          end

        end
      end
    end
  end
end
#
# this class is used to handle purchase orders
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Inventory
        class PurchaseOrderDetails < Base          
          
          self.context_resource_class= '::Main::ProcurementsManagement::Models::PurchaseOrderDetail'
          use_resource!

          self.presenter_name= "Stores::PurchaseOrderDetail"

          helpers do 
            
            def purchase_order_detail_params
              posts[:purchase_order_detail]||= posts
              posts.require(:purchase_order_detail).permit(:variant_id, :name, :quantity, :price)
            end

            def after_get_resource(got_resource)
              if got_resource.persisted?
                standard_not_found_error if got_resource.purchase_order_id != purchase_order.id
              else
                got_resource.purchase_order_id= purchase_order_id.id
              end
              got_resource
            end

          end

          resources "stores/:store_id/inventory/purchase_orders/:purchase_order_id/details" do 

            desc "[GET] index all store purchase order details"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              details= resource_class_constant.filter(filter_params.merge!({purchase_order_id: purchase_order.id}))
              presenter paginate(details)
            end

            desc "[GET] show a detail"
            get ':id', requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create a detail"
            post '', authorize: [:create, self.context_resource_class.constantize] do
              authorize_purchase_order_changes do 
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[PUT] update a detail"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:update, self.context_resource_class.constantize] do 
              authorize_purchase_order_changes do
                if context_resource.update purchase_order_detail_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[DELETE] delete a detail"
            delete ":id", requirements: { id: /[0-9]*/ }, authorize: [:delete, self.context_resource_class.constantize] do 
              authorize_purchase_order_changes do
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end          

          end
        end
      end
    end
  end
end
#
# this class is used to operate return management function process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Cashier
        class Refunds < Base
          
          self.context_resource_class= '::Main::ReturnsManagement::Models::Refund'
          use_resource!

          self.presenter_name= "Stores::Refund"

          helpers do 

            def refund_params
              posts[:refund]||= posts
              posts.require(:refund).permit( :transaction_id, :variant_id, :quantity, :amount, :notes )
            end

            #
            # filter
            # return 404 if context_resource#store_id != current_store.id
            #
            def after_get_resource(got_resource)
              if got_resource.persisted?
                standard_not_found_error if got_resource.store_id != store.id
              else
                context_resource.operator = current_user
                got_resource.store = store
              end
              super
            end

          end

          resources "stores/:store_id/cashier/refunds" do 

            desc "[GET] index all refunds"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              refunds= resource_class_constant.filter(filter_params.merge!({store_id: current_store.id}))
              presenter paginate(refunds)
            end

            desc "[GET] show a refund"
            get ':id', requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create a refund"
            post '', authorize: [:create, self.context_resource_class.constantize] do
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a refund"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:update, self.context_resource_class.constantize] do 
              authorize_return! context_resource do
                if context_resource.update refund_params
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[DELETE] delete a refund"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:delete, self.context_resource_class.constantize] do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end          

            desc "[PUT] reject a refund"
            put ":id/reject", requirements: { id: /[0-9]*/ }, authorize: [:reject, self.context_resource_class.constantize] do 
              if context_resource.reject!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] accept a refund"
            delete ":id/accept", requirements: { id: /[0-9]*/ }, authorize: [:accept, self.context_resource_class.constantize] do 
              if context_resource.accept!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end
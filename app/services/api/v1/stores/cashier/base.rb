#
# this class is used as base class for users stores cashiers namespace
#

module Api
  module V1
    module Stores
      module Cashier
        class Base < Api::V1::Stores::Base

          helpers do

            #
            # current cashier operator
            #
            def current_cashier
              @current_cashier ||= current_user
            end

            #
            # get sale object
            #
            def sale(id=nil, silent=false)
              @sale ||= ::Main::SalesManagement::Models::Sale.fetch(id || posts[:sale_id])
              raise ::ActiveRecord::RecordNotFound if (@sale.nil? || !@sale.store_id.eql?(current_store.id)) and !silent
              @sale
            end 

            #
            # authorize sale
            # only started sale can be updated
            #
            def authorize_sale! id=nil, &block
              if sale(id).try(:started?)
                yield
              else
                standard_permission_denied_error
              end
            end

            #
            # get transaction object
            #
            def transaction(id= nil, silent=false)
              @transaction ||= ::Main::SalesManagement::Models::Transaction.fetch(id || posts[:transaction_id])
              raise ::ActiveRecord::RecordNotFound if (@transaction.nil? and !silent) || (@transaction.sale_id != sale.id)
              @transaction
            end

            #
            # authorize transaction
            # only transaction with state in_progress can be updated
            #
            def authorize_transaction!(id=nil, &block)
              authorize_sale! do 
                if transaction(id).try(:in_progress?)
                  yield
                else
                  standard_permission_denied_error
                end
              end
            end

            #
            # only draft return can be updated
            #
            def authorize_return!(retur, &block)
              if retur.draft?
                yield
              else
                standard_permission_denied_error
              end
            end

          end

          mount Sales
          mount Transactions
          mount TransactionDetails
          mount Payments
          mount Refunds
          mount Returns
          
        end
      end
    end
  end
end
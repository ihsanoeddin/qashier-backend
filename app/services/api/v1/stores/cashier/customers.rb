#
# this class is used to handle sales variants discounts
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Cashier
        class Customers < Base
            
          self.context_resource_class= '::Main::SalesManagement::Models::Customer'
          use_resource!

          self.presenter_name= "Stores::Customer"

          helpers do 

            def customer_params
              posts[:member]||= posts
              posts.require(:member).permit(:name, :number, :phone_number, :id_number, addresses_attributes: [:id, :address, :city, :province, :country, :zip_code, :latitude, :longitude, :_destroy])
            end
         end

          resources "stores/:store_id/cashier/customers" do 

            desc "[GET] index all created members"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              members= resource_class_constant.search(filter_params[:common])
              presenter paginate(members)
            end

            desc "[GET] show a members"
            get ":id", requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create a member"
            post '', authorize: [:create, self.context_resource_class.constantize] do
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a member"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:update, self.context_resource_class.constantize] do
              if context_resource.update customer_params
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a member"
            delete ":id", requirements: { id: /[0-9]*/ }, authorize: [:delete, self.context_resource_class.constantize] do
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end
        end
      end
    end
  end
end
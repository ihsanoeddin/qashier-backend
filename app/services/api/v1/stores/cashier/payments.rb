#
# this class is used to handle sales transactions
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Cashier
        class Payments < Base

          self.context_resource_class= '::Main::SalesManagement::Models::Payment'
          use_resource!

          self.presenter_name= "Stores::Payment"

          helpers do 
            
            def payment_params
              posts[:payment]||= posts
              payment_details= case posts[:payment_type]
                                when 'cash'
                                  [:name]
                                end
              posts.require(:payment).permit(:amount, :name, :payment_type, :details => payment_details)
            end

            #
            # filter
            # return 404 if context_resource#transaction_id != transaction.id
            #
            def after_get_resource(got_resource)
              if got_resource.persisted?
                standard_not_found_error if got_resource.transaction_id != transaction.id
              else
                context_resource.operator = current_user
                got_resource.store = store
              end
              super
            end

          end

          resources "stores/:store_id/cashier/sales/:sale_id/transactions/:transaction_id/payments" do 

            desc "[GET] index all payments a transaction"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              payments= resource_class_constant.filter(filter_params.merge!(transaction_id: transaction.id))
              presenter paginate(payments)
            end

            desc "[GET] show a transaction payment"
            get ':id', requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create a transaction payment"
            post '', authorize: [:create, self.context_resource_class.constantize] do
              authorize_transaction! do
                context_resource.transaction_id= transaction.id
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[PUT] update a transaction payment"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:update, self.context_resource_class.constantize] do 
              authorize_transaction! do
                if context_resource.update payment_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[DELETE] delete transaction payment"
            delete ":id", requirements: { id: /[0-9]*/ }, authorize: [:delete, self.context_resource_class.constantize] do 
              authorize_transaction! do
                if context_resource.destroy
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

          end

        end
      end
    end
  end
end
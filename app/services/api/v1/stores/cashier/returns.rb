#
# this class is used to operate return management function process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Cashier
        class Returns < Base
          
          self.context_resource_class= '::Main::ReturnsManagement::Models::Return'
          use_resource!

          self.presenter_name= "Stores::Return"

          helpers do 

            def return_params
              posts[:return]||= posts
              posts.require(:return).permit( :transaction_id, :variant_id, :quantity, :amount, :exchange_variant_id, :exchange_quantity, :exchange_amount, :notes )
            end

            #
            # filter
            # return 401 if context_resource#store_id != current_store.id
            #
            def after_get_resource(got_resource)
              if got_resource.persisted?
                standard_not_found_error if got_resource.store_id != store.id
              else
                context_resource.operator = current_user
                got_resource.store = store
              end
              super
            end

          end

          resources "stores/:store_id/cashier/returns" do 

            desc "[GET] index all returns"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              returns= resource_class_constant.filter(filter_params.merge!({store_id: current_store.id}))
              presenter paginate(returns)
            end

            desc "[GET] show a return"
            get ':id', requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create a return"
            post '', authorize: [:create, self.context_resource_class.constantize] do 
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a return"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:update, self.context_resource_class.constantize] do 
              authorize_return! context_resource do 
                if context_resource.update return_params
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[DELETE] delete a return"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:delete, self.context_resource_class.constantize] do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end          

            desc "[PUT] reject a return"
            put ":id/reject", requirements: { id: /[0-9]*/ }, authorize: [:reject, self.context_resource_class.constantize] do 
              if context_resource.reject!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] accept a return"
            delete ":id/accept", requirements: { id: /[0-9]*/ }, authorize: [:accept, self.context_resource_class.constantize] do 
              if context_resource.accept!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end
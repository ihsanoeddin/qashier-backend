#
# this class is used to handle sales transaction details
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Cashier
        class TransactionDetails < ::Api::V1::Merchants::SalesManagement::Base

          self.context_resource_class= '::Main::SalesManagement::Models::TransactionDetail'
          use_resource!

          self.presenter_name= "Stores::TransactionDetail"

          helpers do 
            
            def transaction_detail_params
              posts[:transaction_detail]||= posts
              posts.require(:transaction_detail).permit(:variant_id, :name, :quantity, :notes, :amount)
            end

            def batch_transaction_details_params
              posts[:batch_details]||= posts
              posts.require(:batch_details).map do |p|
               ActionController::Parameters.new(p.to_hash).permit(:variant_id, :name, :quantity, :notes, :amount)
              end
            end

            #
            # filter with current transaction
            # return 401 if context_resource#transaction_id != transaction.id
            #
            def after_get_resource(got_resource)
              if got_resource.persisted?
                standard_not_found_error if got_resource.transaction_id != transaction.id
              else
                got_resource.transaction_id = transaction.id
              end
              super
            end

          end

          resources "stores/:store_id/cashier/sales/:sale_id/transactions/:transaction_id/details" do 

            desc "[GET] index all cashier transaction details based on transaction"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              details= resource_class_constant.filter(filter_params.merge!(transaction_id: transaction.id))
              presenter paginate(details)
            end

            desc "[GET] show a transaction detail"
            get ':id', requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create a transaction detail"
            post '', authorize: [:create, self.context_resource_class.constantize] do
              authorize_transaction! do
                context_resource.transaction_id= transaction.id
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[PUT] update a transaction detail"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:update, self.context_resource_class.constantize] do 
              authorize_transaction! do
                if context_resource.update transaction_detail_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[DELETE] delete transaction detail"
            delete ":id", requirements: { id: /[0-9]*/ }, authorize: [:delete, self.context_resource_class.constantize] do 
              authorize_transaction! do 
                if context_resource.destroy
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[POST] batch details"
            post "batch", authorize: [[:create, :update], self.context_resource_class.constantize] do 
              authorize_transaction! do 
                results= resource_class_constant.batch(batch: batch_transaction_details_params, base_query: resource_class_constant.cached_collection.where(transaction_id: transaction.id))
                if not results.error_state
                   presenter results.batch, root: "details"
                else
                  standard_validation_error details: results.errors
                end
              end
            end

          end

        end
      end
    end
  end
end
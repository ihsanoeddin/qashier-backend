#
# this class is used to handle sales transactions
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Cashier
        class Transactions < Base

          self.context_resource_class= '::Main::SalesManagement::Models::Transaction'
          use_resource!

          self.presenter_name= "Stores::Transaction"

          helpers do 
            
            def transaction_params
              posts[:transaction]||= posts
              posts.require(:transaction).permit(:customer_id, :notes, :number, :discount_ids, :exclude_ppn, :exclude_tax,
                                                  payments_attributes: [:id, :_destroy, :amount, :payment_type, :name, :details => [:name, :provider_name, :reference_code]],
                                                  details_attributes: [:id, :name, :_destroy, :variant_id, :quantity, :notes, :amount])
            end

            #
            # filter with current sale
            # return 401 if context_resource#sale_id != sale.id
            #
            def after_get_resource(got_resource)
              if got_resource.persisted?
                standard_not_found_error if got_resource.sale_id != sale.id
              else
                got_resource.sale = sale
              end
              super
            end

          end

          #
          # get all cashiers's transactions
          #
          resources "stores/:id/cashier/transactions" do
            
            desc "[GET] index all cashiers transactions"
            get '', authorize: [:read, self.context_resource_class.constantize] do 
              transactions= resource_class_constant.filter(filter_params.merge!({operator_id: current_cashier.id, store_id: current_store.id}))
              presenter paginate(transactions)
            end

          end

          resources "stores/:store_id/cashier/sales/:sale_id/transactions" do 

            desc "[GET] index all merchants transactions based on sales"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              transactions= resource_class_constant.filter(filter_params.merge!(sale_id: sale.id))
              presenter paginate(transactions)
            end

            desc "[GET] show a transaction"
            get ':id', requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create a transaction"
            post '', authorize: [:create, self.context_resource_class.constantize] do
              authorize_sale! do 
                context_resource.sale_id= sale.id
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[PUT] update a transaction"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:update, self.context_resource_class.constantize] do 
              authorize_transaction!(posts[:id]) do 
                if context_resource.update transaction_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[PUT] pay a transaction"
            put ":id/pay", requirements: { id: /[0-9]*/ }, authorize: [:pay, self.context_resource_class.constantize] do 
              if context_resource.pay!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] cancel a transaction"
            put ":id/cancel", requirements: { id: /[0-9]*/ }, authorize: [:cancel, self.context_resource_class.constantize] do 
              if context_resource.cancel!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] completed a transaction"
            put ":id/complete", requirements: { id: /[0-9]*/ }, authorize: [:complete, self.context_resource_class.constantize] do 
              if context_resource.complete!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end
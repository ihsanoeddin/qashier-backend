module Api
  module V1
    module Sync
      class Entities < Base

        SYNCABLE_RESOURCES=  {
                                users: { model: ::Main::UsersManagement::Models::User, presenter: ::Api::V1::Presenters::Syncs::User },
                                roles: { model: ::Main::UsersManagement::Models::Role, presenter: ::Api::V1::Presenters::Syncs::Role }
                             }

        helpers do

          def sync_params
            posts[:sync]||= posts
            posts.require(:sync).permit(*([:per_page, :page, :clear_cache, :last_sync, :status, :resource] + synchronizable_resources.keys.map{|key| "last_sync_#{key}".to_sym }))
          end

          def synchronizable_resources
             SYNCABLE_RESOURCES
          end 

          def synchronize_status
            requested_resources_status.each do |options|
              config = options[:config].merge(status: true)
              results.send("#{options[:resource_name]}=", options[:model].synchronize(doorkeeper_token, config).total)
            end
            resp= {
                    available: results.to_h
                  }
          end

          def synchronize_collection
            options= synchronizable_resources[posts[:resource].to_s.to_sym].merge!(sync_params)
            results = resource_constant.synchronize(doorkeeper_token, options)
            results.to_h
          end

          def resource_constant
            synchronizable_resources[params[:resource].to_s.to_sym][:model] rescue raise Grape::Exceptions::MethodNotAllowed
          end

          def results
            @results ||= OpenStruct.new
          end

          def requested_resources_status
            @last_sync_params ||= synchronizable_resources.keys.map do |res_name|
              next if params["last_sync_#{res_name}".to_sym].blank?
              { resource_name: res_name, model: synchronizable_resources[res_name][:model], config: synchronizable_resources[res_name], last_sync: sync_params["last_sync_#{res_name}".to_sym] }
            end.compact
          end

        end

        resources "sync" do 

          desc "[POST] get status sync by provide last_sync_* params timestamp"
          post 'status' do 
            synchronize_status
          end

          desc "[POST] get sync collection based on last_sync params"
          post ':resource', requirements: { resources: Regexp.new(SYNCABLE_RESOURCES.keys.to_s) } do
            synchronize_collection
          end

        end

      end
    end
  end
end
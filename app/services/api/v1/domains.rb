#
# this class provide list of all companies domains
#

module Api
  module V1
      class Domains < Api::V1::Base

        helpers do 

          def companies
            ::Tenants::Merchants::Models::Company.cached_collection
          end

          def skip_authorization;true;end

        end

        resources "domains" do 

          desc "[GET] list available domains"
          get do 
            { collection: companies.map{|company| {name: company.name, domain: company.domain} } } 
          end
          
        end

      end
  end
end
#
# this class is used to map access for current user
# valid only for api version 1
# initialize on base class for specific namespace if you'd like
# inherit the class on specific namespace if you need specific authorization
#
module Api
  module V1
    class Ability
      #
      # include cancan methods helper
      #
      include CanCan::Ability

      #
      # attribute contain user class
      #
      class_attribute :user_class
      self.user_class = ::Main::UsersManagement::Models::User

      def initialize(current_user, options={})
        current_user ||= user_class.new # initialize current user if nil
        if current_user.is_merchant?
          can :manage, :all
        else
          specific_authorizations(current_user, options)
        end
      end

      #
      # override if you want
      #
      def specific_authorizations(current_user, options={})
        
      end

    end
  end
end
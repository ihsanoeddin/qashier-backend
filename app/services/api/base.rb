require 'doorkeeper/grape/helpers'
require 'grape/kaminari'

module Api
  class Base < Grape::API

    include StrongParams
    include Responder

    #
    # include doorkeeper helpers module
    #
    helpers ::Doorkeeper::Grape::Helpers

    #
    # include kaminari grape helper
    # set default pagination params
    #
    include Grape::Kaminari
    paginate per_page: 20, max_per_page: 30

    prefix 'api'
    format 'json'

    helpers do
      def logger
        Rails.logger
      end
    end

    #
    # catch active record not found exception
    # catch ActiveRecord::NoDatabaseError exception
    # catch Apartment::TenatNotFound exception
    #
    rescue_from ::ActiveRecord::RecordNotFound, ::Apartment::TenantNotFound, ActiveRecord::NoDatabaseError do |e|
      error_response(message: "Record not found", status: 404)
    end

    #
    # catch active record invalid exception
    #
    rescue_from ::ActiveRecord::RecordInvalid, IdentityCache::DerivedModelError do |e|
      error_response(message: e.message, status: 422)
    end

    #
    # catch bad request exception
    #
    rescue_from ::ActionController::ParameterMissing, ::ActiveRecord::Rollback do |e|
      error_response(message: e.message, status: 400)
    end

    #
    # catch action controller parameter missing
    #
    rescue_from ::ActionController::ParameterMissing do |e|
      error_response(message: e.message, status: 422)
    end

    #
    # catch Grape::Exceptions::ValidationErrors exception
    # catch Grape::Exceptions::MethodNotAllowed
    #
    rescue_from Grape::Exceptions::ValidationErrors, Grape::Exceptions::MethodNotAllowed do |e|
      error_response(message: e.message, status: 403)
    end

    #
    # rescue from aasm invalid transition
    #
    rescue_from AASM::InvalidTransition do |e|
      error_response(message: e.message, status: 422)
    end

    #
    # rescue from cancan access denied
    #
    rescue_from ::CanCan::AccessDenied do
      error!('403 Forbidden', 403)
    end

    #
    # rescue from all others exception
    #
    rescue_from :all do |e|
      if !Rails.env.production?
        raise e
      else
        error_response(message: e.backtrace, status: 500)
      end
    end



    helpers do 

      #
      # define current_resource_owner to get current user
      #
      def current_resource_owner
        @current_resource_owner||= ::Main::UsersManagement::Models::User.fetch(doorkeeper_token.resource_owner_id) if doorkeeper_token
      end

      def current_user
        current_resource_owner
      end

      #
      # define filter params
      #
      def filter_params
        params[:filter] || {}
      end

    end

    mount ::Api::V1::Base

  end
end
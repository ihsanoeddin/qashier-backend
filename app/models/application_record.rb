class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # declare all record to implement soft deletion
  acts_as_paranoid column: :deleted_at

  before_validation :readonlyfalse
  before_destroy :readonlyfalse

  #
  # include synchronization methods
  #
  include ::Supports::Synchronizable::Helpers::Sync

  #
  # include guard module
  #
  #include ::Supports::Guards::Protect

  #
  # set readonly to false
  #
  def readonlyfalse
    send(:instance_variable_set, "@readonly", false)
  end

end

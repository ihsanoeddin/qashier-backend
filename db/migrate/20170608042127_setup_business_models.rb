class SetupBusinessModels < ActiveRecord::Migration[5.0]
  
  def self.up
    #
    # create business model table
    #
    create_table :main_stores_management_business_models do |t|
      t.string :name
      t.string :identifing_name, index: true
      t.boolean :static, default: false
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end

    #
    # add column bussines_model_id to stores
    #
    add_column :main_stores_management_stores, :business_model_id, :integer, index: { name: "main_stores_management_stores_business_model_id" }

  end

  def self.down
    drop_table :main_stores_management_business_models
    remove_column :main_stores_management_stores, :bussines_model_id
  end

end

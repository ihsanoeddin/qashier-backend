class SetupProductsManagement2 < ActiveRecord::Migration[5.0]
  
  def self.up
    #
    # add business model id to products and variants
    #
    add_column :main_products_management_products, :business_model_id, :integer, index: { name: "main_products_management_products_business_model_id" }
    add_column :main_products_management_variants, :business_model_id, :integer, index: { name: "main_products_management_variants_business_model_id" }
    
    #
    # add bundled column to variants
    #
    add_column :main_products_management_variants, :bundled, :boolean, default: false

    #
    # create table vaiants bundle
    #
    create_table :main_products_management_variants_bundles do |t|
      t.references :bundle, index: { name: "main_products_management_variants_bundles_bundle_id" }
      t.references :variant, index: { name: "main_products_management_variants_bundles_variant_id" }
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end

  end

  def self.down
    remove_column :main_products_management_variants, :business_model_id
    remove_column :main_products_management_products, :business_model_id
    remove_column :main_products_management_variants, :bundled
    drop_table :main_products_management_variants_bundles

  end

end

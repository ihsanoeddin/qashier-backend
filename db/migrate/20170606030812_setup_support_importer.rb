class SetupSupportImporter < ActiveRecord::Migration[5.0]
  
  def self.up
    
    #
    # create table imports
    #
    create_table :supports_importers_imports do |t|
      
      t.references :store, index: { name: "supports_importers_imports_store_id" }
      t.string :name
      t.string :state
      t.text :details
      t.datetime :started_at
      t.datetime :finished_at
      t.integer :total_rows, default: 0
      t.integer :current_row, default: 0
      t.integer :skipped_rows, default: 0
      t.string :import_class
      t.text :results
      t.string :type
      t.datetime :deleted_at, index: true
      t.timestamps null: false

    end

    #
    # create table import attachments
    #
    create_table :supports_importers_files do |t|
      
      t.references :import, index: { name: "supports_importers_attachments_import_id" }
      t.string :syntax
      t.string :attachment
      t.datetime :deleted_at, index: true
      t.timestamps null: false

    end

  end

  def self.down
    drop_table :supports_importers_imports
    drop_table :supports_importers_files
  end

end

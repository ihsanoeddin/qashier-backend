class SetupInventoryManagement2 < ActiveRecord::Migration[5.0]
  def change

    add_column :main_inventory_management_stock_adjustments, :direction, :string, default: 'in'

  end
end

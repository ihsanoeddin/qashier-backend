class SetupSalesManagement2 < ActiveRecord::Migration[5.0]

  def self.up
    add_column :main_sales_management_sales, :operator_id, :integer, index: { name: "main_sales_management_sales_operator_id" }
  end

  def self.down
    drop_column :main_sales_management_sales, :operator_id
  end

end

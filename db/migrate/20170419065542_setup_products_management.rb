class SetupProductsManagement < ActiveRecord::Migration[5.0]
  
  def self.up
    
    # create table main products management products type
    create_table :main_products_management_product_types do |t|
      t.string :name
      t.string :permalink
      t.text :description
      t.datetime :deleted_at, index: { name: "products_management_products_type_deleted_at" }
      t.integer :products_count, :integer, default: 0
      t.timestamps
    end

    # create table main products management tax rates
    create_table :main_products_management_tax_rates do |t|
      t.string :name
      t.string :abbr
      t.decimal :rate, precision: 8, scale: 2, default: 0.0
      t.text :country_ids
      t.boolean :static, default: false
      t.datetime :deleted_at, index: { name: "products_management_tax_rates_deleted_at" }
      t.integer :products_count, :integer, default: 0
      t.timestamps
    end

    # create table main products management products table
    create_table :main_products_management_products do |t|
      t.string :name
      t.string :state
      t.string :brand
      t.string :type
      t.text :keywords
      t.string :permalink
      t.decimal :cost, :default => 0.0, :precision => 15, :scale => 2
      t.decimal :price, :default => 0.0, :precision => 15, :scale => 2
      t.string :short_description
      t.text :description
      t.boolean :featured, default: false
      t.references :product_type, index: { name: "products_management_products_product_type_id" }
      t.references :tax_rate, index: { name: "products_management_products_tax_rate_id" }
      t.datetime :deleted_at, index: { name: "products_management_products_deleted_at" }
      t.integer :variants_count, default: 0
      t.integer :images_count, default: 0
      t.integer :entity_properties_count, :integer, default: true
      t.string :barcode      
      t.timestamps
    end

    # create main products management variants table
    create_table :main_products_management_variants do |t|
      t.references :product, index: { name: "products_management_variants_product_id" }
      t.string :name
      t.string :sku
      t.string :state
      t.string :type
      t.text :keywords
      t.string :permalink
      t.decimal :cost, :default => 0.0, :precision => 15, :scale => 2
      t.decimal :price, :default => 0.0, :precision => 15, :scale => 2
      t.string :short_description
      t.boolean :master, default: false
      t.boolean :featured, default: false
      t.datetime :deleted_at, index: { name: "products_management_variants_deleted_at" }
      t.integer :entity_properties_count, :integer, default: 0
      t.boolean :active, default: true
      t.integer :transaction_details_count, default: 0
      t.references :image_group, index: { name: "products_management_variants_image_group_id" }
      t.string :barcode
      t.timestamps
    end

    # create main products management properties
    create_table :main_products_management_properties do |t|
      t.string :identifing_name
      t.string :display_name
      t.text :units
      t.boolean :active, :default => true
      t.string :description
      t.datetime :deleted_at, index: { name: "products_management_properties_deleted_at" }
      t.integer :entity_properties_count, :integer, default: true
      t.timestamps
    end

    # create main products management entity properties
    create_table :main_products_management_entity_properties do |t|
      t.references :entity, polymorphic: true, index: { name: "products_management_entity_type_and_entity_id" }
      t.references :property, index: { name: "products_management_entity_property_id" }
      t.integer :position
      t.float :value, default: 0
      t.string :unit_value
      t.datetime :deleted_at, index: { name: "products_management_entity_properties_deleted_at" }
      t.timestamps
    end

  end

  def self.down
    [ :main_products_management_products, 
      :main_products_management_tax_rates, 
      :main_products_management_product_types, 
      :main_products_management_variants,
      :main_products_management_properties,
      :main_products_management_entity_properties,
    ].each do |table_name|
      drop_table table_name
    end
  end

end

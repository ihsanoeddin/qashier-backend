class SetupReturnManagement2 < ActiveRecord::Migration[5.0]
  
  def self.up
    add_column :main_returns_management_voids, :operator_id, :integer, index: { name: "main_returns_management_voids_operator_id" }
    add_column :main_returns_management_returns, :operator_id, :integer, index: { name: "main_returns_management_returns_operator_id" }
    add_column :main_returns_management_refunds, :operator_id, :integer, index: { name: "main_returns_management_refunds_operator_id" }
  end

  def self.down
    drop_column :main_returns_management_refunds, :operator_id
    drop_column :main_returns_management_returns, :operator_id
    drop_column :main_returns_management_voids, :operator_id
  end

end

class SetupInventoryManagement < ActiveRecord::Migration[5.0]
  
  def self.up
    #
    # create inventories
    #
    create_table :main_inventory_management_stocks do |t|

      t.references :store, index: { name: "main_inventory_management_stocks_store_id" }
      t.references :variant, index: { name: "main_inventory_management_stocks_variant_id" }
      t.integer :count_on_store,               :default => 0
      t.integer :count_pending_to_customer,   :default => 0
      t.integer :count_pending_from_supplier, :default => 0

      t.datetime :deleted_at, index: true
      #t.timestamps null: false
    end

    #
    # create inventories_adjusment
    #
    create_table :main_inventory_management_stock_adjustments do |t|
      t.references :stock, index: { name: "main_inventory_management_stock_adjustments_stock_id" }
      t.references :reference, polymorphic: true, index: { name: "main_inventory_management_stock_adjustments_reference" }
      t.references :group, index: {name: "main_inventory_management_stock_adjustments_group"}
      t.integer :count, :default => 0
      t.string :on
      t.string :type
      t.string :state
      t.text :notes
      t.datetime :deleted_at, index: true
      t.timestamps
    end

  end

  def self.down
    drop_table :main_inventory_management_stocks
    drop_table :main_inventory_management_stock_adjustments
  end

end

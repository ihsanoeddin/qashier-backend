class SetupUsersManagement2 < ActiveRecord::Migration[5.0]
  
  def self.up
    add_column :main_users_management_accesses, :context, :text
    add_column :main_users_management_accesses, :routes, :text
    add_column :main_users_management_accesses, :categories, :text
    add_column :main_users_management_accesses, :description, :text
    change_column :main_users_management_accesses, :key, :text
  end

  def self.down
    remove_column :main_users_management_accesses, :context
    remove_column :main_users_management_accesses, :categories
    remove_column :main_users_management_accesses, :description
    remove_column :main_users_management_accesses, :routes
    change_column :main_users_management_accesses, :key, :string
  end

end

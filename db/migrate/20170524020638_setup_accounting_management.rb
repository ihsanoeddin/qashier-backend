class SetupAccountingManagement < ActiveRecord::Migration[5.0]
  
  def self.up

    #add_column :main_sales_management_transactions, :cost, :decimal, :default => 0.0, :precision => 15, :scale => 2
    #add_column :main_sales_management_transaction_details, :cost, :decimal, :default => 0.0, :precision => 15, :scale => 2

    #
    # create journals table
    #
    create_table :main_accounting_management_journals do |t|
      t.string :name
      t.string :identifing_name, index: true
      t.text :description
      t.boolean :static, default: false
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end

    #
    # create table journal_balances
    #
    create_table :main_accounting_management_journals_balances do |t|
      t.references :journal, index: { name: "main_accounting_management_journals_balances_journal_id" }
      t.references :balance, index: { name: "main_accounting_management_journals_balances_balance_id" }
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end

    #
    # create balances table
    #
    create_table :main_accounting_management_balances do |t|
      t.references :account, index: { name: "main_accounting_management_balances_account_id" }
      t.references :group, index: { name: "main_accounting_management_balances_group_id" }
      t.references :reference, polymorphic: true, index: { name: "main_accounting_management_balances_reference_type_and_id"}
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2 
      t.string :type 
      t.text :notes
      t.datetime :time
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end

    #
    # create table accounts
    #
    create_table :main_accounting_management_accounts do |t|
      t.string :name
      t.string :identifing_name, index: true
      t.string :code
      t.boolean :active, default: true
      t.boolean :static, default: false
      t.text :description
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end

  end

  def self.down
    [
      :main_accounting_management_journals, 
      :main_accounting_management_journals_balances, 
      :main_accounting_management_balances, 
      :main_accounting_management_accounts
      ].each { |table_name|  drop_table table_name }
  end

end

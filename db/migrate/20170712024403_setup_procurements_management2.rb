class SetupProcurementsManagement2 < ActiveRecord::Migration[5.0]
  
  def self.up
    if column_exists?(:main_procurements_management_purchase_orders, :terms_and_conditions)
      remove_column :main_procurements_management_purchase_orders, :terms_and_conditions
    end
    add_column :main_procurements_management_purchase_orders, :terms_and_conditions, :text
    add_column :main_procurements_management_purchase_orders, :shipping_cost, :decimal, :default => 0.0, :precision => 15, :scale => 2
    add_column :main_procurements_management_purchase_orders, :misc_cost, :decimal, :default => 0.0, :precision => 15, :scale => 2
  end

  def self.down
    remove_column :main_procurements_management_purchase_orders, :terms_and_conditions
    remove_column :main_procurements_management_purchase_orders, :shipping_cost
    remove_column :main_procurements_management_purchase_orders, :misc_cost
  end

end

class SetupSalesManagement < ActiveRecord::Migration[5.0]
  
  def self.up
    #drop_table :main_sales_management_sales, if_exists: true
    #drop_table :main_sales_management_transactions, if_exists: true
    #drop_table :main_sales_management_transaction_details, if_exists: true
    #drop_table :main_sales_management_payments, if_exists: true
    #drop_table :main_sales_management_discounts, if_exists: true
    #drop_table :main_sales_management_customers, if_exists: true
    #drop_table :main_sales_management_variants_discounts, if_exists: true
    
    #
    # create table sales
    #
    create_table :main_sales_management_sales do |t|

      t.references :store, index: { name: "main_sales_management_sales_store_id" }
      t.string :name
      t.string :state
      t.datetime :started_at
      t.datetime :finished_at
      t.integer :transactions_count, default: 0
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    
    end

    #
    # create table transactions
    #
    create_table :main_sales_management_transactions do |t|

      t.references :store, index: { name: "main_sales_management_transactions_store_id" }
      t.references :sale, index: { name: "main_sales_management_transactions_sale_id" }
      t.references :customer, index: { name: "main_sales_management_transactions_customer_id" }
      t.string :state
      t.string :number
      t.string :barcode
      t.string :type
      t.text :notes
      t.integer :transaction_details_count, default: 0
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
      t.decimal :cost, :default => 0.0, :precision => 15, :scale => 2
      t.decimal :tax_rate, default: 0.0, precision: 8, :scale => 2
      t.boolean :exclude_tax, default: false
      t.decimal :discount_amount, default: 0.0, precision: 15, scale: 2
      t.datetime :time
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    
    end

    #
    # create table transaction details
    #
    create_table :main_sales_management_transaction_details do |t|

      t.references :transaction, index: { name: "main_sales_management_transaction_details_transaction_id" }
      t.references :variant, index: { name: "main_sales_management_transaction_details_variant_id" }
      t.string :state
      t.string :name
      t.integer :quantity, default: 0
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
      t.decimal :cost, :default => 0.0, :precision => 15, :scale => 2
      t.decimal :discount_amount, default: 0.0, precision: 15, scale: 2
      t.decimal :tax_rate, default: 0.0, precision: 8, scale: 2
      t.text :notes
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    
    end

    #
    # create table transaction payments
    #
    create_table :main_sales_management_payments do |t|

      t.string :name
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
      t.references :transaction, index: { name: "main_sales_management_payment_transaction_id" }
      t.string :state
      t.text :details
      t.string :type
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    
    end

    #
    # create table discounts
    #
    create_table :main_sales_management_discounts do |t|

      t.references :store, index: { name: "main_sales_management_discounts_store_id" }
      t.string :code
      t.string :name
      t.text :description
      t.date :valid_from
      t.date :valid_until
      t.integer :redemption_limit, default: 0
      t.integer :discount_redemptions_count, default: 0
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
      t.string :based_on
      t.string :discount_orientation
      t.decimal :discount_orientation_amount, :default => 0.0, :precision => 15, :scale => 2
      t.string :type
      t.boolean :concurrent, default: false
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    
    end

    #
    # create table discounts variants
    #
    create_table :main_sales_management_variants_discounts do |t|

      t.references :discount, index: { name: "main_sales_management_discount_variants_discount_id" }
      t.references :variant, index: { name: "main_sales_management_discount_variants_variant_id" }
      t.datetime :deleted_at, index: true
      t.timestamps null: false

    end

    #
    # create table discount redemptions
    #
    create_table :main_sales_management_discount_redemptions do |t|

      t.references :discount, index: { name: "main_sales_management_discount_redemptions_discount_id" }
      t.references :redeemable, polymorphic: true, index: { name: "discount_redemptions_redeemable_type_and_redeemable_id" }
      t.string :state
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    
    end    

    #
    # create table customers
    #
    create_table :main_sales_management_customers do |t|

      t.string :name
      t.string :type
      t.string :number
      t.string :phone_number
      t.string :id_number
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    
    end

    #add_column :main_products_management_tax_rates, :static, :boolean, default: false


  end

  def self.down
    drop_table :main_sales_management_sales
    drop_table :main_sales_management_transactions
    drop_table :main_sales_management_transaction_details
    drop_table :main_sales_management_payments
    drop_table :main_sales_management_discounts
    drop_table :main_sales_management_discount_redemptions
    drop_table :main_sales_management_customers
    drop_table :main_sales_management_variants_discounts
  end

end

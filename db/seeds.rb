# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


#
# business field seeds
#

business_fields = [ 
                    { name: "Retail", keywords: [ "electronic", "food", "beverages", "tools" ] }
                  ]

::Tenants::Merchants::Models::BusinessField.create!(business_fields)

#
# end of business field seeds
#

#
# company seed for development only
#

if Rails.env.development?
  company= Tenants::Merchants::Models::Company.new name: "Sejahtera", domain: "sejahtera", email: "sejahtera@mail.com", business_field_id: ::Tenants::Merchants::Models::BusinessField.first.id
  company.save validate: false
  begin
    Apartment::Tenant.create(company.domain)
  rescue => e
    puts "I really don't know this shit is happening!"
  end

  Apartment::Tenant.switch!('sejahtera')

  admin = ::Main::UsersManagement::Models::User.new(username: "admin_sejahtera", password: 'password', password_confirmation: 'password', merchant: true)
  admin.skip_confirmation!
  admin.save
  admin.add_role(:super_admin)
end

#
# end of company seed for development
#


